"use strict";
var KTAppEcommerceReportCustomerOrders = function() {
    var t, e;
    return {
        init: function() {
            (t = document.querySelector("#kt_ecommerce_report_customer_orders_table")) && (t.querySelectorAll("tbody tr").forEach((t => {
                const e = t.querySelectorAll("td"),
                    r = moment(e[3].innerHTML, "DD MMM YYYY, LT").format();
                e[3].setAttribute("data-order", r)
            })), e = $(t).DataTable({
                info: !1,
                order: [],
                pageLength: 10
            }), (() => {
                var t = moment().subtract(29, "days"),
                    e = moment(),
                    r = $("#kt_ecommerce_report_customer_orders_daterangepicker");

                function o(t, e) {
                    r.html(t.format("MMMM D, YYYY") + " - " + e.format("MMMM D, YYYY"))
                }
                r.daterangepicker({
                    startDate: t,
                    endDate: e,
                    ranges: {
                        Today: [moment(), moment()],
                        Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
                        "Last 7 Days": [moment().subtract(6, "days"), moment()],
                        "Last 30 Days": [moment().subtract(29, "days"), moment()],
                        "This Month": [moment().startOf("month"), moment().endOf("month")],
                        "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                    }
                }, o), o(t, e)
            })(), (() => {
                const e = "Customer Orders Report";
                new $.fn.dataTable.Buttons(t, {
                    buttons: [{
                        extend: "copyHtml5",
                        title: e
                    }, {
                        extend: "excelHtml5",
                        title: e
                    }, {
                        extend: "csvHtml5",
                        title: e
                    }, {
                        extend: "pdfHtml5",
                        title: e
                    }]
                }).container().appendTo($("#kt_ecommerce_report_customer_orders_export")), document.querySelectorAll("#kt_ecommerce_report_customer_orders_export_menu [data-kt-ecommerce-export]").forEach((t => {
                    t.addEventListener("click", (t => {
                        t.preventDefault();
                        const e = t.target.getAttribute("data-kt-ecommerce-export");
                        document.querySelector(".dt-buttons .buttons-" + e).click()
                    }))
                }))
            })(), 
            document.querySelector('[data-kt-ecommerce-order-filter="edit_row"]').addEventListener("click", (function(t) {
                //e.search(t.target.value).draw()

                // Select parent row
                const parent = t.target.closest('tr');
                
                // Get allocation name
                const Seating_Area = parent.querySelectorAll('td')[0].innerText;
                const Seating_Seats = parent.querySelectorAll('td')[1].innerText;
                const Allocation_id = parent.querySelectorAll('td')[0].id;
                
                
                // SweetAlert2 pop up --- official docs reference: https://sweetalert2.github.io/
                Swal.fire({
                    text: "Are you sure you want to update allocation " + Seating_Area +" " + Seating_Seats + "?",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Yes, please!",
                    cancelButtonText: "No, cancel",
                    customClass: {
                        confirmButton: "btn fw-bold btn-danger",
                        cancelButton: "btn fw-bold btn-active-light-primary"
                    }
                }).then(function (result) {
                    if (result.value) {

                        // Get form
                        var FData = new FormData();
                        FData.append("Allocation_id", Allocation_id);

                        $.ajax({
                            type: "POST",
                            enctype: 'multipart/form-data',
                            url: "http://localhost:8080/edit_allocation_row",
                            data: FData,
                            processData: false,
                            contentType: false,
                            dataType: "html",
                            cache: false,
                            timeout: 800000,
                            success: function (modal_data) {
                 
                                $('#res_update_allocation').html(modal_data);
                                $('#kt_modal_new_target').modal('show');
                               
                 
                            },
                            error: function (e) {
                 
                                Swal.fire({
                                    text: 'Faild to update, please try again later!',
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn fw-bold btn-primary",
                                    }
                                });
                            }
                        });

                        

                    } else if (result.dismiss === 'cancel') {
                        Swal.fire({
                            text: Seating_Area +" " + Seating_Seats + " was not updated!",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn fw-bold btn-primary",
                            }
                        });
                    }
                });

            })),
            document.querySelector('[data-kt-ecommerce-order-filter="search"]').addEventListener("keyup", (function(t) {
                e.search(t.target.value).draw()
            })), (() => {
                const t = document.querySelector('[data-kt-ecommerce-order-filter="status"]');
                $(t).on("change", (t => {
                    let r = t.target.value;
                    "all" === r && (r = ""), e.column(2).search(r).draw()
                }))
            })())
        }
    }
}();



KTUtil.onDOMContentLoaded((function() {
    KTAppEcommerceReportCustomerOrders.init()
}));




//Delete Allocation
$('#kt_ecommerce_report_customer_orders_table').on('click', '.btnDelete', function(e){

    const this_tr = $(this).closest('tr');
    var Allocation_id = this.getAttribute("Allocation_id");
    var Seating_Area = this.getAttribute("Seating_Area");
    var Seating_Seats = this.getAttribute("Seating_Seats");
    
    
    Swal.fire({
        text: "Are you sure you want to delete allocation " + Seating_Area +" " + Seating_Seats + "?",
        icon: "warning",
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonText: "Yes, delete!",
        cancelButtonText: "No, cancel",
        customClass: {
            confirmButton: "btn fw-bold btn-danger",
            cancelButton: "btn fw-bold btn-active-light-primary"
        }
    }).then(function (result) {
        if (result.value) {

            // Get form
            var FData = new FormData();
            FData.append("Allocation_id", Allocation_id);

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "http://localhost:8080/delete_allocation_row",
                data: FData,
                processData: false,
                contentType: false,
                dataType: "json",
                cache: false,
                timeout: 800000,
                success: function (data) {
     
                    if(data.status==="1"){

                        Swal.fire({
                            text: "You have deleted " + Seating_Area +" " + Seating_Seats + "!.",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn fw-bold btn-primary",
                            }
                        }).then(function () {
                            // Remove current row
                            this_tr.remove();
                        });

                    }else{

                        Swal.fire({
                            text: "Faild to delete " + Seating_Area +" " + Seating_Seats + "!.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn fw-bold btn-primary",
                            }
                        });

                    }
                   
     
                },
                error: function (e) {
     
                    Swal.fire({
                        text: data.message,
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn fw-bold btn-primary",
                        }
                    });
                }
            });

            

        } else if (result.dismiss === 'cancel') {
            Swal.fire({
                text: Seating_Area +" " + Seating_Seats + " was not deleted.",
                icon: "error",
                buttonsStyling: false,
                confirmButtonText: "Ok, got it!",
                customClass: {
                    confirmButton: "btn fw-bold btn-primary",
                }
            });
        }
    });
    

})

