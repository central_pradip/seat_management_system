"use strict";
var KTModalNewTarget = function() {
    var t, e, n, a, o, i;
    return {
        init: function() {
            (i = document.querySelector("#kt_modal_new_target")) && (o = new bootstrap.Modal(i), a = document.querySelector("#kt_modal_new_target_form"), t = document.getElementById("kt_modal_new_target_submit"), e = document.getElementById("kt_modal_new_target_cancel"), new Tagify(a.querySelector('[name="tags"]'), {
                whitelist: ["Important", "Urgent", "High", "Medium", "Low"],
                maxTags: 5,
                dropdown: {
                    maxItems: 10,
                    enabled: 0,
                    closeOnSelect: !1
                }
            }), n = FormValidation.formValidation(a, {
                fields: {
                    project_name: {
                        validators: {
                            notEmpty: {
                                message: "Project title is required"
                            }
                        }
                    },
                    "Project_id": {
                        validators: {
                            notEmpty: {
                                message: "Please select matchup event"
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger,
                    bootstrap: new FormValidation.plugins.Bootstrap5({
                        rowSelector: ".fv-row",
                        eleInvalidClass: "",
                        eleValidClass: ""
                    })
                }
            }), t.addEventListener("click", (function(e) {
                e.preventDefault(), n && n.validate().then((function(e) {
                    console.log("validated!"), "Valid" == e ? (t.setAttribute("data-kt-indicator", "on"), t.disabled = !0, setTimeout((function() {
                        
                        //Calling Ajax
                        // Get form
                        var form = $('#kt_modal_new_target_form')[0];
                        var FData = new FormData(form);

                        $.ajax({
                            type: "POST",
                            enctype: 'multipart/form-data',
                            url: "http://localhost:8080/add_qr_codes",
                            data: FData,
                            processData: false,
                            contentType: false,
                            dataType: "json",
                            cache: false,
                            timeout: 800000,
                            success: function (data) {
                 
                                if(data.status==="1"){

                                    t.removeAttribute("data-kt-indicator"), t.disabled = !1, Swal.fire({
                                        text: data.message,
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Ok, got it!",
                                        customClass: {
                                            confirmButton: "btn btn-primary"
                                        }
                                    }).then((function(t) {
                                        t.isConfirmed && o.hide()
                                        window.location.assign(data.redirect_url);
                                    }))

                                }else{

                                    t.removeAttribute("data-kt-indicator"), t.disabled = !1, Swal.fire({
                                        text: data.message,
                                        icon: "error",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Ok, got it!",
                                        customClass: {
                                            confirmButton: "btn btn-primary"
                                        }
                                    }).then((function(t) {
                                        t.isConfirmed && o.hide()
                                    }))

                                }
                               
                 
                            },
                            error: function (e) {
                 
                                t.removeAttribute("data-kt-indicator"), t.disabled = !1, Swal.fire({
                                    text: 'Sorry, looks like there are some errors detected, please try again.',
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                }).then((function(t) {
                                    t.isConfirmed && o.hide()
                                }))
                                
                 
                            }
                        });

                        

                        // t.removeAttribute("data-kt-indicator"), t.disabled = !1, Swal.fire({
                        //     text: "Form has been successfully submitted!",
                        //     icon: "success",
                        //     buttonsStyling: !1,
                        //     confirmButtonText: "Ok, got it!",
                        //     customClass: {
                        //         confirmButton: "btn btn-primary"
                        //     }
                        // }).then((function(t) {
                        //     t.isConfirmed && o.hide()
                        // }))


                    }), 2e3)) : Swal.fire({
                        text: "Sorry, looks like there are some errors detected, please try again.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    })
                }))
            })), e.addEventListener("click", (function(t) {
                t.preventDefault(), Swal.fire({
                    text: "Are you sure you would like to cancel?",
                    icon: "warning",
                    showCancelButton: !0,
                    buttonsStyling: !1,
                    confirmButtonText: "Yes, cancel it!",
                    cancelButtonText: "No, return",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light"
                    }
                }).then((function(t) {
                    t.value ? (a.reset(), o.hide()) : "cancel" === t.dismiss && Swal.fire({
                        text: "Your form has not been cancelled!.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    })
                }))
            })))
        }
    }
}();
KTUtil.onDOMContentLoaded((function() {
    KTModalNewTarget.init()
}));