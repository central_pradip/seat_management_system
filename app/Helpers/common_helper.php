<?php 

// any_in_array() is not in the Array Helper, so it defines a new function
function any_in_array($needle, $haystack)
{
    $needle = is_array($needle) ? $needle : [$needle];

    foreach ($needle as $item) {
        if (in_array($item, $haystack, true)) {
            return true;
        }
    }

    return false;
}

// random_element() is included in Array Helper, so it overrides the native function
function random_element($array)
{
    shuffle($array);

    return array_pop($array);
}


function get_percentage($value_1,$value_2){

    if($value_2=="0" || $value_1=="0"){

        return '0';

    }else{
        return round(($value_2/$value_1)*100,2);
    }
   
    
}

function get_color_down($value){

    if($value>=80){

        return 'danger';

    }elseif($value>=60){

        return 'warning';

    }elseif($value>=40){

        return 'primary';

    }elseif($value>=0){

        return 'success';
    }

    
}

function get_color_up($value){

    if($value>=80){

        return 'success';

    }elseif($value>=60){

        return 'primary';

    }elseif($value>=40){

        return 'warning';

    }elseif($value>=0){

        return 'danger';
    }

    
}

//Calling Central API
function call_api($url,$api_data)
{ 
    date_default_timezone_set('Europe/London');
    $base=LIVE_WEB_URL;

    $secret_key=md5(strtotime("today"));
    
    //Set up and execute the curl process
    $curl_handle = curl_init();
    curl_setopt($curl_handle, CURLOPT_URL, $base.$url);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_handle, CURLOPT_POST, 1);
    curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
    'secret_key' => $secret_key,
    'data' => json_encode($api_data)
    ));
    
    $buffer = curl_exec($curl_handle);
    
    curl_close($curl_handle);
    $result = json_decode($buffer, true);
    
    return($result);	

}

?>