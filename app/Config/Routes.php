<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

//Syncing live events

$routes->get('/', 'Live_Events::sync_live_events');
$routes->add('/sync_live_events', 'Live_Events::sync_live_events');


$routes->add('/change_mode', 'Home::change_mode');

//Creating Event, Pulling Bookings, View Events
$routes->add('/create_matchup_live/(:any)', 'Live_Events::create_matchup_live/$1');
$routes->add('/store_matchup_event', 'Live_Events::store_matchup_event');
$routes->add('/mark_as_done', 'Live_Events::mark_as_done');

$routes->add('/live_matchup_dashboard(:any)', 'Live_Events::live_matchup_dashboard/$1');
$routes->add('/pull_live_bookings', 'Live_Events::pull_live_bookings');

$routes->add('/view_live_matchup_events', 'Live_Events::view_live_matchup_events');
$routes->add('/view_matchup_events_grid', 'Live_Events::view_matchup_events_grid');


//Doing Matchups
$routes->add('/reset_everything', 'Live_Matchup::reset_everything');
$routes->add('/auto_matchup_everything', 'Live_Matchup::auto_matchup_everything');
$routes->add('/auto_matchup_left_seats', 'Live_Matchup::auto_matchup_left_seats');
$routes->add('/auto_matchup_exact_seats', 'Live_Matchup::auto_matchup_exact_seats');


//QC Report
$routes->add('/Project_qc_report(:any)', 'QC_Report::Project_qc_report/$1');


//Downloads
$routes->add('/view_events_for_bookings', 'Download::view_events_for_bookings');
$routes->add('/view_events_for_allocations', 'Download::view_events_for_allocations');
$routes->add('/view_events_for_qr_codes', 'Download::view_events_for_qr_codes');
$routes->add('/view_all_allocations/(:any)', 'Allocations::view_all_allocations/$1');
$routes->add('/view_left_allocations/(:any)', 'Allocations::view_left_allocations/$1');
$routes->add('/download_seat_numbers/(:any)', 'Download::download_seat_numbers/$1');
$routes->add('/download_qr_codes', 'Download::download_qr_codes');


//Bookings
$routes->add('/add_bookings', 'Bookings::add_bookings');
$routes->add('/view_all_bookings/(:any)', 'Bookings::view_all_bookings/$1');
$routes->add('/get_bookings_for_allocation', 'Bookings::get_bookings_for_allocation');



$routes->add('/delete_booking_row', 'Bookings::delete_booking_row');
$routes->add('/download/download_bookings_type_1/(:any)', 'Download::download_bookings_type_1/$1');
$routes->add('/download/download_bookings_type_2/(:any)', 'Download::download_bookings_type_2/$1');
$routes->add('/download/download_bookings_custom_type/(:any)', 'Download::download_bookings_custom_type/$1');

//Allocations
$routes->add('/add_allocations', 'Allocations::add_allocations');
$routes->add('/edit_allocation_row', 'Allocations::edit_allocation_row');
$routes->add('/delete_allocation_row', 'Allocations::delete_allocation_row');
$routes->add('/download/download_left_allocation/(:any)', 'Download::download_left_allocation/$1');
$routes->add('/download/download_all_allocation/(:any)', 'Download::download_all_allocation/$1');

$routes->add('/access_need_allocations_seat_data', 'Allocations::access_need_allocations_seat_data');

//QR Codes
$routes->add('/add_qr_codes', 'QR_Codes::add_qr_codes');
$routes->add('/add_venue_qr_codes', 'QR_Codes::add_venue_qr_codes');
$routes->add('/view_all_qr_matchups', 'QR_Codes::view_all_qr_matchups');
$routes->add('/merge_ct_qr_codes', 'QR_Codes::merge_ct_qr_codes');
$routes->add('/merge_venue_qr_codes', 'QR_Codes::merge_venue_qr_codes');
$routes->add('/reset_all_qr_codes', 'QR_Codes::reset_all_qr_codes');
$routes->add('/replace_ct_qr_with_venue', 'QR_Codes::replace_ct_qr_with_venue');


$routes->add('/download/download_all_qr_codes/(:any)', 'QR_Codes::download_all_qr_codes/$1');
$routes->add('/download/download_ct_qr_codes/(:any)', 'QR_Codes::download_ct_qr_codes/$1');
$routes->add('/download/download_venue_qr_codes/(:any)', 'QR_Codes::download_venue_qr_codes/$1');
$routes->add('/download/download_qr_codes_for_developers/(:any)', 'QR_Codes::download_qr_codes_for_developers/$1');


$routes->add('/download/download_qr_codes_type_1/(:any)', 'Download::download_qr_codes_type_1/$1');
$routes->add('/download/download_qr_codes_type_2/(:any)', 'Download::download_qr_codes_type_2/$1');
$routes->add('/download/download_qr_codes_custom_type/(:any)', 'Download::download_qr_codes_custom_type/$1');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
