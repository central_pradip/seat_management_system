
<?PHP
header('Access-Control-Allow-Origin: *');
?>
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<?php include_once "common/header.php"; ?>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
		
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				<?php include_once "common/sidebar.php"; ?>
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" style="" class="header align-items-stretch">
						<!--begin::Container-->
						<div class="container-fluid d-flex align-items-stretch justify-content-between">
							<!--begin::Aside mobile toggle-->
							<div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show aside menu">
								<div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
									<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
									<span class="svg-icon svg-icon-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor" />
											<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="currentColor" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</div>
							</div>
							<!--end::Aside mobile toggle-->
							<!--begin::Mobile logo-->
							<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
								<a href="/metronic8/demo1/../demo1/index.html" class="d-lg-none">
									<img alt="Logo" src="assets/media/logos/logo-2.svg" class="h-30px" />
								</a>
							</div>
							<!--end::Mobile logo-->
							<!--begin::Wrapper-->
							<?php include('common/toolbar.php'); ?>
							<!--end::Wrapper-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar bg-transparent pt-6 mb-5" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-xxl d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex flex-column align-items-start me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex text-dark fw-bolder fs-3 flex-column mb-0">List of upcoming matchups
									<!--begin::Description-->
									<span class="text-muted fs-7 fw-bold mt-2">You have <?php echo count($events); ?> 
									<span class="text-primary fw-bolder">Active Events</span></span>
									<!--end::Description--></h1>
									<!--end::Title-->
								</div>
								<!--end::Page title-->
								
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container-xxl">
								<!--begin::Row-->
								<div class="row g-5 g-xl-10 mb-5 mb-xl-10">

                                <?php foreach($events as $event){ 
									$this_percentage = get_percentage($event['tickets'],$event['purchased']);
                                    ?>
                                   
                                   
                                    <!--begin::Col-->
									<div class="col-sm-6 col-xxl-3">
										<!--begin::Card widget 14-->
										<div class="card card-flush h-xl-100">
											<!--begin::Body-->
											<div class="card-body text-center pb-5">
												<!--begin::Overlay-->
												<a class="d-block overlay" data-fslightbox="lightbox-hot-sales" href="<?php echo $detail_images.$event['image']; ?>">
													<!--begin::Image-->
													<div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded mb-7" style="height: 145px;background-image:url('<?php echo $detail_images.$event['image']; ?>')"></div>
													<!--end::Image-->
													<!--begin::Action-->
													<div class="overlay-layer card-rounded bg-dark bg-opacity-25">
														<i class="bi bi-eye-fill fs-2x text-white"></i>
													</div>
													<!--end::Action-->
												</a>
												<!--end::Overlay-->
												<!--begin::Info-->
												<div class="d-flex align-items-end flex-stack mb-1">
													<!--begin::Title-->
													<div class="text-start">
														<span class="fw-bolder text-gray-800 cursor-pointer text-hover-primary fs-6 d-block" style="height: 50px;">
                                                          <a target="_blank" href="<?php echo LIVE_WEB_URL.'admin/Event/editevent/'.$event['event_id']; ?>"> <?php echo $event['title']; ?> </a></span>
														</div>
													<!--end::Title-->
												</div>
												<!--end::Info-->
                                                <!--begin::Info-->
												<div class="d-flex align-items-end flex-stack mb-1">
													<!--begin::Title-->
													<div class="text-start">
                                                      <span class="fw-bolder text-gray-800 cursor-pointer text-hover-primary fs-4 d-block" style="padding-bottom: 5px;">
                                                        <div class="d-flex align-items-center justify-content-end">
                                                        <div class="symbol symbol-30px me-3">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                                                            <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>
                                                            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"/>
                                                            </svg>
                                                        </div>
                                                        <span class="text-gray-600 fw-bolder d-block fs-6"><?php echo date('D jS M',strtotime($event['start_date'])).', '.date('h:i A',strtotime($event['start_time'])); ?></span>
                                                    </div>
                                                    </span>
														<span class="text-gray-600 fw-bolder d-block fs-6">
                                                            <div class="symbol symbol-30px me-3">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-ticket-detailed" viewBox="0 0 16 16">
                                                                  <path d="M4 5.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5Zm0 5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5ZM5 7a1 1 0 0 0 0 2h6a1 1 0 1 0 0-2H5Z"/>
                                                                  <path d="M0 4.5A1.5 1.5 0 0 1 1.5 3h13A1.5 1.5 0 0 1 16 4.5V6a.5.5 0 0 1-.5.5 1.5 1.5 0 0 0 0 3 .5.5 0 0 1 .5.5v1.5a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 11.5V10a.5.5 0 0 1 .5-.5 1.5 1.5 0 1 0 0-3A.5.5 0 0 1 0 6V4.5ZM1.5 4a.5.5 0 0 0-.5.5v1.05a2.5 2.5 0 0 1 0 4.9v1.05a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-1.05a2.5 2.5 0 0 1 0-4.9V4.5a.5.5 0 0 0-.5-.5h-13Z"/>
                                                                </svg>
                                                            </div>Scanned Ticket
                                                        </span>
													</div>
													<!--end::Title-->
													<!--begin::Total-->
                                                    <?php if($event['single_ticket']=="Yes"){ 
                                                          echo '<span class="badge badge-danger fs-base">No</span>';
                                                      }else{
                                                          echo '<span class="badge badge-success fs-base">Yes</span>';  
                                                      } ?>
													
													<!--end::Total-->
												</div>
												<!--end::Info-->
                                                <div class="d-flex align-items-end flex-stack mb-1">
                                                    <div class="d-flex align-items-center flex-column mt-3 w-100">
                                                        <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                                                            <span class="fw-boldest fs-6 text-dark"><?php echo number_format($event['purchased']).' Filled Out Of '.number_format($event['tickets']); ?></span>
                                                            <span class="fw-bolder fs-6 text-gray-400"><?php echo $this_percentage; ?>%</span>
                                                        </div>
                                                        <div class="h-8px mx-3 w-100 bg-light-<?php echo get_color_up($this_percentage); ?> rounded">
                                                            <div class="bg-<?php echo get_color_up($this_percentage); ?> rounded h-8px" role="progressbar" style="width: <?php echo $this_percentage; ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>

											</div>
											<!--end::Body-->
											<!--begin::Footer-->
											<div class="card-footer d-flex flex-stack pt-0">
												<!--begin::Link-->
												<a class="btn btn-sm btn-danger flex-shrink-0 me-2" href="/create_matchup_live/<?php echo $event['schedule_id']; ?>" target="_blank">Create Matchup</a>
												<!--end::Link-->
												<a class="btn btn-sm btn-success flex-shrink-0 me-2" href="<?php echo LIVE_WEB_URL.'admin/booking_details/'.$event['schedule_id']; ?>" target="_blank">Live Bookings</a>
											</div>
											<!--end::Footer-->
										</div>
										<!--end::Card widget 14-->
									</div>
									<!--end::Col-->

                                <?php } ?>

                                    
								</div>
								<!--end::Row-->
								
							</div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					 <?php include_once "common/footer.php"; ?>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
	
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		
		<!--begin::Javascript-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="assets/plugins/custom/fslightbox/fslightbox.bundle.js"></script>
		<script src="assets/plugins/custom/typedjs/typedjs.bundle.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/map.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/geodata/worldLow.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/geodata/continentsLow.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/geodata/usaLow.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/geodata/worldTimeZonesLow.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/geodata/worldTimeZoneAreasLow.js"></script>
		<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/widgets.bundle.js"></script>
		<script src="assets/js/custom/widgets.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
		<script>
			

			function mark_as_done(event_name,schedule_id,allocation,bookings){

				var FData = new FormData();
				FData.append("event_name", event_name);
				FData.append("schedule_id", schedule_id);
				FData.append("allocation", allocation);
				FData.append("bookings", bookings);

				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "http://localhost:8080/mark_as_done",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_message(response);
					},
					error: function (e) {

					}
				});

            }
		</script>
	</body>
	<!--end::Body-->
</html>