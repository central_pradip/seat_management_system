
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<?php include_once "common/header.php"; ?>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				<?php include_once "common/sidebar.php"; ?>
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" style="" class="header align-items-stretch">
						<!--begin::Container-->
						<div class="container-fluid d-flex align-items-stretch justify-content-between">
							<!--begin::Aside mobile toggle-->
							<div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show aside menu">
								<div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
									<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
									<span class="svg-icon svg-icon-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor" />
											<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="currentColor" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</div>
							</div>
							<!--end::Aside mobile toggle-->
							<!--begin::Mobile logo-->
							<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
								<a href="/metronic8/demo1/../demo1/index.html" class="d-lg-none">
									<img alt="Logo" src="http://localhost:8080/assets/media/logos/logo-2.svg" class="h-30px" />
								</a>
							</div>
							<!--end::Mobile logo-->
							<!--begin::Wrapper-->
							<?php include('common/toolbar.php'); ?>
								
							
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content" style="padding-top: 0px;">
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container-xxl">
								<!--begin::Navbar-->
								<div class="card mb-5 mb-xl-10">
									<div class="card-body pt-9 pb-0">
										<!--begin::Details-->
										<div class="d-flex flex-wrap flex-sm-nowrap mb-3">
											<!--begin: Pic-->
											<div class="me-7 mb-4">
												<div class="symbol symbol-100px symbol-lg-260px symbol-fixed position-relative" >
													<img src="<?php echo $listing_images.$schedule_info['image']; ?>" alt="image" style="width: 245px !important;height: 160px !important;" />
													<?php if($schedule_info['status']=="1"){
                                                         echo '<div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"  data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Bookings are still open!"></div>';
													}else{
														echo '<div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-danger rounded-circle border border-4 border-white h-20px w-20px" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Bookings are closed!"></div>';
													}
													?>
													
												</div>
											</div>
											<!--end::Pic-->
											<!--begin::Info-->
											<div class="flex-grow-1">
												<!--begin::Title-->
												<div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
													<!--begin::User-->
													<div class="d-flex flex-column">
														<!--begin::Name-->
														<div class="d-flex align-items-center mb-2">
															<a href="#" class="text-gray-900 text-hover-primary fs-2 fw-bolder me-1"><?php echo $schedule_info['title']; ?></a>
															<?php if($schedule_info['single_ticket']=="Yes"){ 
																echo '<a href="#" class="btn btn-sm btn-danger fw-bolder ms-2 fs-8 py-1 px-3" >Scanned Tickets</a>';
																echo '<a href="#" class="btn btn-sm btn-danger fw-bolder ms-2 fs-8 py-1 px-3" >No</a>';
															}else{
																echo '<a href="#" class="btn btn-sm btn-success fw-bolder ms-2 fs-8 py-1 px-3" >Scanned Tickets</a>';
																echo '<a href="#" class="btn btn-sm btn-success fw-bolder ms-2 fs-8 py-1 px-3" >Yes</a>';
															} ?>
															
														</div>
														<!--end::Name-->
														<!--begin::Info-->
														<div class="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
															
															<a href="#" class="d-flex align-items-center text-gray-600 text-hover-primary me-5 mb-2" style="font-size: 16px;">
															<!--begin::Svg Icon | path: icons/duotune/general/gen018.svg-->
															<span class="svg-icon svg-icon-4 me-1">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																	<path opacity="0.3" d="M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z" fill="currentColor" />
																	<path d="M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z" fill="currentColor" />
																</svg>
															</span>
															<!--end::Svg Icon--><?php echo $schedule_info['address']; ?></a>

															<a href="#" class="d-flex align-items-center text-gray-600 text-hover-primary mb-2" style="font-size: 16px;">
															<!--begin::Svg Icon | path: icons/duotune/communication/com011.svg-->
															<span class="svg-icon svg-icon-4 me-1">
																<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-currency-pound" viewBox="0 0 16 16">
																   <path d="M4 8.585h1.969c.115.465.186.939.186 1.43 0 1.385-.736 2.496-2.075 2.771V14H12v-1.24H6.492v-.129c.825-.525 1.135-1.446 1.135-2.694 0-.465-.07-.913-.168-1.352h3.29v-.972H7.22c-.186-.723-.372-1.455-.372-2.247 0-1.274 1.047-2.066 2.58-2.066a5.32 5.32 0 0 1 2.103.465V2.456A5.629 5.629 0 0 0 9.348 2C6.865 2 5.322 3.291 5.322 5.366c0 .775.195 1.515.399 2.247H4v.972z"/>
																</svg>
															</span>
															<!--end::Svg Icon--><?php echo $schedule_info['price']; ?></a>
														</div>
														<!--end::Info-->
													</div>
													<!--end::User-->
													<!--begin::Actions-->
													<div class="d-flex my-4">
														<!--begin::Indicator-->
														<!-- <a href="#" class="btn btn-sm btn-success me-2" >Bookings</a>
														<a href="#" class="btn btn-sm btn-primary me-2" >QR Code</a> -->
														
														<!--begin::Menu-->
														<div class="me-0">
															<button class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
																<i class="bi bi-three-dots fs-3"></i>
															</button>
															<!--begin::Menu 3-->
															<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
																<!--begin::Heading-->
																<div class="menu-item px-3">
																	<div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Menu</div>
																</div>
																<!--end::Heading-->
																<!--begin::Menu item-->
																<div class="menu-item px-3">
																	<a href="<?php echo 'https://centraltickets.co.uk/admin/Event/editevent/'.$schedule_info['event_id']; ?>" class="menu-link px-3">View Live Event </a>
																</div>
																<!--end::Menu item-->
																<!--begin::Menu item-->
																<div class="menu-item px-3">
																	<a href="<?php echo 'https://centraltickets.co.uk/admin/booking_details/'.$schedule_info['schedule_id']; ?>" class="menu-link px-3">View All Live Bookings </a>
																</div>
																<!--end::Menu item-->
																
															</div>
															<!--end::Menu 3-->
														</div>
														<!--end::Menu-->

													</div>
													<!--end::Actions-->
												</div>

												<!--end::Title-->
												<!--begin::Stats-->
												<div class="d-flex flex-wrap flex-stack">
													<!--begin::Wrapper-->
													<div class="d-flex flex-column flex-grow-1 pe-8">
														<!--begin::Stats-->
														<div class="d-flex flex-wrap">
															<!--begin::Stat-->
															<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3" style="background-color: #50cd89;">
																<!--begin::Number-->
																<div class="d-flex align-items-center">
																<span class="svg-icon svg-icon-4 me-1" style="line-height: 1;color: #f4f4f4;">
																<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-ticket-fill" viewBox="0 0 16 16">
																  <path d="M1.5 3A1.5 1.5 0 0 0 0 4.5V6a.5.5 0 0 0 .5.5 1.5 1.5 0 1 1 0 3 .5.5 0 0 0-.5.5v1.5A1.5 1.5 0 0 0 1.5 13h13a1.5 1.5 0 0 0 1.5-1.5V10a.5.5 0 0 0-.5-.5 1.5 1.5 0 0 1 0-3A.5.5 0 0 0 16 6V4.5A1.5 1.5 0 0 0 14.5 3h-13Z"/>
																</svg>
																</span>
                                                                <div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo $schedule_info['tickets']; ?>" data-kt-countup-prefix="" style="color:white;">0</div>
																</div>
																<!--end::Number-->
																<!--begin::Label-->
																<div class="fw-bold fs-6 text-gray-400" style="color: white !important;">Total Allocation</div>
																<!--end::Label-->
															</div>
															<!--end::Stat-->
															<!--begin::Stat-->
															<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3"  style="background-color: cadetblue;">
																<!--begin::Number-->
																<div class="d-flex align-items-center">
																	<span class="svg-icon svg-icon-4 me-1" style="line-height: 1;color: #f4f4f4;">
																	<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
																	  <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
																	  <path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
																	  <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
																	</svg>
																	</span>
																	<div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo ($schedule_info['purchased']); ?>" data-kt-countup-prefix="" >0</div>
																</div>
																<!--end::Number-->
																<!--begin::Label-->
																<div class="fw-bold fs-6 text-gray-400">Total Bookings</div>
																<!--end::Label-->
															</div>
															<!--end::Stat-->
															<!--begin::Stat-->
															<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3" style="background-color: #f00;">
																<!--begin::Number-->
																<div class="d-flex align-items-center">
																    <span class="svg-icon svg-icon-4 me-1" style="line-height: 1;color: #f4f4f4;">
																	<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-check-fill" viewBox="0 0 16 16">
																		<path fill-rule="evenodd" d="M15.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L12.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
																		<path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
																	</svg>
																	</span>
																	<div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo $schedule_info['ct_purchased']; ?>"data-kt-countup-prefix="" style="color:white;">0</div>
																</div>
																<!--end::Number-->
																<!--begin::Label-->
																<div class="fw-bold fs-6 text-gray-400" style="color: white !important;">Central Tickets</div>
																<!--end::Label-->
															</div>
															<!--end::Stat-->
															<!--begin::Stat-->
															<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3" style="background-color: #009ef7;">
																<!--begin::Number-->
																<div class="d-flex align-items-center">
																    <span class="svg-icon svg-icon-4 me-1" style="line-height: 1;color: #f4f4f4;">
																	<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus-fill" viewBox="0 0 16 16">
																	  <path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
																	  <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
																	</svg>
																	</span>
																	<div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo $schedule_info['aj_purchased']; ?>" data-kt-countup-prefix="" style="color:white;">0</div>
																</div>
																<!--end::Number-->
																<!--begin::Label-->
																<div class="fw-bold fs-6 text-gray-400" style="color: white !important;">AJTix</div>
																<!--end::Label-->
															</div>
															<!--end::Stat-->
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Wrapper-->
													<?php 
													$this_percentage = get_percentage($schedule_info['tickets'],$schedule_info['purchased']);
													?>
													<!--begin::Progress-->
													<div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
														<div class="d-flex justify-content-between w-100 mt-auto mb-2">
															<span class="fw-bold fs-6 text-gray-400">Filling Ratio</span>
															<span class="fw-bolder fs-6"><?php echo $this_percentage; ?>%</span>
														</div>
														<div class="h-5px mx-3 w-100 bg-light mb-3">
															<div class="bg-success rounded h-5px" role="progressbar" style="width: <?php echo $this_percentage; ?>%;" aria-valuenow="<?php echo $this_percentage; ?>" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div>
													<!--end::Progress-->
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
										</div>
										<!--end::Details-->
										
									</div>
								</div>
								<!--end::Navbar-->
								<!--begin::Basic info-->
								<div class="card mb-5 mb-xl-10">
									<!--begin::Card header-->
									<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
										<!--begin::Card title-->
										<div class="card-title m-0">
											<h3 class="fw-bolder m-0">Event Details</h3>
										</div>
										<!--end::Card title-->
									</div>
									<!--begin::Card header-->
									<!--begin::Content-->
									<div id="kt_modal_new_target" class="collapse show">
										<!--begin::Form-->
										<form id="kt_modal_new_target_form" class="form">
											<!--begin::Card body-->
											<div class="card-body border-top p-9">
												<input type="hidden" name="event_id" value="<?php echo $schedule_info['event_id']; ?>" />
												<input type="hidden" name="schedule_id" value="<?php echo $schedule_info['schedule_id']; ?>" />
												<input type="hidden" name="event_image" value="<?php echo $schedule_info['image']; ?>" />
												<!--begin::Input group-->
												<div class="row mb-6">
													<!--begin::Label-->
													<label class="col-lg-4 col-form-label required fw-bold fs-6">Event Title</label>
													<!--end::Label-->
													<!--begin::Col-->
													<div class="col-lg-8 fv-row">
														<input type="text" name="event_name" class="form-control form-control-lg form-control-solid" placeholder="2Cellos At OVO Arena Wembley" value="<?php echo $schedule_info['title']; ?>" />
													</div>
													<!--end::Col-->
												</div>
												<!--end::Input group-->	
												<!--begin::Input group-->
												<div class="row mb-6">
													<!--begin::Label-->
													<label class="col-lg-4 col-form-label required fw-bold fs-6">Tickets Details </label>
													<!--end::Label-->
													<!--begin::Col-->
													<div class="col-lg-8">
														<!--begin::Row-->
														<div class="row">
															<!--begin::Col-->
															<div class="col-lg-6 fv-row">
																<input type="text" name="allocation" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="150" value="<?php echo $schedule_info['tickets']; ?>" />
															</div>
															<!--end::Col-->
															<!--begin::Col-->
															<div class="col-lg-6 fv-row">
																<input type="text" name="purchased" class="form-control form-control-lg form-control-solid" placeholder="150" value="<?php echo $schedule_info['purchased']; ?>" />
															</div>
															<!--begin::Hint-->
															<div class="form-text">Please Enter Allocation & Bookings Details</div>
															<!--end::Hint-->
															<!--end::Col-->
														</div>
														<!--end::Row-->
													</div>
													<!--end::Col-->
												</div>
												<!--end::Input group-->
												
												<!--begin::Input group-->
												<div class="row mb-6">
													<!--begin::Label-->
													<label class="col-lg-4 col-form-label fw-bold fs-6">
														<span class="required">Allocation File</span>
														<i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Phone number must be active"></i>
													</label>
													<!--end::Label-->
													<!--begin::Col-->
													<div class="col-lg-8 fv-row">
														<input type="file" name="file" class="form-control form-control-lg form-control-solid" placeholder="Phone number" value="044 3276 454 935" />
													</div>
													<!--end::Col-->
												</div>
												<!--end::Input group-->
												<!--begin::Input group-->
												<div class="row mb-6">
													<!--begin::Label-->
													<label class="col-lg-4 col-form-label required fw-bold fs-6">Allocation Sheet Colomns</label>
													<!--end::Label-->
													<!--begin::Col-->
													<div class="col-lg-8 fv-row">
														<!--begin::Options-->
														<div class="d-flex align-items-center mt-3">
															<!--begin::Option-->
															<label class="form-check form-check-inline form-check-solid me-5">
																<input class="form-check-input" name="seat_colomns[]" type="checkbox" value="Section" checked />
																<span class="fw-bold ps-2 fs-6">Section</span>
															</label>
															<!--end::Option-->
															<!--begin::Option-->
															<label class="form-check form-check-inline form-check-solid">
																<input class="form-check-input" name="seat_colomns[]" type="checkbox" value="Row" checked />
																<span class="fw-bold ps-2 fs-6">Row</span>
															</label>
															<!--end::Option-->
															<!--begin::Option-->
															<label class="form-check form-check-inline form-check-solid">
																<input class="form-check-input" name="seat_colomns[]" type="checkbox" value="Seat" checked />
																<span class="fw-bold ps-2 fs-6">Seat</span>
															</label>
															<!--end::Option-->
															<!--begin::Option-->
															<label class="form-check form-check-inline form-check-solid">
																<input class="form-check-input" name="seat_colomns[]" type="checkbox" value="Quantity" checked />
																<span class="fw-bold ps-2 fs-6">Quantity</span>
															</label>
															<!--end::Option-->
															<!--begin::Option-->
															<label class="form-check form-check-inline form-check-solid">
																<input class="form-check-input" name="seat_colomns[]" type="checkbox" value="Entrance" checked />
																<span class="fw-bold ps-2 fs-6">Entrance</span>
															</label>
															<!--end::Option-->
															<!--begin::Option-->
															<label class="form-check form-check-inline form-check-solid">
																<input class="form-check-input" name="seat_colomns[]" type="checkbox" value="Extra"  />
																<span class="fw-bold ps-2 fs-6">Extra</span>
															</label>
															<!--end::Option-->
														</div>
														<!--end::Options-->
													</div>
													<!--end::Col-->
												</div>
												<!--end::Input group-->
												<!--begin::Input group-->
												<div class="row mb-6">
													<!--begin::Label-->
													<label class="col-lg-4 col-form-label required fw-bold fs-6">Allocation sepration by</label>
													<!--end::Label-->
													<!--begin::Col-->
													<div class="col-lg-8 fv-row">
														<input type="text" name="sepration" class="form-control form-control-lg form-control-solid" placeholder="Enter the keyword which seprates two tickets (i.e. --)" value="--" />
													</div>
													<!--end::Col-->
												</div>
												<!--end::Input group-->
												

												<!--begin::Input group-->
												<div class="d-flex flex-stack mb-8">
													<!--begin::Label-->
													<div class="me-5">
														<label class="fs-6 fw-bold required">All bookers are seating together?</label>
														<div class="fs-7 fw-bold text-muted">Select yes if you want users to be seated togather</div>
													</div>
													<!--end::Label-->
													<!--begin::Label-->
													<div class="col-lg-8 d-flex align-items-center">
														<div class="form-check form-check-solid form-switch fv-row">
														    <input class="form-check-input w-45px h-30px" type="checkbox" id="allowmarketing" name="seating_type" checked="checked" />
															<span class="form-check-label fw-bold text-muted">Yes</span>
														</div>
													</div>
													<!--begin::Label-->
												</div>
												<!--end::Input group-->

												<!--begin::Input group-->
												<div class="row mb-0">
													<!--begin::Label-->
													<label class="col-lg-4 col-form-label fw-bold fs-6 required">Scanned Tickets</label>
													<!--begin::Label-->
													<!--begin::Label-->
													<div class="col-lg-8 d-flex align-items-center">
														<div class="form-check form-check-solid form-switch fv-row">
														    <?php if($schedule_info['single_ticket']=="Yes"){ 
																echo '
																<input class="form-check-input w-45px h-30px" type="checkbox" id="allowmarketing" name="scanned_tickets" />
																<label class="form-check-label" for="allowmarketing">No</label>';
															}else{
																echo '<input class="form-check-input w-45px h-30px" type="checkbox" id="allowmarketing" name="scanned_tickets" checked="checked" />
																<label class="form-check-label" for="allowmarketing">Yes</label>';
															} ?>
															
															
														</div>
													</div>
													<!--begin::Label-->
												</div>
												<!--end::Input group-->
											</div>
											<!--end::Card body-->

											<!--begin::Actions-->
											<div class="card-footer d-flex justify-content-end py-6 px-9">
												<button type="reset" id="kt_modal_new_target_cancel" class="btn btn-light me-3">Cancel</button>
												<button type="submit" id="kt_modal_new_target_submit" class="btn btn-primary">
													<span class="indicator-label">Create Matchup</span>
													<span class="indicator-progress">Creating Matchup... 
													<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
												</button>
											</div>
											<!--end::Actions-->

											<!--begin::Actions-->
											<!-- <div class="card-footer d-flex justify-content-end py-6 px-9">
												<button type="reset" class="btn btn-light btn-active-light-primary me-2" id="kt_modal_new_target_cancel">Discard</button>
												<button type="submit" class="btn btn-primary" id="kt_modal_new_target_submit">Create Matchup</button>
												
											</div> -->
											<!--end::Actions-->

											
										</form>
										<!--end::Form-->
									</div>
									<!--end::Content-->
								</div>
								<!--end::Basic info-->
								
							</div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<?php include_once "common/footer.php"; ?>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		
		<!--begin::Javascript-->
		
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="http://localhost:8080/assets/plugins/global/plugins.bundle.js"></script>
		<script src="http://localhost:8080/assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="http://localhost:8080/assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="http://localhost:8080/assets/js/forms/create_matchup_live.js"></script>
		
		<script src="http://localhost:8080/assets/js/widgets.bundle.js"></script>
		<script src="http://localhost:8080/assets/js/custom/widgets.js"></script>
		
		<script src="http://localhost:8080/assets/js/custom/utilities/modals/offer-a-deal/type.js"></script>
		<script src="http://localhost:8080/assets/js/custom/utilities/modals/offer-a-deal/details.js"></script>
		<script src="http://localhost:8080/assets/js/custom/utilities/modals/offer-a-deal/finance.js"></script>
		<script src="http://localhost:8080/assets/js/custom/utilities/modals/offer-a-deal/complete.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>