
<?PHP
header('Access-Control-Allow-Origin: *');
?>
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<?php include_once "common/header.php"; ?>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
		
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				<?php include_once "common/sidebar.php"; ?>
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" style="" class="header align-items-stretch">
						<!--begin::Container-->
						<div class="container-fluid d-flex align-items-stretch justify-content-between">
							<!--begin::Aside mobile toggle-->
							<div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show aside menu">
								<div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
									<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
									<span class="svg-icon svg-icon-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor" />
											<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="currentColor" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</div>
							</div>
							<!--end::Aside mobile toggle-->
							<!--begin::Mobile logo-->
							<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
								<a href="/metronic8/demo1/../demo1/index.html" class="d-lg-none">
									<img alt="Logo" src="assets/media/logos/logo-2.svg" class="h-30px" />
								</a>
							</div>
							<!--end::Mobile logo-->
							<!--begin::Wrapper-->
							<?php include('common/toolbar.php'); ?>
							<!--end::Wrapper-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar bg-transparent pt-6 mb-5" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-xxl d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex flex-column align-items-start me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex text-dark fw-bolder fs-3 flex-column mb-0">QR Code Matchup Events
									<!--begin::Description-->
									<span class="text-muted fs-7 fw-bold mt-2">You have total - 
									<span class="text-primary fw-bolder"><?php echo count($matchup_events); ?>  Matchups</span></span>
									<!--end::Description--></h1>
									<!--end::Title-->
								</div>
								<!--end::Page title-->
								
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container-xxl">
								<!--begin::Row-->
								<div class="row g-5 g-xl-10 mb-5 mb-xl-10">

                                <?php foreach($matchup_events as $event){ 
									$this_percentage = get_percentage($event->Allocation,$event->Purchased);
                                    ?>
                                   
                                   
                                    <!--begin::Col-->
									<div class="col-sm-6 col-xxl-3">
										<!--begin::Card widget 14-->
										<div class="card card-flush h-xl-100">
											<!--begin::Body-->
											<div class="card-body text-center pb-5">
                                                
												<!--begin::Overlay-->
												<a class="d-block overlay" data-fslightbox="lightbox-hot-sales" href="#">
													<!--begin::Image-->
													<div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded mb-7" style="height: 145px;background-image:url('<?php echo $detail_images.$event->Image; ?>')"></div>
													<!--end::Image-->
													<!--begin::Action-->
													<div class="overlay-layer card-rounded bg-dark bg-opacity-25">
														<i class="bi bi-eye-fill fs-2x text-white"></i>
													</div>
													<!--end::Action-->
												</a>
												<!--end::Overlay-->
												<!--begin::Info-->
												<div class="d-flex align-items-end flex-stack mb-1">
													<!--begin::Title-->
													<div class="text-start">
														<span class="fw-bolder text-gray-800 cursor-pointer text-hover-primary fs-6 d-block" >
                                                          <a target="_blank" href="https://centraltickets.co.uk/admin/booking_details/<?php echo $event->Schedule_id; ?>"> <?php echo $event->Event_name; ?> </a></span>
														</div>
													<!--end::Title-->
												</div>
												<!--end::Info-->
                                                <!--begin::Info-->
												<div class="d-flex align-items-end flex-stack mb-1">
													<!--begin::Title-->
													<div class="text-start">
                                                      <span class="fw-bolder text-gray-800 cursor-pointer text-hover-primary fs-4 d-block" style="padding-bottom: 5px;">
                                                        <div class="d-flex align-items-center justify-content-end">
                                                        <div class="symbol symbol-30px me-3">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                                                            <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>
                                                            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"/>
                                                            </svg>
                                                        </div>
                                                        <span class="text-gray-600 fw-bolder d-block fs-6"><?php echo date('D jS M',strtotime($event->Created_at)).', '.date('h:i A',strtotime($event->Created_at)); ?></span>
                                                        
													</div>
                                                    </span>
														
													</div>
													<!--end::Title-->
													
												</div>
												<!--end::Info-->
												<?php 
												$db = db_connect();
												 $ct_qr_code_query = $db->query("SELECT id FROM qr_codes qr where qr.Project_id='".$event->Project_id."'");
												 $ct_qr_codes_numbers = $ct_qr_code_query->getNumRows();

												 if($ct_qr_codes_numbers>=1){
												?>
												 <!--begin::Info-->
												 <div class="d-flex align-items-end flex-stack mb-1" style="padding-top: 5px;">
													<!--begin::Title-->
													<div class="text-start">
                                                       <span class="text-gray-600 fw-bolder d-block fs-6">
                                                            <div class="symbol symbol-30px me-3">
															<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-qr-code" viewBox="0 0 16 16">
															<path d="M2 2h2v2H2V2Z"/>
															<path d="M6 0v6H0V0h6ZM5 1H1v4h4V1ZM4 12H2v2h2v-2Z"/>
															<path d="M6 10v6H0v-6h6Zm-5 1v4h4v-4H1Zm11-9h2v2h-2V2Z"/>
															<path d="M10 0v6h6V0h-6Zm5 1v4h-4V1h4ZM8 1V0h1v2H8v2H7V1h1Zm0 5V4h1v2H8ZM6 8V7h1V6h1v2h1V7h5v1h-4v1H7V8H6Zm0 0v1H2V8H1v1H0V7h3v1h3Zm10 1h-1V7h1v2Zm-1 0h-1v2h2v-1h-1V9Zm-4 0h2v1h-1v1h-1V9Zm2 3v-1h-1v1h-1v1H9v1h3v-2h1Zm0 0h3v1h-2v1h-1v-2Zm-4-1v1h1v-2H7v1h2Z"/>
															<path d="M7 12h1v3h4v1H7v-4Zm9 2v2h-3v-1h2v-1h1Z"/>
															</svg>
                                                            </div>CT's QR Codes
                                                        </span>
													</div>
													<!--end::Title-->
													<!--begin::Total-->
												       <span class="badge badge-success fs-base"><?php echo $ct_qr_codes_numbers; ?></span>
                                                    <!--end::Total-->
												</div>
												<!--end::Info-->
												<?php } 
												 
												 $venue_qr_code_query = $db->query("SELECT id FROM venue_qr_codes qr where qr.Project_id='".$event->Project_id."'");
												 $venue_qr_codes_numbers = $venue_qr_code_query->getNumRows();

												 if($venue_qr_codes_numbers>=1){
												?>
												 <!--begin::Info-->
												 <div class="d-flex align-items-end flex-stack mb-1" style="padding-top: 10px;">
													<!--begin::Title-->
													<div class="text-start">
                                                       <span class="text-gray-600 fw-bolder d-block fs-6">
                                                            <div class="symbol symbol-30px me-3">
															<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-qr-code" viewBox="0 0 16 16">
															<path d="M2 2h2v2H2V2Z"/>
															<path d="M6 0v6H0V0h6ZM5 1H1v4h4V1ZM4 12H2v2h2v-2Z"/>
															<path d="M6 10v6H0v-6h6Zm-5 1v4h4v-4H1Zm11-9h2v2h-2V2Z"/>
															<path d="M10 0v6h6V0h-6Zm5 1v4h-4V1h4ZM8 1V0h1v2H8v2H7V1h1Zm0 5V4h1v2H8ZM6 8V7h1V6h1v2h1V7h5v1h-4v1H7V8H6Zm0 0v1H2V8H1v1H0V7h3v1h3Zm10 1h-1V7h1v2Zm-1 0h-1v2h2v-1h-1V9Zm-4 0h2v1h-1v1h-1V9Zm2 3v-1h-1v1h-1v1H9v1h3v-2h1Zm0 0h3v1h-2v1h-1v-2Zm-4-1v1h1v-2H7v1h2Z"/>
															<path d="M7 12h1v3h4v1H7v-4Zm9 2v2h-3v-1h2v-1h1Z"/>
															</svg>
                                                            </div>Venue's QR Codes
                                                        </span>
													</div>
													<!--end::Title-->
													<!--begin::Total-->
													<span class="badge badge-danger fs-base"><?php echo $venue_qr_codes_numbers; ?></span>
                                                    <!--end::Total-->
												</div>
												<!--end::Info-->
												<?php } ?>
                                                <div class="d-flex align-items-end flex-stack mb-1">
                                                    <div class="d-flex align-items-center flex-column mt-3 w-100">
                                                        <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                                                            <span class="fw-boldest fs-6 text-dark"><?php echo number_format($event->Purchased).' Filled Out Of '.number_format($event->Allocation); ?></span>
                                                            <span class="fw-bolder fs-6 text-gray-400"><?php echo $this_percentage; ?>%</span>
                                                        </div>
                                                        <div class="h-8px mx-3 w-100 bg-light-<?php echo get_color_up($this_percentage); ?> rounded">
                                                            <div class="bg-<?php echo get_color_up($this_percentage); ?> rounded h-8px" role="progressbar" style="width: <?php echo $this_percentage; ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>

											</div>
											<!--end::Body-->
                                            <!--begin::Footer-->
											<div class="card-footer d-flex flex-stack pt-0">
												<div class="card-toolbar">
															<!--begin::Menu-->
															<button class="btn btn-sm btn-primary flex-shrink-0 me-2" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
																<!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="currentColor"></rect>
																		<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
																		<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
																		<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
																</svg>
																Click here for more actions
																<!--end::Svg Icon-->
															</button>

                                                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true" style="width: 250px !important;">
																
															    <!--begin::Menu separator-->
																<div class="separator mb-3 opacity-75"></div>
																<!--end::Menu separator-->
																
																<?php if($ct_qr_codes_numbers>=1){ ?>

																<!--begin::Menu item-->
																<div class="menu-item px-3">
																	<a href="<?php echo APP_URL.'download/download_ct_qr_codes/'.$event->Project_id; ?>" target="_blank" class="menu-link px-3">View CT QR </a>
																</div>
																<!--end::Menu item-->
																<?php } ?>

																<?php if($venue_qr_codes_numbers>=1){ ?>
																<!--begin::Menu item-->
																<div class="menu-item px-3">
																	<a href="<?php echo APP_URL.'download/download_venue_qr_codes/'.$event->Project_id; ?>" target="_blank" class="menu-link px-3">View Venue QR</a>
																</div>
																<!--end::Menu item-->

																<?php } ?>
																
																


																<?php if($ct_qr_codes_numbers>=1){ ?>
																<!--begin::Menu item-->
																<div class="menu-item px-3 ">
																			<a onclick="merge_ct_qr_codes('<?php echo $event->Project_id; ?>')"  class="menu-link px-3" style="color:#0095e8;">Merge CT's QR</a>
																		</div>
																		<!--end::Menu item-->
																<?php } ?>

																<?php if($venue_qr_codes_numbers>=1){ ?>
																<!--begin::Menu item-->
																<div class="menu-item px-3">
																	<a onclick="merge_venue_qr_codes('<?php echo $event->Project_id; ?>')" class="menu-link px-3" style="color:orange;">Merge Venue's QR</a>
																</div>
																<!--end::Menu item-->
																<?php } ?>

																<?php if($ct_qr_codes_numbers>=1){ ?>
																<!--begin::Menu item-->
																<div class="menu-item px-3">
																	<a onclick="reset_all_qr_codes('<?php echo $event->Project_id; ?>')" class="menu-link px-3" style="color:#f1416c;">Reset All QR</a>
																</div>
																<!--end::Menu item-->
																<?php } ?>
																<!--begin::Menu item-->
																<div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
																	<!--begin::Menu item-->
																	<a href="#" class="menu-link px-3">
																		<span class="menu-title">For Developers</span>
																		<span class="menu-arrow"></span>
																	</a>
																	<!--end::Menu item-->
																	<!--begin::Menu sub-->
																	<div class="menu-sub menu-sub-dropdown w-175px py-4" style="">
                                                                        
																	    <!--begin::Menu item-->
																	   <div class="menu-item px-3">
																			<a href="<?php echo APP_URL.'download/download_all_qr_codes/'.$event->Project_id; ?>"  class="menu-link px-3">View All QR</a>
																		</div>
																		<!--end::Menu item-->
																		<!--begin::Menu item-->
																		<div class="menu-item px-3">
																			<a href="<?php echo APP_URL.'download/download_qr_codes_for_developers/'.$event->Project_id; ?>"  class="menu-link px-3">CT QR Data</a>
																		</div>
																		<!--end::Menu item-->
																		<!--begin::Menu item-->
																		<div class="menu-item px-3">
																			<a href="<?php echo APP_URL.'download/download_qr_codes_for_developers/'.$event->Project_id; ?>" class="menu-link px-3">Venue QR DATA</a>
																		</div>
																		<!--end::Menu item-->
																		
																	</div>
																	<!--end::Menu sub-->
																</div>
																<!--end::Menu item-->

																<!--begin::Menu item-->
																<div class="menu-item px-3">
																	<div class="menu-content px-3 py-3">
																		<a class="btn btn-danger btn-sm px-4" onclick="replace_ct_qr_with_venue('<?php echo $event->Project_id; ?>')">Replace CT QR With Venue</a>
																	</div>
																</div>
																<!--end::Menu item-->
																
															</div>
															<!--begin::Menu 2-->
															
															<!--end::Menu 2-->
															<!--end::Menu-->
														</div>
												<!--end::Link-->
												</div>
											<!--end::Footer-->
										</div>

										<!--end::Card widget 14-->
									</div>
									<!--end::Col-->

                                <?php } ?>

                                    
								</div>
								<!--end::Row-->
								
							</div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					 <?php include_once "common/footer.php"; ?>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
	
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		
		<div id="toastr-container" class="toastr-bottom-right" style="display:none;">
			<div class="toastr toastr-success" aria-live="polite" style="">
				<button type="button" class="toastr-close-button" role="button">×</button>
				<div class="toastr-title" id="toaster_title">Success Alert!</div>
				<div class="toastr-message" id="toaster_message">Process has been started and it may take a while.</div>
			</div>
		</div>
		<!--begin::Javascript-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="assets/plugins/custom/fslightbox/fslightbox.bundle.js"></script>
		<script src="assets/plugins/custom/typedjs/typedjs.bundle.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/map.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/geodata/worldLow.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/geodata/continentsLow.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/geodata/usaLow.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/geodata/worldTimeZonesLow.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/geodata/worldTimeZoneAreasLow.js"></script>
		<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/widgets.bundle.js"></script>
		<script src="assets/js/custom/widgets.js"></script>
		<script src="assets/js/custom/apps/chat/chat.js"></script>
		<script src="assets/js/custom/intro.js"></script>
		<script src="assets/js/custom/utilities/modals/upgrade-plan.js"></script>
		<script src="assets/js/custom/utilities/modals/create-project/type.js"></script>
		<script src="assets/js/custom/utilities/modals/create-project/budget.js"></script>
		<script src="assets/js/custom/utilities/modals/create-project/settings.js"></script>
		<script src="assets/js/custom/utilities/modals/create-project/team.js"></script>
		<script src="assets/js/custom/utilities/modals/create-project/targets.js"></script>
		<script src="assets/js/custom/utilities/modals/create-project/files.js"></script>
		<script src="assets/js/custom/utilities/modals/create-project/complete.js"></script>
		<script src="assets/js/custom/utilities/modals/create-project/main.js"></script>
		<script src="assets/js/custom/utilities/modals/new-target.js"></script>
		<script src="assets/js/custom/utilities/modals/bidding.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
		<script>
			function show_alert_and_reload(status,message){
				
				if(status==="0"){
					$('.toastr').addClass('toastr-warning');
					$('.toastr').removeClass('toastr-success');
				}

				$('#toaster_title').text('Success Alert!');
				$('#toaster_message').text(message);
				document.getElementById('toastr-container').style.display = 'block';

				setTimeout(function(){
					document.getElementById('toastr-container').style.display = 'none';
					location.reload();
				}, 3000);

			}

			function merge_ct_qr_codes(Project_id){

				var FData = new FormData();
				FData.append("Project_id", Project_id);;

				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "http://localhost:8080/merge_ct_qr_codes",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_alert_and_reload(response.status,response.message);
					},
					error: function (e) {

					}
				});

            }

			function merge_venue_qr_codes(Project_id){

				var FData = new FormData();
				FData.append("Project_id", Project_id);;

				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "http://localhost:8080/merge_venue_qr_codes",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_alert_and_reload(response.status,response.message);
					},
					error: function (e) {

					}
				});

			}


			function reset_all_qr_codes(Project_id){

				Swal.fire({
					text: "Are you sure you want to reset all QR codes?",
					icon: "warning",
					showCancelButton: true,
					buttonsStyling: false,
					confirmButtonText: "Yes, delete!",
					cancelButtonText: "No, cancel",
					customClass: {
						confirmButton: "btn fw-bold btn-danger",
						cancelButton: "btn fw-bold btn-active-light-primary"
					}
				}).then(function (result) {
					if (result.value) {

						// Get form
						var FData = new FormData();
						FData.append("Project_id", Project_id);

						$.ajax({
							type: "POST",
							enctype: 'multipart/form-data',
							url: "http://localhost:8080/reset_all_qr_codes",
							data: FData,
							processData: false,
							contentType: false,
							dataType: "json",
							cache: false,
							timeout: 800000,
							success: function (data) {
				
								if(data.status==="1"){

									Swal.fire({
										text: "You have successfully reseted all QR codes!.",
										icon: "success",
										buttonsStyling: false,
										confirmButtonText: "Ok, got it!",
										customClass: {
											confirmButton: "btn fw-bold btn-primary",
										}
									}).then(function () {
										//
									});

								}else{

									Swal.fire({
										text: "Faild to reset QR codes!.",
										icon: "error",
										buttonsStyling: false,
										confirmButtonText: "Ok, got it!",
										customClass: {
											confirmButton: "btn fw-bold btn-primary",
										}
									});

								}
							
				
							},
							error: function (e) {
				
								Swal.fire({
									text: data.message,
									icon: "error",
									buttonsStyling: false,
									confirmButtonText: "Ok, got it!",
									customClass: {
										confirmButton: "btn fw-bold btn-primary",
									}
								});
							}
						});

						

					} else if (result.dismiss === 'cancel') {
						Swal.fire({
							text: "QR codes was not reseted.",
							icon: "error",
							buttonsStyling: false,
							confirmButtonText: "Ok, got it!",
							customClass: {
								confirmButton: "btn fw-bold btn-primary",
							}
						});
					}
				});


			}


			
			
		function replace_ct_qr_with_venue(Project_id){

			Swal.fire({
				text: "Are you sure you want to replace CT's QR code?",
				icon: "warning",
				showCancelButton: true,
				buttonsStyling: false,
				confirmButtonText: "Yes, delete!",
				cancelButtonText: "No, cancel",
				customClass: {
					confirmButton: "btn fw-bold btn-danger",
					cancelButton: "btn fw-bold btn-active-light-primary"
				}
			}).then(function (result) {
				if (result.value) {

					// Get form
					var FData = new FormData();
					FData.append("Project_id", Project_id);

					$.ajax({
						type: "POST",
						enctype: 'multipart/form-data',
						url: "http://localhost:8080/replace_ct_qr_with_venue",
						data: FData,
						processData: false,
						contentType: false,
						dataType: "json",
						cache: false,
						timeout: 800000,
						success: function (data) {

							if(data.status==="1"){

								Swal.fire({
									text: data.message,
									icon: "success",
									buttonsStyling: false,
									confirmButtonText: "Ok, got it!",
									customClass: {
										confirmButton: "btn fw-bold btn-primary",
									}
								}).then(function () {
									//
								});

							}else{

								Swal.fire({
									text: "Faild to replaced QR codes!.",
									icon: "error",
									buttonsStyling: false,
									confirmButtonText: "Ok, got it!",
									customClass: {
										confirmButton: "btn fw-bold btn-primary",
									}
								});

							}
						

						},
						error: function (e) {

							Swal.fire({
								text: data.message,
								icon: "error",
								buttonsStyling: false,
								confirmButtonText: "Ok, got it!",
								customClass: {
									confirmButton: "btn fw-bold btn-primary",
								}
							});
						}
					});

					

				} else if (result.dismiss === 'cancel') {
					Swal.fire({
						text: "QR codes was not reseted.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, got it!",
						customClass: {
							confirmButton: "btn fw-bold btn-primary",
						}
					});
				}
			});
        }
		</script>
	</body>
	<!--end::Body-->
</html>