<!DOCTYPE html>
<html lang="en">
   <!--begin::Head-->
	<?php include_once "common/header.php"; ?>
	<!--end::Head-->
   <!--begin::Page Vendor Stylesheets(used by this page)-->
   <link href="<?php echo APP_URL; ?>assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
   <!--end::Page Vendor Stylesheets-->

   <!--begin::Body-->
   <body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
      <!--begin::Main-->
      <!--begin::Root-->
      <div class="d-flex flex-column flex-root">
         <!--begin::Page-->
         <div class="page d-flex flex-row flex-column-fluid">
            <?php include_once "common/sidebar.php"; ?>
            <!--begin::Wrapper-->
            <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
               <!--begin::Header-->
               <div id="kt_header" style="" class="header align-items-stretch">
                  <!--begin::Container-->
                  <div class="container-fluid d-flex align-items-stretch justify-content-between">
                     <!--begin::Aside mobile toggle-->
                     <div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show aside menu">
                        <div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
                           <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                           <span class="svg-icon svg-icon-1">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                 <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor" />
                                 <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="currentColor" />
                              </svg>
                           </span>
                           <!--end::Svg Icon-->
                        </div>
                     </div>
                     <!--end::Aside mobile toggle-->
                     <!--begin::Mobile logo-->
                     <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
                        <a href="../../demo1/dist/index.html" class="d-lg-none">
                        <img alt="Logo" src="assets/media/logos/logo-2.svg" class="h-30px" />
                        </a>
                     </div>
                     <!--end::Mobile logo-->
                     <!--begin::Wrapper-->
                     <div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1">
                        <!--begin::Toolbar wrapper-->
                        <?php include('common/toolbar.php'); ?>
                        <!--end::Toolbar wrapper-->
                     </div>
                     <!--end::Wrapper-->
                  </div>
                  <!--end::Container-->
               </div>
               <!--end::Header-->
               <!--begin::Content-->
               <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                  <!--begin::Post-->
                  <div class="post d-flex flex-column-fluid" id="kt_post">
                     <!--begin::Container-->
                     <div id="kt_content_container" class="container-xxl">
                        <!--begin::Products-->
                        <div class="card card-flush">
                           <!--begin::Card header-->
                           <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                              <!--begin::Card title-->
                              <div class="card-title">
                                 <!--begin::Search-->
                                 <div class="d-flex align-items-center position-relative my-1">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                    <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                          <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                          <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor" />
                                       </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                    <input type="text" data-kt-ecommerce-order-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Search Events" />
                                 </div>
                                 <!--end::Search-->
                                 <!--begin::Export buttons-->
                                 <div id="kt_ecommerce_report_views_export" class="d-none"></div>
                                 <!--end::Export buttons-->
                              </div>
                              <!--end::Card title-->
                              <!--begin::Card toolbar-->
                              <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                                 <!--begin::Filter-->
                                 <div class="w-150px">
                                    <!--begin::Select2-->
                                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="E-Ticket" data-kt-ecommerce-order-filter="rating">
                                       <option></option>
                                       <option value="all">All</option>
                                       <option value="Yes">Yes</option>
                                       <option value="No">No</option>
                                    </select>
                                    <!--end::Select2-->
                                 </div>
                                 <!--end::Filter-->
                                 <!--begin::Export dropdown-->
                                 <button type="button" class="btn btn-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr078.svg-->
                                    <span class="svg-icon svg-icon-2">
                                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                          <rect opacity="0.3" x="12.75" y="4.25" width="12" height="2" rx="1" transform="rotate(90 12.75 4.25)" fill="currentColor" />
                                          <path d="M12.0573 6.11875L13.5203 7.87435C13.9121 8.34457 14.6232 8.37683 15.056 7.94401C15.4457 7.5543 15.4641 6.92836 15.0979 6.51643L12.4974 3.59084C12.0996 3.14332 11.4004 3.14332 11.0026 3.59084L8.40206 6.51643C8.0359 6.92836 8.0543 7.5543 8.44401 7.94401C8.87683 8.37683 9.58785 8.34458 9.9797 7.87435L11.4427 6.11875C11.6026 5.92684 11.8974 5.92684 12.0573 6.11875Z" fill="currentColor" />
                                          <path d="M18.75 8.25H17.75C17.1977 8.25 16.75 8.69772 16.75 9.25C16.75 9.80228 17.1977 10.25 17.75 10.25C18.3023 10.25 18.75 10.6977 18.75 11.25V18.25C18.75 18.8023 18.3023 19.25 17.75 19.25H5.75C5.19772 19.25 4.75 18.8023 4.75 18.25V11.25C4.75 10.6977 5.19771 10.25 5.75 10.25C6.30229 10.25 6.75 9.80228 6.75 9.25C6.75 8.69772 6.30229 8.25 5.75 8.25H4.75C3.64543 8.25 2.75 9.14543 2.75 10.25V19.25C2.75 20.3546 3.64543 21.25 4.75 21.25H18.75C19.8546 21.25 20.75 20.3546 20.75 19.25V10.25C20.75 9.14543 19.8546 8.25 18.75 8.25Z" fill="#C4C4C4" />
                                       </svg>
                                    </span>
                                    <!--end::Svg Icon-->Export Events
                                 </button>
                                 <!--begin::Menu-->
                                 <div id="kt_ecommerce_report_views_export_menu" class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-200px py-4" data-kt-menu="true">
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                       <a href="#" class="menu-link px-3" data-kt-ecommerce-export="copy">Copy to clipboard</a>
                                    </div>
                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                       <a href="#" class="menu-link px-3" data-kt-ecommerce-export="excel">Export as Excel</a>
                                    </div>
                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                       <a href="#" class="menu-link px-3" data-kt-ecommerce-export="csv">Export as CSV</a>
                                    </div>
                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                       <a href="#" class="menu-link px-3" data-kt-ecommerce-export="pdf">Export as PDF</a>
                                    </div>
                                    <!--end::Menu item-->
                                 </div>
                                 <!--end::Menu-->
                                 <!--end::Export dropdown-->
                              </div>
                              <!--end::Card toolbar-->
                           </div>
                           <!--end::Card header-->
                           <!--begin::Card body-->
                           <div class="card-body pt-0">
                              <!--begin::Table-->
                              <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_ecommerce_report_views_table">
                                 <!--begin::Table head-->
                                 <thead>
                                    <!--begin::Table row-->
                                    <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                       <th class="min-w-100px">Event Name</th>
                                       <th class="min-w-100px">Filling Ratio</th>
                                       <th class="min-w-70px">E-Ticket</th>
                                       <th class="min-w-70px">Bookings</th>
                                       <th class="min-w-100px">Seating Type</th>
                                       <th class="min-w-100px">Event's Action</th>
                                    </tr>
                                    <!--end::Table row-->
                                 </thead>
                                 <!--end::Table head-->
                                 <!--begin::Table body-->
                                 <tbody class="fw-bold text-gray-600">
                                    <!--begin::Table row-->
                                    <?php 
                                       $type='1';
                                       foreach($matchup_events as $matchup_event){ 
                                             $this_percentage = get_percentage($matchup_event->Allocation,$matchup_event->Purchased);    
                                       ?>
                                    <tr>
                                       <td data_value ='<?php echo $matchup_event->Project_id; ?>'>
                                          <a target="_blank" href="<?php echo  LIVE_WEB_URL.'/admin/Event/editevent/'.$matchup_event->Event_id; ?>" class="text-gray-800 text-hover-primary mb-1">
                                          <?php echo $matchup_event->Event_name; ?>
                                          </a>
                                       </td>
                                       <td>
                                          <div class="d-flex align-items-end flex-stack mb-1">
                                             <div class="d-flex align-items-center flex-column mt-3 w-100">
                                                <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                                                   <span class="fw-boldest fs-6 text-dark"><?php echo $matchup_event->Purchased; ?> Filled Out Of <?php echo $matchup_event->Allocation; ?></span>
                                                   <span class="fw-bolder fs-6 text-gray-400"><?php echo $this_percentage; ?>%</span>
                                                </div>
                                                <div class="h-8px mx-3 w-100 bg-light-<?php echo get_color_up($this_percentage); ?> rounded" style="height: 5px !important;">
                                                   <div class="bg-<?php echo get_color_up($this_percentage); ?> rounded h-8px" role="progressbar" style="width: <?php echo $this_percentage; ?>%;height: 5px !important;" aria-valuenow="<?php echo $this_percentage; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </td>
                                       <td class="pe-0">
                                          <span class="fw-bolder">
                                          <?php 
                                             if($matchup_event->Scanned_tickets=="No"){ 
                                             echo '<div class="badge badge-light-success">Yes</div>'; 
                                             }else{ 
                                             echo '<div class="badge badge-light-danger">No</div>'; 
                                             } 
                                             ?>
                                          </span>
                                       </td>
                                       <td >
                                          <span class="fw-bolder">
                                             <a target="_blank" href="<?php echo APP_URL.'view_all_bookings/'.$matchup_event->Project_id; ?>" >
                                               Bookings
                                             </a>  
                                          </span>
                                       </td>
                                       <td >
                                          <span class="fw-bolder">
                                             <a >
                                               <?php echo ucfirst($matchup_event->Seating_Type); ?>
                                             </a>  
                                          </span>
                                       </td>
                                       <td >
                                          
                                          

                                          <div class="card-toolbar">
                                             <!--begin::Menu-->
                                             <button class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
                                             Matchup's Actions
                                             </button>
                                             <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true" style="">
                                                
                                                <!--begin::Menu separator-->
                                                <div class="separator mb-3 opacity-75"></div>
                                                <!--end::Menu separator-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                   <a href="<?php echo APP_URL.'live_matchup_dashboard/'.$matchup_event->Project_id; ?>" target="_blank" class="menu-link px-3">
                                                      Matchup Screen
                                                   </a>
                                                </div>
                                                <!--end::Menu item-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
                                                   <!--begin::Menu item-->
                                                   <a href="#" class="menu-link px-3">
                                                      <span class="menu-title">Download Bookings</span>
                                                      <span class="menu-arrow"></span>
                                                   </a>
                                                   <!--end::Menu item-->
                                                   <!--begin::Menu sub-->
                                                   <div class="menu-sub menu-sub-dropdown w-175px py-4" style="">
                                                      <!--begin::Menu item-->
                                                      <div class="menu-item px-3">
                                                         <a href="<?php echo APP_URL.'download/download_bookings_type_1/'.$matchup_event->Project_id; ?>" target="_blank" class="menu-link px-3" style="color:#f1416c;">Single Tickets (Yes)</a>
                                                      </div>
                                                      <!--end::Menu item-->
                                                      <!--begin::Menu item-->
                                                      <div class="menu-item px-3">
                                                         <a href="<?php echo APP_URL.'download/download_bookings_type_2/'.$matchup_event->Project_id; ?>" target="_blank" class="menu-link px-3">Single Tickets (No)</a>
                                                      </div>
                                                      <!--end::Menu item-->
                                                      <!--begin::Menu item-->
                                                      <div class="menu-item px-3">
                                                         <a href="<?php echo APP_URL.'download/download_bookings_custom_type/'.$matchup_event->Project_id; ?>" target="_blank" class="menu-link px-3">Custom Format</a>
                                                      </div>
                                                      <!--end::Menu item-->
                                                      <!--begin::Menu item-->
                                                      <div class="menu-item px-3">
                                                         <a href="<?php echo APP_URL.'view_all_bookings/'.$matchup_event->Project_id; ?>" target="_blank" class="menu-link px-3">View All Bookings</a>
                                                      </div>
                                                      <!--end::Menu item-->
                                                   </div>
                                                   <!--end::Menu sub-->
                                                </div>
                                                <!--end::Menu item-->

                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
                                                   <!--begin::Menu item-->
                                                   <a href="#" class="menu-link px-3">
                                                      <span class="menu-title">Download Allocation</span>
                                                      <span class="menu-arrow"></span>
                                                   </a>
                                                   <!--end::Menu item-->
                                                   <!--begin::Menu sub-->
                                                   <div class="menu-sub menu-sub-dropdown w-175px py-4" style="">
                                                      <!--begin::Menu item-->
                                                      <div class="menu-item px-3 ">
                                                         <a href="<?php echo APP_URL.'download/download_left_allocation/'.$matchup_event->Project_id; ?>" target="_blank" class="menu-link px-3" style="color:#f1416c;">Left Allocations</a>
                                                      </div>
                                                      <!--end::Menu item-->
                                                      <!--begin::Menu item-->
                                                      <div class="menu-item px-3">
                                                         <a href="<?php echo APP_URL.'download/download_all_allocation/'.$matchup_event->Project_id; ?>" target="_blank" class="menu-link px-3">All Allocations</a>
                                                      </div>
                                                      <!--end::Menu item-->
                                                   </div>
                                                   <!--end::Menu sub-->
                                                </div>
                                                <!--end::Menu item-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                   <a href="<?php echo APP_URL.'Project_qc_report/'.$matchup_event->Project_id; ?>" target="_blank" class="menu-link px-3">QC Report</a>
                                                </div>
                                                <!--end::Menu item-->
                                                <!--begin::Menu separator-->
                                                <div class="separator mt-3 opacity-75"></div>
                                                <!--end::Menu separator-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                   <div class="menu-content px-3 py-3">
                                                      <a class="btn btn-danger btn-sm px-4" href="<?php echo APP_URL.'live_matchup_dashboard/'.$matchup_event->Project_id; ?>">Delete Matchup</a>
                                                   </div>
                                                </div>
                                                <!--end::Menu item-->
                                             </div>
                                             <!--begin::Menu 2-->
                                             
                                             <!--end::Menu 2-->
                                             <!--end::Menu-->
                                          </div>
                                          <!--end::Link-->
                                            

                                         
                                       </td>
                                    </tr>
                                    <?php } ?>
                                 </tbody>
                                 <!--end::Table body-->
                              </table>
                              <!--end::Table-->
                           </div>
                           <!--end::Card body-->
                        </div>
                        <!--end::Products-->
                     </div>
                     <!--end::Container-->
                  </div>
                  <!--end::Post-->
               </div>
               <!--end::Content-->
               <!--begin::Footer-->
               <?php include_once "common/footer.php"; ?>
               <!--end::Footer-->
            </div>
            <!--end::Wrapper-->
         </div>
         <!--end::Page-->
      </div>
      <!--end::Root-->
      <!--begin::Scrolltop-->
      <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
         <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
         <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
               <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor" />
               <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor" />
            </svg>
         </span>
         <!--end::Svg Icon-->
      </div>
      <!--end::Scrolltop-->
      <!--begin::Javascript-->
      <script>
         var file_title = 'View ALl Matchup Events';
      </script>
      <!--begin::Javascript-->
      <script>var hostUrl = "assets/";</script>
      <!--begin::Global Javascript Bundle(used by all pages)-->
      <script src="<?php echo APP_URL; ?>assets/plugins/global/plugins.bundle.js"></script>
      <script src="<?php echo APP_URL; ?>assets/js/scripts.bundle.js"></script>
      <!--end::Global Javascript Bundle-->
      <!--begin::Page Vendors Javascript(used by this page)-->
      <script src="<?php echo APP_URL; ?>assets/plugins/custom/datatables/datatables.bundle.js"></script>
      <!--end::Page Vendors Javascript-->
      <!--begin::Page Custom Javascript(used by this page)-->
      <script src="<?php echo APP_URL; ?>assets/js/custom/apps/ecommerce/reports/views/views.js"></script>
      <script src="<?php echo APP_URL; ?>assets/js/widgets.bundle.js"></script>
      <script src="<?php echo APP_URL; ?>assets/js/custom/widgets.js"></script>
      <script src="<?php echo APP_URL; ?>assets/js/custom/apps/chat/chat.js"></script>
      <script src="<?php echo APP_URL; ?>assets/js/custom/utilities/modals/upgrade-plan.js"></script>
      <script src="<?php echo APP_URL; ?>assets/js/custom/utilities/modals/create-app.js"></script>
      <script src="<?php echo APP_URL; ?>assets/js/custom/utilities/modals/users-search.js"></script>
      <!--end::Page Custom Javascript-->
      <!--end::Javascript-->
   </body>
   <!--end::Body-->
</html>