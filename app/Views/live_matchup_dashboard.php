
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<?php include_once "common/header.php"; ?>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				<?php include_once "common/sidebar.php"; ?>
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" style="" class="header align-items-stretch">
						<!--begin::Container-->
						<div class="container-fluid d-flex align-items-stretch justify-content-between">
							<!--begin::Aside mobile toggle-->
							<div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show aside menu">
								<div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
									<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
									<span class="svg-icon svg-icon-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor" />
											<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="currentColor" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</div>
							</div>
							<!--end::Aside mobile toggle-->
							<!--begin::Mobile logo-->
							<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
								<a href="/metronic8/demo1/../demo1/index.html" class="d-lg-none">
									<img alt="Logo" src="http://localhost:8080/assets/media/logos/logo-2.svg" class="h-30px" />
								</a>
							</div>
							<!--end::Mobile logo-->
							<!--begin::Wrapper-->
							<?php include('common/toolbar.php'); ?>
								
							
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content" style="padding-top: 0px;">
						
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container-xxl">
								<!--begin::Navbar-->
								<div class="card mb-5 mb-xl-10">
									<div class="card-body pt-9 pb-0">
										<!--begin::Details-->
										<div class="d-flex flex-wrap flex-sm-nowrap mb-3">
											<!--begin: Pic-->
											<div class="me-7 mb-4">
												<div class="symbol symbol-100px symbol-lg-260px symbol-fixed position-relative" >
													<img src="<?php echo $listing_images.$schedule_info['image']; ?>" alt="image" style="width: 245px !important;height: 160px !important;" />
													<?php if($schedule_info['status']=="1"){
                                                         echo '<div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"  data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Bookings are still open!"></div>';
													}else{
														echo '<div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-danger rounded-circle border border-4 border-white h-20px w-20px" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Bookings are closed!"></div>';
													}
													?>
													
												</div>
											</div>
											<!--end::Pic-->
											<!--begin::Info-->
											<div class="flex-grow-1">
												<!--begin::Title-->
												<div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
													<!--begin::User-->
													<div class="d-flex flex-column">
														<!--begin::Name-->
														<div class="d-flex align-items-center mb-2">
															<a href="#" class="text-gray-900 text-hover-primary fs-2 fw-bolder me-1"><?php echo $event_data->Event_name; ?></a>
															<?php if($event_data->Scanned_tickets=="Yes"){ 
																echo '<a href="#" class="btn btn-sm btn-danger fw-bolder ms-2 fs-8 py-1 px-3" >Scanned Tickets</a>';
																echo '<a href="#" class="btn btn-sm btn-danger fw-bolder ms-2 fs-8 py-1 px-3" >No</a>';
															}else{
																echo '<a href="#" class="btn btn-sm btn-success fw-bolder ms-2 fs-8 py-1 px-3" >Scanned Tickets</a>';
																echo '<a href="#" class="btn btn-sm btn-success fw-bolder ms-2 fs-8 py-1 px-3" >Yes</a>';
															} ?>
															
														</div>
														<!--end::Name-->
														<!--begin::Info-->
														<div class="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
															
															<a href="#" class="d-flex align-items-center text-gray-600 text-hover-primary me-5 mb-2" style="font-size: 16px;">
															<!--begin::Svg Icon | path: icons/duotune/general/gen018.svg-->
															<span class="svg-icon svg-icon-4 me-1">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																	<path opacity="0.3" d="M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z" fill="currentColor" />
																	<path d="M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z" fill="currentColor" />
																</svg>
															</span>
															<!--end::Svg Icon--><?php echo $schedule_info['address']; ?></a>

															<a href="#" class="d-flex align-items-center text-gray-600 text-hover-primary mb-2" style="font-size: 16px;">
															<!--begin::Svg Icon | path: icons/duotune/communication/com011.svg-->
															<span class="svg-icon svg-icon-4 me-1">
																<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-currency-pound" viewBox="0 0 16 16">
																   <path d="M4 8.585h1.969c.115.465.186.939.186 1.43 0 1.385-.736 2.496-2.075 2.771V14H12v-1.24H6.492v-.129c.825-.525 1.135-1.446 1.135-2.694 0-.465-.07-.913-.168-1.352h3.29v-.972H7.22c-.186-.723-.372-1.455-.372-2.247 0-1.274 1.047-2.066 2.58-2.066a5.32 5.32 0 0 1 2.103.465V2.456A5.629 5.629 0 0 0 9.348 2C6.865 2 5.322 3.291 5.322 5.366c0 .775.195 1.515.399 2.247H4v.972z"/>
																</svg>
															</span>
															<!--end::Svg Icon--><?php echo $schedule_info['price']; ?></a>
														</div>
														<!--end::Info-->
													</div>
													<!--end::User-->
													<!--begin::Actions-->
													<div class="d-flex my-4">
														   <!--begin::Indicator-->
														    <a class="btn btn-sm btn-primary me-2" href="<?php echo APP_URL.'view_all_bookings/'.$event_data->Project_id; ?>">
															<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-card-list" viewBox="0 0 16 16">
															 <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
															 <path d="M5 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 5 8zm0-2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0 5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-1-5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zM4 8a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm0 2.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
															</svg>
															View All Bookings</a>
															<!--end::Indicator-->

														    <!--begin::Indicator-->
															<a class="btn btn-sm btn-success me-2" onclick="pull_fresh_bookings('<?php echo $event_data->Schedule_id; ?>','<?php echo $event_data->Project_id; ?>')">
															<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cloud-arrow-down" viewBox="0 0 16 16">
															 <path fill-rule="evenodd" d="M7.646 10.854a.5.5 0 0 0 .708 0l2-2a.5.5 0 0 0-.708-.708L8.5 9.293V5.5a.5.5 0 0 0-1 0v3.793L6.354 8.146a.5.5 0 1 0-.708.708l2 2z"/>
															 <path d="M4.406 3.342A5.53 5.53 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773 16 11.569 14.502 13 12.687 13H3.781C1.708 13 0 11.366 0 9.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383zm.653.757c-.757.653-1.153 1.44-1.153 2.056v.448l-.445.049C2.064 6.805 1 7.952 1 9.318 1 10.785 2.23 12 3.781 12h8.906C13.98 12 15 10.988 15 9.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 4.825 10.328 3 8 3a4.53 4.53 0 0 0-2.941 1.1z"/>
															</svg>
															Pull Live Bookings</a>
															<!--end::Indicator-->
														
															<!--begin::Menu-->
															<div class="me-0">
																<button class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
																	<i class="bi bi-three-dots fs-3"></i>
																</button>
																<!--begin::Menu 3-->
																<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
																	<!--begin::Heading-->
																	<div class="menu-item px-3">
																		<div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Menu</div>
																	</div>
																	<!--end::Heading-->
																	
																	  <!--begin::Menu item-->
																	  <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
																		<!--begin::Menu item-->
																		<a href="#" class="menu-link px-3">
																			<span class="menu-title">Upload Files</span>
																			<span class="menu-arrow"></span>
																		</a>
																		<!--end::Menu item-->
																		<!--begin::Menu sub-->
																		<div class="menu-sub menu-sub-dropdown w-175px py-4" style="">
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="<?php echo APP_URL.'add_qr_codes'; ?>" target="_blank" class="menu-link px-3">Add QR Codes</a>
																			</div>
																			<!--end::Menu item-->
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="<?php echo APP_URL.'add_allocations'; ?>" target="_blank" class="menu-link px-3">Add Allocations</a>
																			</div>
																			<!--end::Menu item-->
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="<?php echo APP_URL.'add_bookings'; ?>" target="_blank" class="menu-link px-3">Add Bookings</a>
																			</div>
																			<!--end::Menu item-->
																			
																		</div>
																		<!--end::Menu sub-->
																		</div>
																		<!--end::Menu item-->

																		<!--begin::Menu item-->
																		<div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
																		<!--begin::Menu item-->
																		<a href="#" class="menu-link px-3">
																			<span class="menu-title">Download Bookings</span>
																			<span class="menu-arrow"></span>
																		</a>
																		<!--end::Menu item-->
																		<!--begin::Menu sub-->
																		<div class="menu-sub menu-sub-dropdown w-175px py-4" style="">
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="<?php echo APP_URL.'download/download_bookings_type_1/'.$event_data->Project_id; ?>" target="_blank" class="menu-link px-3" style="color:#f1416c;">Single Tickets (Yes)</a>
																			</div>
																			<!--end::Menu item-->
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="<?php echo APP_URL.'download/download_bookings_type_2/'.$event_data->Project_id; ?>" target="_blank" class="menu-link px-3">Single Tickets (No)</a>
																			</div>
																			<!--end::Menu item-->
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="<?php echo APP_URL.'download/download_bookings_custom_type/'.$event_data->Project_id; ?>" target="_blank" class="menu-link px-3">Custom Format</a>
																			</div>
																			<!--end::Menu item-->
																		</div>
																		<!--end::Menu sub-->
																		</div>
																		<!--end::Menu item-->

																		<!--begin::Menu item-->
																		<div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
																		<!--begin::Menu item-->
																		<a href="#" class="menu-link px-3">
																			<span class="menu-title">Download Allocation</span>
																			<span class="menu-arrow"></span>
																		</a>
																		<!--end::Menu item-->
																		<!--begin::Menu sub-->
																		<div class="menu-sub menu-sub-dropdown w-175px py-4" style="">
																			<!--begin::Menu item-->
																			<div class="menu-item px-3 ">
																				<a href="<?php echo APP_URL.'download/download_left_allocation/'.$event_data->Project_id; ?>" target="_blank" class="menu-link px-3" style="color:#f1416c;">Left Allocations</a>
																			</div>
																			<!--end::Menu item-->
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="<?php echo APP_URL.'download/download_all_allocation/'.$event_data->Project_id; ?>" target="_blank" class="menu-link px-3">All Allocations</a>
																			</div>
																			<!--end::Menu item-->
																		</div>
																		<!--end::Menu sub-->
																		</div>
																		<!--end::Menu item-->
                                                                        <!--begin::Menu item-->
																		<div class="menu-item px-3">
																			<a href="<?php echo APP_URL.'view_all_bookings/'.$event_data->Project_id; ?>" class="menu-link px-3">View All Bookings </a>
																		</div>
																		<!--end::Menu item-->
																	
													</div>
													<!--end::Menu 3-->
												</div>
												<!--end::Menu-->


															
													</div>
													<!--end::Actions-->
												</div>

												<!--end::Title-->
												<!--begin::Stats-->
												<div class="d-flex flex-wrap flex-stack">
													<!--begin::Wrapper-->
													<div class="d-flex flex-column flex-grow-1 pe-8">
														<!--begin::Stats-->
														<div class="d-flex flex-wrap">
															<!--begin::Stat-->
															<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3" style="background-color: #50cd89;">
																<!--begin::Number-->
																<div class="d-flex align-items-center">
																<span class="svg-icon svg-icon-4 me-1" style="line-height: 1;color: #f4f4f4;">
																<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-ticket-fill" viewBox="0 0 16 16">
																  <path d="M1.5 3A1.5 1.5 0 0 0 0 4.5V6a.5.5 0 0 0 .5.5 1.5 1.5 0 1 1 0 3 .5.5 0 0 0-.5.5v1.5A1.5 1.5 0 0 0 1.5 13h13a1.5 1.5 0 0 0 1.5-1.5V10a.5.5 0 0 0-.5-.5 1.5 1.5 0 0 1 0-3A.5.5 0 0 0 16 6V4.5A1.5 1.5 0 0 0 14.5 3h-13Z"/>
																</svg>
																</span>
                                                                <div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo $schedule_info['tickets']; ?>" data-kt-countup-prefix="" style="color:white;">0</div>
																</div>
																<!--end::Number-->
																<!--begin::Label-->
																<div class="fw-bold fs-6 text-gray-400" style="color: white !important;">Live Allocation</div>
																<!--end::Label-->
															</div>
															<!--end::Stat-->
															<!--begin::Stat-->
															<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3"  style="background-color: cadetblue;">
																<!--begin::Number-->
																<div class="d-flex align-items-center">
																	<span class="svg-icon svg-icon-4 me-1" style="line-height: 1;color: #f4f4f4;">
																	<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
																	  <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
																	  <path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
																	  <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
																	</svg>
																	</span>
																	<div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo ($schedule_info['purchased']); ?>" data-kt-countup-prefix="" >0</div>
																</div>
																<!--end::Number-->
																<!--begin::Label-->
																<div class="fw-bold fs-6 text-gray-400">Live Bookings</div>
																<!--end::Label-->
															</div>
															<!--end::Stat-->
															<!--begin::Stat-->
															<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3" style="background-color: #f00;">
																<!--begin::Number-->
																<div class="d-flex align-items-center">
																    <span class="svg-icon svg-icon-4 me-1" style="line-height: 1;color: #f4f4f4;">
																	<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-check-fill" viewBox="0 0 16 16">
																		<path fill-rule="evenodd" d="M15.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L12.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
																		<path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
																	</svg>
																	</span>
																	<div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo $schedule_info['ct_purchased']; ?>"data-kt-countup-prefix="" style="color:white;">0</div>
																</div>
																<!--end::Number-->
																<!--begin::Label-->
																<div class="fw-bold fs-6 text-gray-400" style="color: white !important;">Central Tickets</div>
																<!--end::Label-->
															</div>
															<!--end::Stat-->
															<!--begin::Stat-->
															<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3" style="background-color: #009ef7;">
																<!--begin::Number-->
																<div class="d-flex align-items-center">
																    <span class="svg-icon svg-icon-4 me-1" style="line-height: 1;color: #f4f4f4;">
																	<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus-fill" viewBox="0 0 16 16">
																	  <path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
																	  <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
																	</svg>
																	</span>
																	<div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo $schedule_info['aj_purchased']; ?>" data-kt-countup-prefix="" style="color:white;">0</div>
																</div>
																<!--end::Number-->
																<!--begin::Label-->
																<div class="fw-bold fs-6 text-gray-400" style="color: white !important;">AJTix</div>
																<!--end::Label-->
															</div>
															<!--end::Stat-->
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Wrapper-->
													<?php 
													$this_percentage = get_percentage($schedule_info['tickets'],$schedule_info['purchased']);
													?>
													<!--begin::Progress-->
													<div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
														<div class="d-flex justify-content-between w-100 mt-auto mb-2">
															<span class="fw-bold fs-6 text-gray-400">Filling Ratio</span>
															<span class="fw-bolder fs-6"><?php echo $this_percentage; ?>%</span>
														</div>
														<div class="h-5px mx-3 w-100 bg-light mb-3">
															<div class="bg-success rounded h-5px" role="progressbar" style="width: <?php echo $this_percentage; ?>%;" aria-valuenow="<?php echo $this_percentage; ?>" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div>
													<!--end::Progress-->
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
										</div>
										<!--end::Details-->
										
									</div>
								</div>
								<!--end::Navbar-->
								<!--begin::Basic info-->

								<!--begin::Row-->
								<div class="row g-xxl-9" >
									<!--begin::Col-->
									<div class="col-xxl-8">
										<!--begin::Earnings-->
										<div class="card card-xxl-stretch mb-5 mb-xxl-10">
											<!--begin::Header-->
											<div class="card-header" style="min-height: 60px;">
												<div class="card-title">
													<h3>Auto Matching</h3>

													<?php if($event_data->Seating_Type=="together"){ 
														echo '<a href="#" class="btn btn-sm btn-success fw-bolder ms-2 fs-8 py-1 px-3" >Seating Type</a>';
														echo '<a href="#" class="btn btn-sm btn-success fw-bolder ms-2 fs-8 py-1 px-3" >Together</a>';
													}else{
														echo '<a href="#" class="btn btn-sm btn-danger fw-bolder ms-2 fs-8 py-1 px-3" >Seating Type</a>';
														echo '<a href="#" class="btn btn-sm btn-danger fw-bolder ms-2 fs-8 py-1 px-3" >Alone</a>';
													} ?>
												</div>
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body pb-0">
												<?php 
												if($event_data->Allocation-$event_data->Purchased>=1){ 
												   echo '<span class="btn btn-sm btn-warning fw-bolder ms-2 fs-8 py-1 px-3" style="background-color: black !important;">Looks like we have got some '.round($event_data->Allocation-$event_data->Purchased,2).' extra allocation</span>';
												}elseif($event_data->Allocation-$event_data->Purchased<0){ 
													echo '<span class="btn btn-sm btn-danger fw-bolder ms-2 fs-8 py-1 px-3">Alert, Looks like we got less allocation digits.</span>';
												}else{
													echo '<span class="btn btn-sm btn-success fw-bolder ms-2 fs-8 py-1 px-3">Congratulation, we got exact same allocations & bookings digits</span>';
												} ?>
												
												<!--begin::Left Section-->
												<div class="d-flex flex-wrap justify-content-between pb-6">
													<!--begin::Row-->
													<div class="d-flex flex-wrap">
														<!--begin::Col-->
														<div class="border border-dashed border-gray-300 w-140px rounded my-3 p-4 me-6" >
															<span class="fs-2x fw-bolder text-gray-800 lh-1">
															<span class="" data-kt-countup="true" data-kt-countup-value="<?php echo $allocated_tickets->allocated_tickets; ?>" data-kt-countup-prefix="" style="color: #50cd89;">0</span></span>
															<span class="fs-6 fw-bold text-gray-400 d-block lh-1 pt-2" style="font-size: 1.0rem!important;">Allocated Seats</span>
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="border border-dashed border-gray-300 w-175px rounded my-3 p-4 me-6" >
															<span class="fs-2x fw-bolder text-gray-800 lh-1">
															<span class="" data-kt-countup="true" data-kt-countup-value="<?php echo $unallocated_tickets->unallocated_tickets; ?>" data-kt-countup-prefix="" style="color: #f1416c;">0</span></span>
															<span class="fs-6 fw-bold text-gray-400 d-block lh-1 pt-2" style="font-size: 1.0rem!important;">Unallocated Seats</span>
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="border border-dashed border-gray-300 w-160px rounded my-3 p-4 me-6" >
															<span class="fs-2x fw-bolder text-gray-800 lh-1">
															<span class="" data-kt-countup="true" data-kt-countup-value="<?php if(isset($allocated_bookings)){ echo $allocated_bookings->allocated_bookings; }else{ echo '0'; }  ?>" data-kt-countup-prefix="" style="color: #50cd89;">0</span></span>
															<span class="fs-6 fw-bold text-gray-400 d-block lh-1 pt-2" style="font-size: 1.0rem!important;">Allocated Bookings</span>
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="border border-dashed border-gray-300 w-175px rounded my-3 p-4 me-6" >
															<span class="fs-2x fw-bolder text-gray-800 lh-1">
															<span class="" data-kt-countup="true" data-kt-countup-value="<?php if(isset($unallocated_bookings)){ echo $unallocated_bookings->unallocated_bookings; }else{ echo '0'; }  ?>" data-kt-countup-prefix="" style="color: #f1416c;">0</span></span>
															<span class="fs-6 fw-bold text-gray-400 d-block lh-1 pt-2" style="font-size: 1.0rem!important;">Unallocated Bookings</span>
														</div>
														<!--end::Col-->

														<!--begin::Col-->
														<div class="w-190px rounded my-3 p-4 me-6">
															<span class="fs-2x fw-bolder text-gray-800 lh-1">
															<a class="btn btn-success px-6 flex-shrink-0 align-self-center" onclick="auto_matchup_everything('<?php echo $event_data->Project_id; ?>')">
															<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-steam" viewBox="0 0 16 16">
															 <path d="M.329 10.333A8.01 8.01 0 0 0 7.99 16C12.414 16 16 12.418 16 8s-3.586-8-8.009-8A8.006 8.006 0 0 0 0 7.468l.003.006 4.304 1.769A2.198 2.198 0 0 1 5.62 8.88l1.96-2.844-.001-.04a3.046 3.046 0 0 1 3.042-3.043 3.046 3.046 0 0 1 3.042 3.043 3.047 3.047 0 0 1-3.111 3.044l-2.804 2a2.223 2.223 0 0 1-3.075 2.11 2.217 2.217 0 0 1-1.312-1.568L.33 10.333Z"/>
															 <path d="M4.868 12.683a1.715 1.715 0 0 0 1.318-3.165 1.705 1.705 0 0 0-1.263-.02l1.023.424a1.261 1.261 0 1 1-.97 2.33l-.99-.41a1.7 1.7 0 0 0 .882.84Zm3.726-6.687a2.03 2.03 0 0 0 2.027 2.029 2.03 2.03 0 0 0 2.027-2.029 2.03 2.03 0 0 0-2.027-2.027 2.03 2.03 0 0 0-2.027 2.027Zm2.03-1.527a1.524 1.524 0 1 1-.002 3.048 1.524 1.524 0 0 1 .002-3.048Z"/>
															</svg>
															Auto Matchup Everything</a>
												        	<a class="btn btn-danger px-6 flex-shrink-0 align-self-center" onclick="reset_everything('<?php echo $event_data->Project_id; ?>')">
															<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-disc" viewBox="0 0 16 16">
															  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
															  <path d="M10 8a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 4a4 4 0 0 0-4 4 .5.5 0 0 1-1 0 5 5 0 0 1 5-5 .5.5 0 0 1 0 1zm4.5 3.5a.5.5 0 0 1 .5.5 5 5 0 0 1-5 5 .5.5 0 0 1 0-1 4 4 0 0 0 4-4 .5.5 0 0 1 .5-.5z"/>
															</svg>
															Reset Everything</a>
															<!-- <a class="btn btn-primary px-6 flex-shrink-0 align-self-center" onclick="turn_on_manual_mode()">Turn On Manual Mode</a> -->
															
														</div>
														<!--end::Col-->
													</div>
													<!--end::Row-->
													
												</div>
												<!--end::Left Section-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::Earnings-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xxl-4">
										<!--begin::Invoices-->
										<div class="card card-xxl-stretch mb-5 mb-xxl-10">
											<!--begin::Header-->
											<div class="card-header" style="min-height: 60px;">
												<div class="card-title">
													<h3 class="text-gray-800">Exact Matchup</h3>
												</div>
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body">
												<span class="fs-5 fw-bold text-gray-600 pb-6 d-block">Matchup for those seats which are eactly matching with user bookings.</span>
												<!--begin::Left Section-->
												<div class="d-flex align-self-center">
													<div class="flex-grow-1 me-3">
														<!--begin::Select-->
														<select class="form-select form-select-solid" data-control="select2" data-placeholder="Select Matching Bookings" data-hide-search="true" id="auto_matchup_seat">
															<option value=""></option>
															<?php foreach($get_exact_matching as $exact_matchup){  ?>
																<option value="<?php echo $exact_matchup['Quantity']; ?>"><?php echo '['.$exact_matchup['Quantity'].'s] - ['.$exact_matchup['total_seats'].' Purchased, '.$exact_matchup['total_bookings'].' Allocations]'; ?></option>
															<?php } ?>
															
														</select>
														<!--end::Select-->
													</div>
													<!--begin::Action-->
													<button type="button" class="btn btn-primary btn-icon flex-shrink-0">
														<!--begin::Svg Icon | path: icons/duotune/arrows/arr065.svg-->
														<span class="svg-icon svg-icon-1">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<rect opacity="0.5" x="11" y="18" width="13" height="2" rx="1" transform="rotate(-90 11 18)" fill="currentColor" />
																<path d="M11.4343 15.4343L7.25 11.25C6.83579 10.8358 6.16421 10.8358 5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75L11.2929 18.2929C11.6834 18.6834 12.3166 18.6834 12.7071 18.2929L18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25C17.8358 10.8358 17.1642 10.8358 16.75 11.25L12.5657 15.4343C12.2533 15.7467 11.7467 15.7467 11.4343 15.4343Z" fill="currentColor" />
															</svg>
														</span>
														<!--end::Svg Icon-->
													</button>
													<!--end::Action-->
												</div>
												<!--end::Left Section-->

												<!--begin::Left Section-->
												<div class="d-flex align-self-center" style="margin-top: 30px;">
													<div class="flex-grow-1 me-3">
													  <a href="#" class="btn btn-primary px-6 flex-shrink-0 align-self-center" onclick="assign_exact_matching_seats('<?php echo $event_data->Project_id; ?>')">
													  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check2-all" viewBox="0 0 16 16">
														  <path d="M12.354 4.354a.5.5 0 0 0-.708-.708L5 10.293 1.854 7.146a.5.5 0 1 0-.708.708l3.5 3.5a.5.5 0 0 0 .708 0l7-7zm-4.208 7-.896-.897.707-.707.543.543 6.646-6.647a.5.5 0 0 1 .708.708l-7 7a.5.5 0 0 1-.708 0z"/>
														  <path d="m5.354 7.146.896.897-.707.707-.897-.896a.5.5 0 1 1 .708-.708z"/>
														</svg>
														Assign Exact Matching Seats</a>
													</div>
												</div>
												<!--end::Left Section-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::Invoices-->
									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->

								<div class="row g-xxl-9" id="manual_matchup" style="padding-bottom: 30px;padding-left: 15px !important;padding-right: 15px !important;">

								<!--begin::Tables Widget 9-->
								<div class="card mb-5 mb-xl-8" >
									<!--begin::Header-->
									<div class="card-header border-0 pt-5">
										<h3 class="card-title align-items-start flex-column">
											<span class="card-label fw-bolder fs-3 mb-1">Allocation Statestics</span>
											
											<span class="text-muted mt-1 fw-bold fs-7">
											  <a href="<?php echo APP_URL.'download/download_left_allocation/'.$event_data->Project_id; ?>" class="btn btn-sm btn-danger fw-bolder" style="padding-top: 5px;padding-bottom: 5px;"><?php echo $unallocated_tickets->unallocated_tickets; ?> Seats Left</a>
											</span>
										</h3>
										<div class="card-toolbar" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Click to add a more allocation">
											<a class="btn btn-sm btn-light btn-active-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_invite_friends">
											<!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->
											<span class="svg-icon svg-icon-3">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor" />
													<rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor" />
												</svg>
											</span>
											<!--end::Svg Icon-->Add Allocation</a>
										</div>
									</div>
									<!--end::Header-->
									<!--begin::Body-->
									<div class="card-body py-3">
										<!--begin::Table container-->
										<div class="table-responsive">
											<!--begin::Table-->
											<table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
												<!--begin::Table head-->
												<thead>
													<tr class="fw-bolder text-muted">
														<!-- <th class="w-25px">
															<div class="form-check form-check-sm form-check-custom form-check-solid">
																<input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-9-check" />
															</div>
														</th> -->
														<th class="min-w-70px">Pair Of</th>
														<th class="min-w-150px">Total Seats</th>
														<th class="min-w-150px">Left Seats</th>
														<th class="min-w-150px">Left Percentage</th>
														<th class="min-w-100px text-end">Actions</th>
													</tr>
												</thead>
												<!--end::Table head-->
												<!--begin::Table body-->
												<tbody>
												<?php 
												if(!empty($left_allocations)){ 
													foreach($left_allocations as $allocation){

														$this_a_percentage = get_percentage($allocation['total_seats'],$allocation['left_seats']);
													?>
													<tr>
														<!-- <td>
															<div class="form-check form-check-sm form-check-custom form-check-solid">
																<input class="form-check-input widget-9-check" type="checkbox" value="1" />
															</div>
														</td> -->
														<td>
															<div class="d-flex align-items-center">
																<div class="symbol symbol-45px me-5">
																	<div class="symbol symbol-45px me-5">
																		<span class="symbol-label bg-light-danger text-danger fw-bolder"><?php echo $allocation['Quantity']; ?></span>
																	</div>
																</div>
															</div>
														</td>
														<td>
															<a href="#" class="btn btn-sm btn-success fw-bolder ms-2 fs-8 py-1 px-3"><?php echo $allocation['total_seats'].' Total Seats'; ?></a>
														</td>
														<td>
															<a href="#" class="btn btn-sm btn-danger fw-bolder ms-2 fs-8 py-1 px-3"><?php echo $allocation['left_seats'].' Left Seats'; ?></a>
														</td>
														<td class="text-end">
															<div class="d-flex flex-column w-100 me-2">
																<div class="d-flex flex-stack mb-2">
																	<span class="text-muted me-2 fs-7 fw-bold"><?php echo $this_a_percentage; ?>% Left</span>
																</div>
																<div class="progress h-6px w-100">
																	<div class="progress-bar bg-<?php echo get_color_down($this_a_percentage); ?>" role="progressbar" style="width: <?php echo $this_a_percentage; ?>%" aria-valuenow="<?php echo $this_a_percentage; ?>" aria-valuemin="0" aria-valuemax="100"></div>
																</div>
															</div>
														</td>
														<td>
															<div class="d-flex justify-content-end flex-shrink-0">
																<a class="btn btn-primary px-6 flex-shrink-0 align-self-center" onclick="auto_matchup_left_seats('<?php echo $allocation['Quantity']; ?>','<?php echo $event_data->Project_id; ?>')">
																	<!--begin::Svg Icon | path: icons/duotune/general/gen019.svg-->
																	<span class="svg-icon svg-icon-3">
																		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																			<path d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z" fill="currentColor" />
																			<path opacity="0.3" d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z" fill="currentColor" />
																		</svg>
																	</span>
																	<!--end::Svg Icon-->
																	Assign Left Seats 
																</a>
															</div>
														</td>
													</tr>
												<?php } } ?>
													
												</tbody>
												<!--end::Table body-->
											</table>
											<!--end::Table-->
										</div>
										<!--end::Table container-->
									</div>
									<!--begin::Body-->
								</div>
								<!--end::Tables Widget 9-->

								</div>
								
							<!--end::Container-->
						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<?php include_once "common/footer.php"; ?>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		
		
		<!--begin::Javascript-->
		
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="http://localhost:8080/assets/plugins/global/plugins.bundle.js"></script>
		<script src="http://localhost:8080/assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="http://localhost:8080/assets/js/forms/create_matchup_live.js"></script>
		
		<script src="http://localhost:8080/assets/js/widgets.bundle.js"></script>
		<script src="http://localhost:8080/assets/js/custom/widgets.js"></script>

		

		<!--end::Javascript-->
        <script>

			function turn_on_manual_mode(){
				document.getElementById('manual_matchup').style.display = 'block';
				
            }
			
			
			function pull_fresh_bookings(schedule_id,project_id){

				var FData = new FormData();
				FData.append("schedule_id", schedule_id);
				FData.append("project_id", project_id);
				
				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "<?php echo APP_URL; ?>pull_live_bookings",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_message(response)
					},
					error: function (e) {

					}
				});
            }


			function auto_matchup_everything(Project_id)
			{

				var FData = new FormData();
				FData.append("Project_id", Project_id);
				
				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "<?php echo APP_URL; ?>auto_matchup_everything",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_message(response)
					},
					error: function (e) {

					}
				});


			}

			function auto_matchup_left_seats(Quantity,Project_id)
			{

				const toster_data = ({"status": '3',"message": 'Assigning Left Tickets...'});
				show_message(toster_data);


				var FData = new FormData();
				FData.append("Project_id", Project_id);
				FData.append("Quantity", Quantity);

				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "<?php echo APP_URL; ?>auto_matchup_left_seats",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_message(response);
					},
					error: function (e) {

					}
				});


			}

			function assign_exact_matching_seats(Project_id)
			{

				var Quantity = document.getElementById('auto_matchup_seat').value;
				if(Quantity){

					const toster_data = ({"status": '3',"message": 'Assigning Tickets...'});
				    show_message(toster_data);

				    var FData = new FormData();
					FData.append("Project_id", Project_id);
					FData.append("Quantity", Quantity);

					$.ajax({
						type: "POST",
						enctype: 'multipart/form-data',
						url: "<?php echo APP_URL; ?>auto_matchup_exact_seats",
						data: FData,
						processData: false,
						contentType: false,
						dataType: "json",
						cache: false,
						timeout: 800000,
						success: function (response) {
							show_message(response)
						},
						error: function (e) {

						}
					});

				}else{
					const toster_data = ({"status": '0',"message": 'Please select any bookings from exact matching'});
				    show_message(toster_data);	
				}
				
				


			}
			

			function reset_everything(Project_id){

				var FData = new FormData();
				FData.append("Project_id", Project_id);

				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "<?php echo APP_URL; ?>reset_everything",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_message(response)
					},
					error: function (e) {

					}
				});


			}

			

			
		</script>
	</body>
	<!--end::Body-->
</html>