
<!DOCTYPE html>

<html lang="en">
	<!--begin::Head-->
	<head><base href="../">
		<title>Seats Matchup - Seat Management System</title>
		<meta charset="utf-8" />
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Vendor Stylesheets(used by this page)-->
		<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/vis-timeline/vis-timeline.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendor Stylesheets-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
	</head>
	<style>
		.button5 {
			border: none;
			color: white;
			padding: 15px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			font-size: 16px;
			margin: 4px 2px;
			border-radius: 15%;
        }
</style>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				<?php include_once "common/sidebar.php"; ?>
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" style="" class="header align-items-stretch">
						<!--begin::Container-->
						<div class="container-fluid d-flex align-items-stretch justify-content-between">
							<!--begin::Aside mobile toggle-->
							<div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show aside menu">
								<div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
									<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
									<span class="svg-icon svg-icon-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor" />
											<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="currentColor" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</div>
							</div>
							<!--end::Aside mobile toggle-->
							<!--begin::Mobile logo-->
							<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
								<a href="../../demo1/dist/index.html" class="d-lg-none">
									<img alt="Logo" src="assets/media/logos/logo-2.svg" class="h-30px" />
								</a>
							</div>
							<!--end::Mobile logo-->
							<!--begin::Wrapper-->
							<div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1">
								<!--begin::Navbar-->
								<div class="d-flex align-items-stretch" id="kt_header_nav">
									<!--begin::Menu wrapper-->
									<div class="header-menu align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_header_menu_mobile_toggle" data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav'}">
										<!--begin::Menu-->
										<div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 align-items-stretch" id="#kt_header_menu" data-kt-menu="true">
											<!--begin::Breadcrumb-->
                                            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                                                <!--begin::Item-->
                                                <li class="breadcrumb-item text-muted">
                                                    <a href="#" class="text-muted text-hover-primary">Home</a>
                                                </li>
                                                <!--end::Item-->
                                                <!--begin::Item-->
                                                <li class="breadcrumb-item">
                                                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                                                </li>
                                                <!--end::Item-->
                                                <!--begin::Item-->
                                                <li class="breadcrumb-item text-muted">
                                                    <a href="#" class="text-muted text-hover-primary">Events</a>
                                                </li>
                                                <!--end::Item-->
                                                <!--begin::Item-->
                                                <li class="breadcrumb-item">
                                                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                                                </li>
                                                <!--end::Item-->
                                                <!--begin::Item-->
                                                <li class="breadcrumb-item text-muted">
                                                    <a href="#" class="text-muted text-hover-primary">Seat Matchup</a>
                                                </li>
                                                <!--end::Item-->
                                            </ul>
                                            <!--end::Breadcrumb-->

                                            
											
										</div>
										<!--end::Menu-->
									</div>
									<!--end::Menu wrapper-->
								</div>
								<!--end::Navbar-->
								<?php include('common/toolbar.php'); ?>
							</div>
							<!--end::Wrapper-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1"><?php echo $event->Name; ?></h1>
									<!--end::Title-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								<div class="d-flex align-items-center gap-2 gap-lg-3">
									
									<!--begin::Primary button-->
									<a href="#" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_create_campaign">Create New Event</a>
									<!--end::Primary button-->
								</div>
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container-xxl">
								<!--begin::Row-->
								<div class="row g-5 g-xl-10">
									<!--begin::Col-->
									<div class="col-xl-4 mb-xl-10">
										<!--begin::Lists Widget 19-->
										<div class="card card-flush h-xl-100">
											<!--begin::Heading-->
											<div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-250px" style="background-image:url('assets/media/svg/shapes/top-green.png')">
												<!--begin::Title-->
												<h3 class="card-title align-items-start flex-column text-white pt-15">
													<span class="fw-bolder fs-2x mb-3">Hello, Admin</span>
													<div class="fs-4 text-white">
														<span class="opacity-75 ">Are you having a good day?</span>
													</div>
												</h3>
												<!--end::Title-->
												<!--begin::Toolbar-->
												<div class="card-toolbar pt-5">
													<!--begin::Menu-->
													<button class="btn btn-sm btn-icon btn-active-color-primary btn-color-white bg-white bg-opacity-25 bg-hover-opacity-100 bg-hover-white bg-active-opacity-25 w-20px h-20px" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
														<!--begin::Svg Icon | path: icons/duotune/general/gen052.svg-->
														<span class="svg-icon svg-icon-4">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<rect x="10" y="10" width="4" height="4" rx="2" fill="black" />
																<rect x="17" y="10" width="4" height="4" rx="2" fill="black" />
																<rect x="3" y="10" width="4" height="4" rx="2" fill="black" />
															</svg>
														</span>
														<!--end::Svg Icon-->
													</button>
													
													<!--end::Menu-->
												</div>
												<!--end::Toolbar-->
											</div>
											<!--end::Heading-->
											<!--begin::Body-->
											<div class="card-body mt-n20">
												<!--begin::Stats-->
												<div class="mt-n20 position-relative">
													<!--begin::Row-->
													<div class="row g-3 g-lg-6">
														<!--begin::Col-->
														<div class="col-6">
															<!--begin::Items-->
															<div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
																<!--begin::Symbol-->
																<div class="symbol symbol-30px me-5 mb-8">
																	<span class="symbol-label">
																		
																		<!--begin::Svg Icon | path: icons/duotune/general/gen020.svg-->
																		<span class="svg-icon svg-icon-1 svg-icon-primary">
																			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																				<path d="M14 18V16H10V18L9 20H15L14 18Z" fill="black" />
																				<path opacity="0.3" d="M20 4H17V3C17 2.4 16.6 2 16 2H8C7.4 2 7 2.4 7 3V4H4C3.4 4 3 4.4 3 5V9C3 11.2 4.8 13 7 13C8.2 14.2 8.8 14.8 10 16H14C15.2 14.8 15.8 14.2 17 13C19.2 13 21 11.2 21 9V5C21 4.4 20.6 4 20 4ZM5 9V6H7V11C5.9 11 5 10.1 5 9ZM19 9C19 10.1 18.1 11 17 11V6H19V9ZM17 21V22H7V21C7 20.4 7.4 20 8 20H16C16.6 20 17 20.4 17 21ZM10 9C9.4 9 9 8.6 9 8V5C9 4.4 9.4 4 10 4C10.6 4 11 4.4 11 5V8C11 8.6 10.6 9 10 9ZM10 13C9.4 13 9 12.6 9 12V11C9 10.4 9.4 10 10 10C10.6 10 11 10.4 11 11V12C11 12.6 10.6 13 10 13Z" fill="black" />
																			</svg>
																		</span>
																		<!--end::Svg Icon-->
																	</span>
																</div>
																<!--end::Symbol-->
																<!--begin::Stats-->
																<div class="m-0">
																	<!--begin::Number-->
																	<span class="text-gray-700 fw-boldest d-block fs-2qx lh-1 ls-n1 mb-1">
																	        <?php  
																	        if(empty($allocated_tickets->allocated_tickets)){ 
																			   echo '0'; 
																			}else{ 
																				echo $allocated_tickets->allocated_tickets; 
																			} ?>

																	</span>
																	<!--end::Number-->
																	<!--begin::Desc-->
																	<span class="text-gray-500 fw-bold fs-6">Allocated</span>
																	<!--end::Desc-->
																</div>
																<!--end::Stats-->
															</div>
															<!--end::Items-->
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="col-6">
															<!--begin::Items-->
															<div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
																<!--begin::Symbol-->
																<div class="symbol symbol-30px me-5 mb-8">
																	<span class="symbol-label">
																		<!--begin::Svg Icon | path: icons/duotune/finance/fin001.svg-->
																		<span class="svg-icon svg-icon-1 svg-icon-primary">
																			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																				<path d="M20 19.725V18.725C20 18.125 19.6 17.725 19 17.725H5C4.4 17.725 4 18.125 4 18.725V19.725H3C2.4 19.725 2 20.125 2 20.725V21.725H22V20.725C22 20.125 21.6 19.725 21 19.725H20Z" fill="black" />
																				<path opacity="0.3" d="M22 6.725V7.725C22 8.325 21.6 8.725 21 8.725H18C18.6 8.725 19 9.125 19 9.725C19 10.325 18.6 10.725 18 10.725V15.725C18.6 15.725 19 16.125 19 16.725V17.725H15V16.725C15 16.125 15.4 15.725 16 15.725V10.725C15.4 10.725 15 10.325 15 9.725C15 9.125 15.4 8.725 16 8.725H13C13.6 8.725 14 9.125 14 9.725C14 10.325 13.6 10.725 13 10.725V15.725C13.6 15.725 14 16.125 14 16.725V17.725H10V16.725C10 16.125 10.4 15.725 11 15.725V10.725C10.4 10.725 10 10.325 10 9.725C10 9.125 10.4 8.725 11 8.725H8C8.6 8.725 9 9.125 9 9.725C9 10.325 8.6 10.725 8 10.725V15.725C8.6 15.725 9 16.125 9 16.725V17.725H5V16.725C5 16.125 5.4 15.725 6 15.725V10.725C5.4 10.725 5 10.325 5 9.725C5 9.125 5.4 8.725 6 8.725H3C2.4 8.725 2 8.325 2 7.725V6.725L11 2.225C11.6 1.925 12.4 1.925 13.1 2.225L22 6.725ZM12 3.725C11.2 3.725 10.5 4.425 10.5 5.225C10.5 6.025 11.2 6.725 12 6.725C12.8 6.725 13.5 6.025 13.5 5.225C13.5 4.425 12.8 3.725 12 3.725Z" fill="black" />
																			</svg>
																		</span>
																		<!--end::Svg Icon-->
																	</span>
																</div>
																<!--end::Symbol-->
																<!--begin::Stats-->
																<div class="m-0">
																	<!--begin::Number-->
																	<span class="text-gray-700 fw-boldest d-block fs-2qx lh-1 ls-n1 mb-1"> 
																		    <?php  
																	        if(empty($unallocated_tickets->unallocated_tickets)){ 
																			   echo '0'; 
																			}else{ 
																				echo $unallocated_tickets->unallocated_tickets; 
																			} ?></span>
																	<!--end::Number-->
																	<!--begin::Desc-->
																	<span class="text-gray-500 fw-bold fs-6">Unallocated</span>
																	<!--end::Desc-->
																</div>
																<!--end::Stats-->
															</div>
															<!--end::Items-->
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="col-6">
															<!--begin::Items-->
															<div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
																<!--begin::Symbol-->
																<div class="symbol symbol-30px me-5 mb-8">
																	<span class="symbol-label">
																		<!--begin::Svg Icon | path: icons/duotune/medicine/med005.svg-->
																		<span class="svg-icon svg-icon-1 svg-icon-primary">
																			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																				<path opacity="0.3" d="M17.9061 13H11.2061C11.2061 12.4 10.8061 12 10.2061 12C9.60605 12 9.20605 12.4 9.20605 13H6.50606L9.20605 8.40002V4C8.60605 4 8.20605 3.6 8.20605 3C8.20605 2.4 8.60605 2 9.20605 2H15.2061C15.8061 2 16.2061 2.4 16.2061 3C16.2061 3.6 15.8061 4 15.2061 4V8.40002L17.9061 13ZM13.2061 9C12.6061 9 12.2061 9.4 12.2061 10C12.2061 10.6 12.6061 11 13.2061 11C13.8061 11 14.2061 10.6 14.2061 10C14.2061 9.4 13.8061 9 13.2061 9Z" fill="black" />
																				<path d="M18.9061 22H5.40605C3.60605 22 2.40606 20 3.30606 18.4L6.40605 13H9.10605C9.10605 13.6 9.50605 14 10.106 14C10.706 14 11.106 13.6 11.106 13H17.8061L20.9061 18.4C21.9061 20 20.8061 22 18.9061 22ZM14.2061 15C13.1061 15 12.2061 15.9 12.2061 17C12.2061 18.1 13.1061 19 14.2061 19C15.3061 19 16.2061 18.1 16.2061 17C16.2061 15.9 15.3061 15 14.2061 15Z" fill="black" />
																			</svg>
																		</span>
																		<!--end::Svg Icon-->
																	</span>
																</div>
																<!--end::Symbol-->
																<!--begin::Stats-->
																<div class="m-0">
																	<!--begin::Number-->
																	<span class="text-gray-700 fw-boldest d-block fs-2qx lh-1 ls-n1 mb-1">0</span>
																	<!--end::Number-->
																	<!--begin::Desc-->
																	<span class="text-gray-500 fw-bold fs-6">Duplicates</span>
																	<!--end::Desc-->
																</div>
																<!--end::Stats-->
															</div>
															<!--end::Items-->
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="col-6">
															<!--begin::Items-->
															<div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
																<!--begin::Symbol-->
																<div class="symbol symbol-30px me-5 mb-8">
																	<span class="symbol-label">
																		<!--begin::Svg Icon | path: icons/duotune/general/gen013.svg-->
																		<span class="svg-icon svg-icon-1 svg-icon-primary">
																			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																				<path opacity="0.3" d="M20.9 12.9C20.3 12.9 19.9 12.5 19.9 11.9C19.9 11.3 20.3 10.9 20.9 10.9H21.8C21.3 6.2 17.6 2.4 12.9 2V2.9C12.9 3.5 12.5 3.9 11.9 3.9C11.3 3.9 10.9 3.5 10.9 2.9V2C6.19999 2.5 2.4 6.2 2 10.9H2.89999C3.49999 10.9 3.89999 11.3 3.89999 11.9C3.89999 12.5 3.49999 12.9 2.89999 12.9H2C2.5 17.6 6.19999 21.4 10.9 21.8V20.9C10.9 20.3 11.3 19.9 11.9 19.9C12.5 19.9 12.9 20.3 12.9 20.9V21.8C17.6 21.3 21.4 17.6 21.8 12.9H20.9Z" fill="black" />
																				<path d="M16.9 10.9H13.6C13.4 10.6 13.2 10.4 12.9 10.2V5.90002C12.9 5.30002 12.5 4.90002 11.9 4.90002C11.3 4.90002 10.9 5.30002 10.9 5.90002V10.2C10.6 10.4 10.4 10.6 10.2 10.9H9.89999C9.29999 10.9 8.89999 11.3 8.89999 11.9C8.89999 12.5 9.29999 12.9 9.89999 12.9H10.2C10.4 13.2 10.6 13.4 10.9 13.6V13.9C10.9 14.5 11.3 14.9 11.9 14.9C12.5 14.9 12.9 14.5 12.9 13.9V13.6C13.2 13.4 13.4 13.2 13.6 12.9H16.9C17.5 12.9 17.9 12.5 17.9 11.9C17.9 11.3 17.5 10.9 16.9 10.9Z" fill="black" />
																			</svg>
																		</span>
																		<!--end::Svg Icon-->
																	</span>
																</div>
																<!--end::Symbol-->
																<!--begin::Stats-->
																<div class="m-0">
																	<!--begin::Number-->
																	<span class="text-gray-700 fw-boldest d-block fs-2qx lh-1 ls-n1 mb-1">0</span>
																	<!--end::Number-->
																	<!--begin::Desc-->
																	<span class="text-gray-500 fw-bold fs-6">Errors</span>
																	<!--end::Desc-->
																</div>
																<!--end::Stats-->
															</div>
															<!--end::Items-->
														</div>
														<!--end::Col-->
													</div>
													<!--end::Row-->
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::Lists Widget 19-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-8 mb-5 mb-xl-10">
										<!--begin::Row-->
										<div class="row g-5 g-xl-10">
											<!--begin::Col-->
											<div class="col-xl-6 mb-xl-10">
												<!--begin::Slider Widget 1-->
												<div id="kt_sliders_widget_1_slider" class="card card-flush carousel carousel-custom carousel-stretch slide h-xl-100" data-bs-ride="carousel" data-bs-interval="5000">
													<!--begin::Header-->
													<div class="card-header pt-5">
														<!--begin::Title-->
														<h4 class="card-title d-flex align-items-start flex-column">
															<span class="card-label fw-bolder text-gray-800">Allocation Pairs</span>
															<span class="text-gray-400 mt-1 fw-bolder fs-7">Total : <?php  
																	        if(empty($unallocated_tickets->unallocated_tickets)){ 
																			   echo '0 Seats'; 
																			}else{ 
																				echo $unallocated_tickets->unallocated_tickets. ' Seats'; 
																			} ?></span>
														</h4>
														<!--end::Title-->
														<!--begin::Toolbar-->
														<div class="card-toolbar">
															<!--begin::Carousel Indicators-->
															<ol class="p-0 m-0 carousel-indicators carousel-indicators-bullet carousel-indicators-active-primary">
															<?php 
															$count=0;
															foreach($left_matching as $matching){ ?>
																<li data-bs-target="#kt_sliders_widget_1_slider" data-bs-slide-to="<?php echo $count; ?>" class="<?php if($count=="0"){ echo 'active'; } ?> ms-1"></li>
															<?php 
															$count++;
															} ?>
																
															</ol>
															<!--end::Carousel Indicators-->
														</div>
														<!--end::Toolbar-->
													</div>
													<!--end::Header-->
													<!--begin::Body-->
													<div class="card-body pt-6" style="padding-top: 25px !important;">
														<!--begin::Carousel-->
														<div class="carousel-inner mt-n5">

														<?php 
														$count=0;
														foreach($left_matching as $matching){ 
															
															$this_percentage=get_percentage($matching['total_seats'],$matching['left_seats']);
														?>
														
															<!--begin::Item-->
															<div class="carousel-item <?php if($count=="0"){ echo 'active show'; } ?> ">
																<!--begin::Wrapper-->
																<div class="d-flex align-items-center mb-9">
																	<!--begin::Chart-->
																	<div class="w-80px flex-shrink-0 me-2">
																	   <button class="button5 btn-<?php echo get_color_down($this_percentage); ?>"><?php echo $this_percentage.'%'; ?></button>
																	</div>
																	<!--end::Chart-->
																	<!--begin::Info-->
																	<div class="m-0">
																		<!--begin::Subtitle-->
																		<h4 class="fw-bolder text-gray-800 mb-3"><?php echo 'Pair of '.$matching['Quantity']; ?></h4>
																		<!--end::Subtitle-->
																		<!--begin::Items-->
																		<div class="d-flex d-grid gap-5">
																			<!--begin::Item-->
																			<div class="d-flex flex-column flex-shrink-0 me-4">
																				<!--begin::Section-->
																				<span class="d-flex align-items-center fs-7 fw-bolder text-gray-400 mb-2">
																				<!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
																				<span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
																					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																						<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
																						<path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="black" />
																					</svg>
																				</span>
																				<!--end::Svg Icon-->
																				<div class="badge badge-light-success"><?php echo $matching['total_seats'].' Total Seats'; ?></div>
																				</span>
																				<!--end::Section-->
																				
																			</div>
																			<!--end::Item-->
																			<!--begin::Item-->
																			<div class="d-flex flex-column flex-shrink-0">
																				<!--begin::Section-->
																				<span class="d-flex align-items-center fs-7 fw-bolder text-gray-400 mb-2">
																				<!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
																				<span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
																					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																						<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
																						<path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="black" />
																					</svg>
																				</span>
																				<!--end::Svg Icon-->
																				<div class="badge badge-light-danger"><?php echo $matching['left_seats'].' Left Seats'; ?></div>
																				</span>
																				<!--end::Section-->
																				
																			</div>
																			<!--end::Item-->
																		</div>
																		<!--end::Items-->
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::Wrapper-->
																<?php if($matching['left_seats']>=1){ ?>
																<!--begin::Action-->
																<div class="mb-1">
																	<a class="btn btn-sm btn-primary" onclick="auto_assign_allocation('<?php echo $event->Event_id; ?>','<?php echo $matching['Quantity']; ?>')">Find Bookings For <?php echo $matching['left_seats'].' Seats'; ?></a>
																</div>
																<!--end::Action-->
																
																<?php } ?>
																
															</div>
															<!--end::Item-->

														<?php $count++; } ?>

															
														</div>
														<!--end::Carousel-->
													</div>
													<!--end::Body-->
												</div>
												<!--end::Slider Widget 1-->
											</div>
											<!--end::Col-->
											<!--begin::Col-->
											<div class="col-xl-6 mb-5 mb-xl-10">
												<!--begin::Slider Widget 2-->
												<div id="kt_sliders_widget_2_slider" class="card card-flush carousel carousel-custom carousel-stretch slide h-xl-100" data-bs-ride="carousel" data-bs-interval="5500">
													<!--begin::Header-->
													<div class="card-header pt-5">
														<!--begin::Title-->
														<h4 class="card-title d-flex align-items-start flex-column">
															<span class="card-label fw-bolder text-gray-800">Exact Matching</span>
															<span class="text-gray-400 mt-1 fw-bolder fs-7">Total Seats : <?php echo $exact_total_seats.' Seats';   ?></span>
														</h4>
														<!--end::Title-->
														<!--begin::Toolbar-->
														<div class="card-toolbar">
															<!--begin::Carousel Indicators-->
															<ol class="p-0 m-0 carousel-indicators carousel-indicators-bullet carousel-indicators-active-success">
															<?php 
															$count=0;
															foreach($exact_matching as $matching){ ?>
																<li data-bs-target="#kt_sliders_widget_2_slider" data-bs-slide-to="<?php echo $count; ?>" class="<?php if($count=="0"){ echo 'active'; } ?> ms-1"></li>
															<?php 
															$count++;
															} ?>
															</ol>
															<!--end::Carousel Indicators-->
														</div>
														<!--end::Toolbar-->
													</div>
													<!--end::Header-->
													<!--begin::Body-->
													<div class="card-body pt-6" >
														<!--begin::Carousel-->
														<div class="carousel-inner">
														<?php 
														if(count($exact_matching)>=1){ 
														
													
															$count=0;
															foreach($exact_matching as $matching){ ?>
															<!--begin::Item-->
															<div class="carousel-item <?php if($count=="0"){ echo 'active show'; } ?>">
																<!--begin::Wrapper-->
																<div class="d-flex align-items-center mb-9" >
																	
																	<!--begin::Info-->
																	<div class="m-0">
																		<!--begin::Subtitle-->
																		<h4 class="fw-bolder text-gray-800 mb-3"><?php echo 'Pair Of '.$matching['Quantity']; ?></h4>
																		<!--end::Subtitle-->
																		<!--begin::Items-->
																		<div class="d-flex d-grid gap-5">
																			<!--begin::Item-->
																			<div class="d-flex flex-column flex-shrink-0 me-4">
																				<!--begin::Section-->
																				<span class="d-flex align-items-center fs-7 fw-bolder text-gray-400 mb-2">
																				<!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
																				<span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
																					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																						<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
																						<path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="black" />
																					</svg>
																				</span>
																				<!--end::Svg Icon-->
																				<div class="badge badge-light-success"><?php echo 'We Got '.$matching['total_seats'].' Seats'; ?></div>
																				</span>
																				<!--end::Section-->
																				
																			</div>
																			<!--end::Item-->
																			<!--begin::Item-->
																			<div class="d-flex flex-column flex-shrink-0">
																				<!--begin::Section-->
																				<span class="d-flex align-items-center fs-7 fw-bolder text-gray-400 mb-2">
																				<!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
																				<span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
																					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																						<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
																						<path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="black" />
																					</svg>
																				</span>
																				<!--end::Svg Icon-->
																				<div class="badge badge-light-primary"><?php echo 'User Booked '.$matching['total_bookings'].' Tickets'; ?></div>
																				</span>
																				<!--end::Section-->
																				
																			</div>
																			<!--end::Item-->
																		</div>
																		<!--end::Items-->
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::Wrapper-->
																<!--begin::Action-->
																<div class="mb-1">
																	<a class="btn btn-sm btn-success" onclick="exact_matching('<?php echo $event->Event_id; ?>','<?php echo $matching['Quantity']; ?>')">
																		Assign <?php  if($matching['total_bookings']>=$matching['total_seats']){ echo $matching['total_seats'].' Tickets'; }else{ echo $matching['total_bookings'].' Tickets'; }  ?>
																	</a>
																</div>
																<!--end::Action-->
															</div>
															<!--end::Item-->
															<?php $count++; }  } ?>
														</div>
														<!--end::Carousel-->
													</div>
													<!--end::Body-->
												</div>
												<!--end::Slider Widget 2-->
											</div>
											<!--end::Col-->
										</div>
										<!--end::Row-->
										<!--begin::Engage widget 4-->
										<div class="card" style="background: #1C325E;">
											<!--begin::Body-->
											<div class="card-body d-flex ps-xl-15">
												<!--begin::Action-->
												<div class="m-0">
													<!--begin::Title-->
													<div class="position-relative fs-2x z-index-2 fw-bolder text-white mb-7">
													<span class="me-2">
													<span class="position-relative d-inline-block text-warning">
														<a href="#" class="text-warning opacity-75-hover">Auto Matching Possibility : 90%</a>
														<!--begin::Separator-->
														<span class="position-absolute opacity-50 bottom-0 start-0 border-4 border-warning border-bottom w-100"></span>
														<!--end::Separator-->
													</span></span>
													<br />Feel free to us custom matchup</div>
													<!--end::Title-->
													<!--begin::Action-->
													<div class="mb-3">
														<a class="btn btn-success fw-bold me-2" onclick="auto_assign_allocation('<?php echo $event->Event_id; ?>','lazy')">Assign Everything</a>
														<a class="btn btn-danger fw-bold me-2" onclick="reset_everything('<?php echo $event->Event_id; ?>')">Reset Everything</a>
														<a href="#" class="btn btn-color-white bg-body bg-opacity-15 bg-hover-opacity-25 fw-bold">How it Works?</a>
													</div>
													<!--begin::Action-->
												</div>
												<!--begin::Action-->
												<!--begin::Illustration-->
												<img src="assets/media/illustrations/sigma-1/17-dark.png" class="position-absolute me-3 bottom-0 end-0 h-200px" alt="" />
												<!--end::Illustration-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::Engage widget 4-->
									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row g-5 g-xl-10">
									
									
									<!--begin::Col-->
									<div class="col-xl-4 mb-xl-10">
										<!--begin::List widget 20-->
										<div class="card h-xl-100">
											<!--begin::Header-->
											<div class="card-header border-0 pt-5">
												<h3 class="card-title align-items-start flex-column">
													<span class="card-label fw-bolder text-dark">Bookings </span>
													<span class="text-muted mt-1 fw-bold fs-7">Total Bookings : 319</span>
												</h3>
												<!--begin::Toolbar-->
												<div class="card-toolbar">
													<a href="#" class="btn btn-sm btn-light">Amend Bookings</a>
												</div>
												<!--end::Toolbar-->
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body pt-6">
												<?php 
												
												 foreach($user_bookings as $booking){ 
													
													$this_percentage=get_percentage($booking['total_bookings'],$booking['left_bookings']);
													?>
												        <div class="d-flex flex-stack">
															<!--begin::Wrapper-->
															<div class="d-flex align-items-center me-3">
																<!--begin::Symbol-->
																<div class="symbol symbol-40px me-4">
																	<div class="symbol-label fs-2 fw-bold bg-<?php echo get_color_up($this_percentage); ?> text-inverse-<?php echo get_color_up($this_percentage); ?>"><?php echo $booking['Quantity']; ?></div>
																</div>
																<!--end::Symbol-->
																<!--begin::Section-->
																<div class="flex-grow-1">
																	<a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0"><?php echo $booking['total_bookings'].' Total Tickets'; ?></a>
																	<span class="text-gray-400 fw-bold d-block fs-6"><?php echo $booking['left_bookings'].' Left Tickets'; ?></span>
																</div>
																<!--end::Section-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Statistics-->
															<div class="d-flex align-items-center w-100 mw-125px">
																<!--begin::Progress-->
																<div class="progress h-6px w-100 me-2 bg-light-<?php echo get_color_down($this_percentage); ?>">
																	<div class="progress-bar bg-<?php echo get_color_down($this_percentage); ?>" role="progressbar" style="width: <?php echo $this_percentage.'%'; ?>" aria-valuenow="<?php echo $this_percentage; ?>" aria-valuemin="0" aria-valuemax="100"></div>
																</div>
																<!--end::Progress-->
																<!--begin::Value-->
																<span class="text-gray-400 fw-bold"><?php echo $this_percentage.'%'; ?></span>
																<!--end::Value-->
															</div>
															<!--end::Statistics-->
														</div>

													
													<!--begin::Separator-->
													<div class="separator separator-dashed my-4"></div>
													<!--end::Separator-->

												<?php } ?>
												
												
												
												
											</div>
											<!--end::Body-->
										</div>
										<!--end::List widget 20-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-4 mb-xl-10">
										<!--begin::List widget 20-->
										<div class="card h-xl-100">
											<!--begin::Header-->
											<div class="card-header border-0 pt-5">
												<h3 class="card-title align-items-start flex-column">
													<span class="card-label fw-bolder text-dark">Allocation</span>
													<span class="text-muted mt-1 fw-bold fs-7">Total Seats : 319</span>
												</h3>
												<!--begin::Toolbar-->
												<div class="card-toolbar">
													<a href="#" class="btn btn-sm btn-light">Amend Allocation</a>
												</div>
												<!--end::Toolbar-->
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body pt-6">
												<?php foreach($user_alloations as $alloation){ 
													
													$this_percentage=get_percentage($alloation['total_seats'],$alloation['left_seats']);
													?>
												
												        <div class="d-flex flex-stack">
															<!--begin::Wrapper-->
															<div class="d-flex align-items-center me-3">
																<!--begin::Symbol-->
																<div class="symbol symbol-40px me-4">
																	<div class="symbol-label fs-2 fw-bold bg-<?php echo get_color_up($this_percentage); ?> text-inverse-<?php echo get_color_up($this_percentage); ?>"><?php echo $alloation['Quantity']; ?></div>
																</div>
																<!--end::Symbol-->
																<!--begin::Section-->
																<div class="flex-grow-1">
																	<a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0"><?php echo $alloation['total_seats'].' Total Seats'; ?></a>
																	<span class="text-gray-400 fw-bold d-block fs-6"><?php echo $alloation['left_seats'].' Left Seats'; ?></span>
																</div>
																<!--end::Section-->
															</div>
															<!--end::Wrapper-->
															<!--begin::Statistics-->
															<div class="d-flex align-items-center w-100 mw-125px">
																<!--begin::Progress-->
																<div class="progress h-6px w-100 me-2 bg-light-<?php echo get_color_down($this_percentage); ?>">
																	<div class="progress-bar bg-<?php echo get_color_down($this_percentage); ?>" role="progressbar" style="width: <?php echo $this_percentage.'%'; ?>" aria-valuenow="<?php echo $this_percentage; ?>" aria-valuemin="0" aria-valuemax="100"></div>
																</div>
																<!--end::Progress-->
																<!--begin::Value-->
																<span class="text-gray-400 fw-bold"><?php echo $this_percentage.'%'; ?></span>
																<!--end::Value-->
															</div>
															<!--end::Statistics-->
														</div>
													<!--begin::Separator-->
													<div class="separator separator-dashed my-4"></div>
													<!--end::Separator-->

												<?php } ?>
												
												
												
												
											</div>
											<!--end::Body-->
										</div>
										<!--end::List widget 20-->
									</div>
									<!--end::Col-->

									<!--begin::Col-->
									<div class="col-xxl-4">
										<!--begin::Forms widget 1-->
										<div class="card h-xl-100">
											<!--begin::Header-->
											<div class="card-header position-relative min-h-50px p-0 border-bottom-2">
												<!--begin::Nav-->
												<ul class="nav nav-pills nav-pills-custom d-flex position-relative w-100">
													<!--begin::Item-->
													<li class="nav-item mx-0 p-0 w-50">
														<!--begin::Link-->
														<a class="nav-link btn btn-color-muted active border-0 h-100 px-0" data-bs-toggle="pill" id="kt_forms_widget_1_tab_1" href="#kt_forms_widget_1_tab_content_1">
															<!--begin::Subtitle-->
															<span class="nav-text fw-bolder fs-4 mb-3">Custom Matchup</span>
															<!--end::Subtitle-->
															<!--begin::Bullet-->
															<span class="bullet-custom position-absolute z-index-2 w-100 h-2px top-100 bottom-n100 bg-primary rounded"></span>
															<!--end::Bullet-->
														</a>
														<!--end::Link-->
													</li>
													<!--end::Item-->
													
												</ul>
												<!--end::Nav-->
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body">
												<!--begin::Tab Content-->
												<div class="tab-content">
													<!--begin::Tap pane-->
													<div class="tab-pane fade active show" id="kt_forms_widget_1_tab_content_1">
														<!--begin::Input group-->
														<div class="form-floating border rounded mb-7">
															<select class="form-select form-select-transparent select2-hidden-accessible" id="kt_forms_widget_1_select_1" data-select2-id="select2-data-kt_forms_widget_1_select_1" tabindex="-1" aria-hidden="true">
																<option data-kt-select2-icon="https://cdn.iconscout.com/icon/free/png-256/ticket-1855957-1574163.png">Please Select Any Pair</option>
																<?php foreach($user_alloations as $alloation){ ?>
                                                                     <option value="<?php echo $alloation['Quantity']; ?>" data-kt-select2-icon="https://cdn.iconscout.com/icon/free/png-256/ticket-1855957-1574163.png"><?php echo $alloation['Quantity'].' - '.$alloation['left_seats'].' Seats Left'; ?></option>
                                                                <?php } ?>
															</select>
															<label for="floatingInputValue">Allocation Pairs</label>
														</div>
														<!--end::Input group-->
														<!--begin::Row-->
														<div class="row mb-7">
															<!--begin::Col-->
															<div class="col-12">
																<!--begin::Input group-->
																<div class="form-floating">
																	<input type="number" class="form-control text-gray-800 fw-bolder" placeholder="00.0" id="floatingInputValue" value="5">
																	<label for="floatingInputValue">Matchup With Who Booked X Tickets</label>
																</div>
																<!--end::Input group-->
															</div>
															<!--end::Col-->
															
														</div>
														<!--end::Row-->
														<!--begin::Action-->
														<div class="d-flex align-items-end">
															<a class="btn btn-primary fs-3 w-100" onclick="custom_matchup()">Start Matching</a>
														</div>
														<!--end::Action-->
													</div>
													<!--end::Tap pane-->
													
												</div>
												<!--end::Tab Content-->
											</div>
											<!--end: Card Body-->
										</div>
										<!--end::Forms widget 1-->
									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
								
							</div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<?php include_once "common/footer.php"; ?>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		
		
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		<!--begin::Modals-->
		
		<div id="toastr-container" class="toastr-bottom-right" style="display:none;">
			<div class="toastr toastr-success" aria-live="polite" style="">
				<button type="button" class="toastr-close-button" role="button">×</button>
				<div class="toastr-title" id="toaster_title">Success Alert!</div>
				<div class="toastr-message" id="toaster_message">Process has been started and it may take a while.</div>
			</div>
		</div>

		<!--begin::Javascript-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<script src="assets/plugins/custom/vis-timeline/vis-timeline.bundle.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
		<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/custom/layout-builder/layout-builder.js"></script>
		<script src="assets/js/widgets.bundle.js"></script>
		<script src="assets/js/custom/widgets.js"></script>
		<script src="assets/js/custom/apps/chat/chat.js"></script>
		<script src="assets/js/custom/intro.js"></script>
		<script src="assets/js/custom/utilities/modals/upgrade-plan.js"></script>
		<script src="assets/js/custom/utilities/modals/create-app.js"></script>
		<script src="assets/js/custom/utilities/modals/users-search.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->

		<script src="assets/js/custom/intro.js"></script>
		
		<script>
			function show_alert(status,message){
				
				if(status==="0"){
					$('.toastr').addClass('toastr-warning');
					$('.toastr').removeClass('toastr-success');
				}

				$('#toaster_title').text('Success Alert!');
				$('#toaster_message').text(message);
				document.getElementById('toastr-container').style.display = 'block';

				setTimeout(function(){
					document.getElementById('toastr-container').style.display = 'none';
				}, 5000);

			}

			function show_alert_and_reload(status,message){
				
				if(status==="0"){
					$('.toastr').addClass('toastr-warning');
					$('.toastr').removeClass('toastr-success');
				}

				$('#toaster_title').text('Success Alert!');
				$('#toaster_message').text(message);
				document.getElementById('toastr-container').style.display = 'block';

				setTimeout(function(){
					document.getElementById('toastr-container').style.display = 'none';
					location.reload();
				}, 3000);

			}

			function custom_matchup(){

				var Allocation_Quantity = document.getElementById("kt_forms_widget_1_select_1").value;
				var Booking_Quantity = document.getElementById("floatingInputValue").value;
				var Event_id = '<?php echo $event->Event_id; ?>';
				
				var FData = new FormData();
				FData.append("Allocation_Quantity", Allocation_Quantity);
				FData.append("Booking_Quantity", Booking_Quantity);
				FData.append("Event_id", Event_id);
				

				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "http://localhost:8080/matchup_seats/custom_matchup_allocation",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_alert_and_reload(response.status,response.message);
					},
					error: function (e) {
		
					}
				});


			}

			function exact_matching(Event_id,Quantity){

				var FData = new FormData();
				FData.append("Event_id", Event_id);
				FData.append("Quantity", Quantity);
				
				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "http://localhost:8080/matchup_seats/assign_seats_for_exact_matching",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_alert_and_reload(response.status,response.message);
					},
					error: function (e) {
		
					}
				});


			}

			
			function auto_assign_allocation(Event_id,Quantity){

				var FData = new FormData();
				FData.append("Event_id", Event_id);
				FData.append("Quantity", Quantity);

				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "http://localhost:8080/matchup_seats/auto_assign_allocation",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_alert_and_reload(response.status,response.message);
					},
					error: function (e) {

					}
				});
            }

			function reset_everything(Event_id){

				var FData = new FormData();
				FData.append("Event_id", Event_id);
				
				$.ajax({
					type: "POST",
					enctype: 'multipart/form-data',
					url: "http://localhost:8080/matchup_seats/reset_everything",
					data: FData,
					processData: false,
					contentType: false,
					dataType: "json",
					cache: false,
					timeout: 800000,
					success: function (response) {
						show_alert_and_reload(response.status,response.message);
					},
					error: function (e) {
		
					}
				});


			}

		</script>

	</body>
	<!--end::Body-->
</html>