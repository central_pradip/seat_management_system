
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	 <?php include_once "common/header.php"; ?>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				<?php include_once "common/sidebar.php"; ?>
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" style="" class="header align-items-stretch">
						<!--begin::Container-->
						<div class="container-fluid d-flex align-items-stretch justify-content-between">
							<!--begin::Aside mobile toggle-->
							<div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show aside menu">
								<div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
									<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
									<span class="svg-icon svg-icon-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor" />
											<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="currentColor" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</div>
							</div>
							<!--end::Aside mobile toggle-->
							<!--begin::Mobile logo-->
							<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
								<a href="/metronic8/demo1/../demo1/index.html" class="d-lg-none">
									<img alt="Logo" src="assets/media/logos/logo-2.svg" class="h-30px" />
								</a>
							</div>
							<!--end::Mobile logo-->
							<!--begin::Wrapper-->
							<?php include('common/toolbar.php'); ?>
							<!--end::Wrapper-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container-xxl">
								<!--begin::Row-->
								<div class="row g-5 g-xl-8">
									<!--begin::Col-->
									<div class="col-xl-4">
										<!--begin::Mixed Widget 1-->
										<div class="card card-xl-stretch mb-xl-8">
											<!--begin::Body-->
											<div class="card-body p-0">
												<!--begin::Header-->
												<div class="px-9 pt-7 card-rounded h-275px w-100 bg-primary">
													<!--begin::Heading-->
													<div class="d-flex flex-stack">
														<h3 class="m-0 text-white fw-bolder fs-3">Allocations Summary</h3>
													</div>
													<!--end::Heading-->
													<!--begin::Balance-->
													<div class="d-flex text-center flex-column text-white pt-8">
														<span class="fw-bold fs-7">Total Allocations</span>
														<span class="fw-bolder fs-2x pt-1"><?php echo $event_data->Allocation; ?></span>
													</div>
													<!--end::Balance-->
												</div>
												<!--end::Header-->
												<!--begin::Items-->
												<div class="bg-body shadow-sm card-rounded mx-9 mb-9 px-6 py-9 position-relative z-index-1" style="margin-top: -100px">
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-6">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/maps/map004.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<path opacity="0.3" d="M18.4 5.59998C21.9 9.09998 21.9 14.8 18.4 18.3C14.9 21.8 9.2 21.8 5.7 18.3L18.4 5.59998Z" fill="currentColor" />
																		<path d="M12 2C6.5 2 2 6.5 2 12C2 17.5 6.5 22 12 22C17.5 22 22 17.5 22 12C22 6.5 17.5 2 12 2ZM19.9 11H13V8.8999C14.9 8.6999 16.7 8.00005 18.1 6.80005C19.1 8.00005 19.7 9.4 19.9 11ZM11 19.8999C9.7 19.6999 8.39999 19.2 7.39999 18.5C8.49999 17.7 9.7 17.2001 11 17.1001V19.8999ZM5.89999 6.90002C7.39999 8.10002 9.2 8.8 11 9V11.1001H4.10001C4.30001 9.4001 4.89999 8.00002 5.89999 6.90002ZM7.39999 5.5C8.49999 4.7 9.7 4.19998 11 4.09998V7C9.7 6.8 8.39999 6.3 7.39999 5.5ZM13 17.1001C14.3 17.3001 15.6 17.8 16.6 18.5C15.5 19.3 14.3 19.7999 13 19.8999V17.1001ZM13 4.09998C14.3 4.29998 15.6 4.8 16.6 5.5C15.5 6.3 14.3 6.80002 13 6.90002V4.09998ZM4.10001 13H11V15.1001C9.1 15.3001 7.29999 16 5.89999 17.2C4.89999 16 4.30001 14.6 4.10001 13ZM18.1 17.1001C16.6 15.9001 14.8 15.2 13 15V12.8999H19.9C19.7 14.5999 19.1 16.0001 18.1 17.1001Z" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Pending</a>
																<div class="text-gray-400 fw-bold fs-7">Allocations</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1"><?php echo $allocations_summary['pending_allocations']; ?></div>
																
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-6">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" />
																		<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" />
																		<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" />
																		<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Allocated</a>
																<div class="text-gray-400 fw-bold fs-7">Allocations</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1"><?php echo $allocations_summary['allocated_allocations']; ?></div>
																
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-6">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/electronics/elc005.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<path opacity="0.3" d="M15 19H7C5.9 19 5 18.1 5 17V7C5 5.9 5.9 5 7 5H15C16.1 5 17 5.9 17 7V17C17 18.1 16.1 19 15 19Z" fill="currentColor" />
																		<path d="M8.5 2H13.4C14 2 14.5 2.4 14.6 3L14.9 5H6.89999L7.2 3C7.4 2.4 7.9 2 8.5 2ZM7.3 21C7.4 21.6 7.9 22 8.5 22H13.4C14 22 14.5 21.6 14.6 21L14.9 19H6.89999L7.3 21ZM18.3 10.2C18.5 9.39995 18.5 8.49995 18.3 7.69995C18.2 7.29995 17.8 6.90002 17.3 6.90002H17V10.9H17.3C17.8 11 18.2 10.7 18.3 10.2Z" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Unallocated</a>
																<div class="text-gray-400 fw-bold fs-7">Allocations</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1"><?php echo $allocations_summary['unallocated_allocations']; ?></div>
																
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/general/gen005.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z" fill="currentColor" />
																		<rect x="7" y="17" width="6" height="2" rx="1" fill="currentColor" />
																		<rect x="7" y="12" width="10" height="2" rx="1" fill="currentColor" />
																		<rect x="7" y="7" width="6" height="2" rx="1" fill="currentColor" />
																		<path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Duplicates</a>
																<div class="text-gray-400 fw-bold fs-7">Allocations</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1">0</div>
																
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
												</div>
												<!--end::Items-->
											</div>
											<!--end::Body-->
											<!--begin::Footer-->
											<div class="card-footer d-flex flex-center py-5">
												<!--begin::Item-->
												<div class="d-flex align-items-center flex-shrink-0 me-7 me-lg-12">
													<!--begin::Bullet-->
													<span class="bullet bullet-dot bg-primary me-2 h-10px w-10px"></span>
													<!--end::Bullet-->
													<!--begin::Label-->
													<span class="fw-bold text-gray-400 fs-6">Total : </span>
													<!--end::Label-->
												</div>
												<!--ed::Item-->
												<!--begin::Item-->
												<div class="d-flex align-items-center flex-shrink-0">
													<!--begin::Bullet-->
													<span class="bullet bullet-dot bg-success me-2 h-10px w-10px"></span>
													<!--end::Bullet-->
													<!--begin::Label-->
													<span class="fw-bold text-gray-400 fs-6"><?php echo $event_data->Allocation; ?></span>
													<!--end::Label-->
												</div>
												<!--ed::Item-->
											</div>
											<!--ed::Info-->
										</div>
										<!--end::Mixed Widget 1-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-4">
										<!--begin::Mixed Widget 1-->
										<div class="card card-xl-stretch mb-xl-8">
											<!--begin::Body-->
											<div class="card-body p-0">
												<!--begin::Header-->
												<div class="px-9 pt-7 card-rounded h-275px w-100 bg-danger">
													<!--begin::Heading-->
													<div class="d-flex flex-stack">
														<h3 class="m-0 text-white fw-bolder fs-3">Bookings Summary</h3>
													</div>
													<!--end::Heading-->
													<!--begin::Balance-->
													<div class="d-flex text-center flex-column text-white pt-8">
														<span class="fw-bold fs-7">Total Bookings</span>
														<span class="fw-bolder fs-2x pt-1"><?php echo $event_data->Purchased; ?></span>
													</div>
													<!--end::Balance-->
												</div>
												<!--end::Header-->
												<!--begin::Items-->
												<div class="bg-body shadow-sm card-rounded mx-9 mb-9 px-6 py-9 position-relative z-index-1" style="margin-top: -100px">
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-6">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/maps/map004.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<path opacity="0.3" d="M18.4 5.59998C21.9 9.09998 21.9 14.8 18.4 18.3C14.9 21.8 9.2 21.8 5.7 18.3L18.4 5.59998Z" fill="currentColor" />
																		<path d="M12 2C6.5 2 2 6.5 2 12C2 17.5 6.5 22 12 22C17.5 22 22 17.5 22 12C22 6.5 17.5 2 12 2ZM19.9 11H13V8.8999C14.9 8.6999 16.7 8.00005 18.1 6.80005C19.1 8.00005 19.7 9.4 19.9 11ZM11 19.8999C9.7 19.6999 8.39999 19.2 7.39999 18.5C8.49999 17.7 9.7 17.2001 11 17.1001V19.8999ZM5.89999 6.90002C7.39999 8.10002 9.2 8.8 11 9V11.1001H4.10001C4.30001 9.4001 4.89999 8.00002 5.89999 6.90002ZM7.39999 5.5C8.49999 4.7 9.7 4.19998 11 4.09998V7C9.7 6.8 8.39999 6.3 7.39999 5.5ZM13 17.1001C14.3 17.3001 15.6 17.8 16.6 18.5C15.5 19.3 14.3 19.7999 13 19.8999V17.1001ZM13 4.09998C14.3 4.29998 15.6 4.8 16.6 5.5C15.5 6.3 14.3 6.80002 13 6.90002V4.09998ZM4.10001 13H11V15.1001C9.1 15.3001 7.29999 16 5.89999 17.2C4.89999 16 4.30001 14.6 4.10001 13ZM18.1 17.1001C16.6 15.9001 14.8 15.2 13 15V12.8999H19.9C19.7 14.5999 19.1 16.0001 18.1 17.1001Z" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Pending</a>
																<div class="text-gray-400 fw-bold fs-7">Bookings</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1"><?php echo $bookings_summary['pending_bookings']; ?></div>
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-6">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" />
																		<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" />
																		<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" />
																		<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Allocated</a>
																<div class="text-gray-400 fw-bold fs-7">Bookings</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1"><?php echo $bookings_summary['allocated_bookings']; ?></div>
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-6">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/electronics/elc005.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<path opacity="0.3" d="M15 19H7C5.9 19 5 18.1 5 17V7C5 5.9 5.9 5 7 5H15C16.1 5 17 5.9 17 7V17C17 18.1 16.1 19 15 19Z" fill="currentColor" />
																		<path d="M8.5 2H13.4C14 2 14.5 2.4 14.6 3L14.9 5H6.89999L7.2 3C7.4 2.4 7.9 2 8.5 2ZM7.3 21C7.4 21.6 7.9 22 8.5 22H13.4C14 22 14.5 21.6 14.6 21L14.9 19H6.89999L7.3 21ZM18.3 10.2C18.5 9.39995 18.5 8.49995 18.3 7.69995C18.2 7.29995 17.8 6.90002 17.3 6.90002H17V10.9H17.3C17.8 11 18.2 10.7 18.3 10.2Z" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Unallocated</a>
																<div class="text-gray-400 fw-bold fs-7">Bookings</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1"><?php echo $bookings_summary['unallocated_bookings']; ?></div>
																
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/general/gen005.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z" fill="currentColor" />
																		<rect x="7" y="17" width="6" height="2" rx="1" fill="currentColor" />
																		<rect x="7" y="12" width="10" height="2" rx="1" fill="currentColor" />
																		<rect x="7" y="7" width="6" height="2" rx="1" fill="currentColor" />
																		<path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Duplicate</a>
																<div class="text-gray-400 fw-bold fs-7">Bookings</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1">0</div>
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
												</div>
												<!--end::Items-->
											</div>
											<!--end::Body-->
											<!--begin::Footer-->
											<div class="card-footer d-flex flex-center py-5">
												<!--begin::Item-->
												<div class="d-flex align-items-center flex-shrink-0 me-7 me-lg-12">
													<!--begin::Bullet-->
													<span class="bullet bullet-dot bg-primary me-2 h-10px w-10px"></span>
													<!--end::Bullet-->
													<!--begin::Label-->
													<span class="fw-bold text-gray-400 fs-6">Total : </span>
													<!--end::Label-->
												</div>
												<!--ed::Item-->
												<!--begin::Item-->
												<div class="d-flex align-items-center flex-shrink-0">
													<!--begin::Bullet-->
													<span class="bullet bullet-dot bg-success me-2 h-10px w-10px"></span>
													<!--end::Bullet-->
													<!--begin::Label-->
													<span class="fw-bold text-gray-400 fs-6"><?php echo $bookings_summary['total_bookings']; ?></span>
													<!--end::Label-->
												</div>
												<!--ed::Item-->
											</div>
											<!--ed::Info-->
										</div>
										<!--end::Mixed Widget 1-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-4">
										<!--begin::Mixed Widget 1-->
										<div class="card card-xl-stretch mb-5 mb-xl-8">
											<!--begin::Body-->
											<div class="card-body p-0">
												<!--begin::Header-->
												<div class="px-9 pt-7 card-rounded h-275px w-100 bg-success">
													<!--begin::Heading-->
													<div class="d-flex flex-stack">
														<h3 class="m-0 text-white fw-bolder fs-3">QR Codes Summary</h3>
													</div>
													<!--end::Heading-->
													<!--begin::Balance-->
													<div class="d-flex text-center flex-column text-white pt-8">
														<span class="fw-bold fs-7">Total QR Codes</span>
														<span class="fw-bolder fs-2x pt-1"><?php echo $qr_codes_summary['total_qr_codes']; ?></span>
													</div>
													<!--end::Balance-->
												</div>
												<!--end::Header-->
												<!--begin::Items-->
												<div class="bg-body shadow-sm card-rounded mx-9 mb-9 px-6 py-9 position-relative z-index-1" style="margin-top: -100px">
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-6">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/maps/map004.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<path opacity="0.3" d="M18.4 5.59998C21.9 9.09998 21.9 14.8 18.4 18.3C14.9 21.8 9.2 21.8 5.7 18.3L18.4 5.59998Z" fill="currentColor" />
																		<path d="M12 2C6.5 2 2 6.5 2 12C2 17.5 6.5 22 12 22C17.5 22 22 17.5 22 12C22 6.5 17.5 2 12 2ZM19.9 11H13V8.8999C14.9 8.6999 16.7 8.00005 18.1 6.80005C19.1 8.00005 19.7 9.4 19.9 11ZM11 19.8999C9.7 19.6999 8.39999 19.2 7.39999 18.5C8.49999 17.7 9.7 17.2001 11 17.1001V19.8999ZM5.89999 6.90002C7.39999 8.10002 9.2 8.8 11 9V11.1001H4.10001C4.30001 9.4001 4.89999 8.00002 5.89999 6.90002ZM7.39999 5.5C8.49999 4.7 9.7 4.19998 11 4.09998V7C9.7 6.8 8.39999 6.3 7.39999 5.5ZM13 17.1001C14.3 17.3001 15.6 17.8 16.6 18.5C15.5 19.3 14.3 19.7999 13 19.8999V17.1001ZM13 4.09998C14.3 4.29998 15.6 4.8 16.6 5.5C15.5 6.3 14.3 6.80002 13 6.90002V4.09998ZM4.10001 13H11V15.1001C9.1 15.3001 7.29999 16 5.89999 17.2C4.89999 16 4.30001 14.6 4.10001 13ZM18.1 17.1001C16.6 15.9001 14.8 15.2 13 15V12.8999H19.9C19.7 14.5999 19.1 16.0001 18.1 17.1001Z" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Pending</a>
																<div class="text-gray-400 fw-bold fs-7">QR Codes</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1"><?php echo $qr_codes_summary['pending_qr_codes']; ?></div>
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-6">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" />
																		<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" />
																		<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" />
																		<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Allocated</a>
																<div class="text-gray-400 fw-bold fs-7">QR Codes</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1"><?php echo $qr_codes_summary['allocated_qr_codes']; ?></div>
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-6">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/electronics/elc005.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<path opacity="0.3" d="M15 19H7C5.9 19 5 18.1 5 17V7C5 5.9 5.9 5 7 5H15C16.1 5 17 5.9 17 7V17C17 18.1 16.1 19 15 19Z" fill="currentColor" />
																		<path d="M8.5 2H13.4C14 2 14.5 2.4 14.6 3L14.9 5H6.89999L7.2 3C7.4 2.4 7.9 2 8.5 2ZM7.3 21C7.4 21.6 7.9 22 8.5 22H13.4C14 22 14.5 21.6 14.6 21L14.9 19H6.89999L7.3 21ZM18.3 10.2C18.5 9.39995 18.5 8.49995 18.3 7.69995C18.2 7.29995 17.8 6.90002 17.3 6.90002H17V10.9H17.3C17.8 11 18.2 10.7 18.3 10.2Z" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Unallocated</a>
																<div class="text-gray-400 fw-bold fs-7">QR Codes</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1"><?php echo $qr_codes_summary['unallocated_qr_codes']; ?></div>
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center">
														<!--begin::Symbol-->
														<div class="symbol symbol-45px w-40px me-5">
															<span class="symbol-label bg-lighten">
																<!--begin::Svg Icon | path: icons/duotune/general/gen005.svg-->
																<span class="svg-icon svg-icon-1">
																	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z" fill="currentColor" />
																		<rect x="7" y="17" width="6" height="2" rx="1" fill="currentColor" />
																		<rect x="7" y="12" width="10" height="2" rx="1" fill="currentColor" />
																		<rect x="7" y="7" width="6" height="2" rx="1" fill="currentColor" />
																		<path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="currentColor" />
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</span>
														</div>
														<!--end::Symbol-->
														<!--begin::Description-->
														<div class="d-flex align-items-center flex-wrap w-100">
															<!--begin::Title-->
															<div class="mb-1 pe-3 flex-grow-1">
																<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Duplicate</a>
																<div class="text-gray-400 fw-bold fs-7">QR Codes</div>
															</div>
															<!--end::Title-->
															<!--begin::Label-->
															<div class="d-flex align-items-center">
																<div class="fw-bolder fs-5 text-gray-800 pe-1">0</div>
															</div>
															<!--end::Label-->
														</div>
														<!--end::Description-->
													</div>
													<!--end::Item-->
												</div>
												<!--end::Items-->
											</div>
											<!--end::Body-->
											<!--begin::Footer-->
											<div class="card-footer d-flex flex-center py-5">
												<!--begin::Item-->
												<div class="d-flex align-items-center flex-shrink-0 me-7 me-lg-12">
													<!--begin::Bullet-->
													<span class="bullet bullet-dot bg-primary me-2 h-10px w-10px"></span>
													<!--end::Bullet-->
													<!--begin::Label-->
													<span class="fw-bold text-gray-400 fs-6">Total : </span>
													<!--end::Label-->
												</div>
												<!--ed::Item-->
												<!--begin::Item-->
												<div class="d-flex align-items-center flex-shrink-0">
													<!--begin::Bullet-->
													<span class="bullet bullet-dot bg-success me-2 h-10px w-10px"></span>
													<!--end::Bullet-->
													<!--begin::Label-->
													<span class="fw-bold text-gray-400 fs-6"><?php echo $qr_codes_summary['total_qr_codes']; ?></span>
													<!--end::Label-->
												</div>
												<!--ed::Item-->
											</div>
											<!--ed::Info-->
										</div>
										<!--end::Mixed Widget 1-->
									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
								
								<!--begin::Row-->
								<div class="row g-5 g-xl-8">
									<!--begin::Col-->
									<div class="col-xl-4">
										<!--begin::Mixed Widget 4-->
										<div class="card card-xl-stretch mb-xl-8">
											<!--begin::Beader-->
											<div class="card-header border-0 py-5">
												<h3 class="card-title align-items-start flex-column">
													<span class="card-label fw-bolder fs-3 mb-1">Allocation Filling Ratio</span>
													<!-- <span class="text-muted fw-bold fs-7">Complete your profile setup</span> -->
												</h3>
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body d-flex flex-column">
												<div class="flex-grow-1">
													<div class="mixed-widget-allocations-chart" data-kt-chart-color="primary" style="height: 200px"></div>
												</div>
												<div class="pt-5">
												<p class="text-center fs-6 pb-5">
													<span class="badge badge-light-danger fs-8">Notes:</span>&#160; Everything is good, 
													<br />if above chart shows 100% filling ratio</p>
													<a href="#" class="btn btn-primary w-100 py-3">View Allocations</a>
												</div>
											</div>
											<!--end::Body-->
											<!--begin::Footer-->
											<!-- <div class="card-footer d-flex flex-center py-5">
												<div class="d-flex align-items-center flex-shrink-0 me-7 me-lg-12">
													<span class="bullet bullet-dot bg-primary me-2 h-10px w-10px"></span>
													<span class="fw-bold text-gray-400 fs-6">Amount X</span>
												</div>
												<div class="d-flex align-items-center flex-shrink-0">
													<span class="bullet bullet-dot bg-success me-2 h-10px w-10px"></span>
													<span class="fw-bold text-gray-400 fs-6">Amount Y</span>
												</div>
											</div> -->
											<!--ed::Info-->
										</div>
										<!--end::Mixed Widget 4-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-4">
										<!--begin::Mixed Widget 4-->
										<div class="card card-xl-stretch mb-5 mb-xl-8">
											<!--begin::Beader-->
											<div class="card-header border-0 py-5">
												<h3 class="card-title align-items-start flex-column">
													<span class="card-label fw-bolder fs-3 mb-1">Bookings Filling Ratio</span>
													<!-- <span class="text-muted fw-bold fs-7">Complete your profile setup</span> -->
												</h3>
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body d-flex flex-column">
												<div class="flex-grow-1">
													<div class="mixed-widget-bookings-chart" data-kt-chart-color="danger" style="height: 200px"></div>
												</div>
												<div class="pt-5">
													<p class="text-center fs-6 pb-5">
													<span class="badge badge-light-danger fs-8">Notes:</span>&#160; Everything is good, 
													<br />if above chart shows 100% filling ratio</p>
													<a href="#" class="btn btn-danger w-100 py-3">View Bookings</a>
												</div>
											</div>
											<!--end::Body-->
											<!--begin::Footer-->
											<!-- <div class="card-footer d-flex flex-center py-5">
												<div class="d-flex align-items-center flex-shrink-0 me-7 me-lg-12">
													<span class="bullet bullet-dot bg-primary me-2 h-10px w-10px"></span>
													<span class="fw-bold text-gray-400 fs-6">Amount X</span>
												</div>
												<div class="d-flex align-items-center flex-shrink-0">
													<span class="bullet bullet-dot bg-success me-2 h-10px w-10px"></span>
													<span class="fw-bold text-gray-400 fs-6">Amount Y</span>
												</div>
											</div> -->
											<!--ed::Info-->
										</div>
										<!--end::Mixed Widget 4-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-4">
										<!--begin::Mixed Widget 4-->
										<div class="card card-xl-stretch mb-xl-8">
											<!--begin::Beader-->
											<div class="card-header border-0 py-5">
												<h3 class="card-title align-items-start flex-column">
													<span class="card-label fw-bolder fs-3 mb-1">QR Codes Filling Ratio</span>
													<!-- <span class="text-muted fw-bold fs-7">Complete your profile setup</span> -->
												</h3>
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body d-flex flex-column">
												<div class="flex-grow-1">
													<div class="mixed-widget-qr_codes-chart" data-kt-chart-color="success" style="height: 200px"></div>
												</div>
												<div class="pt-5">
													<p class="text-center fs-6 pb-5">
													<p class="text-center fs-6 pb-5">
													<span class="badge badge-light-danger fs-8">Notes:</span>&#160; Everything is good, 
													<br />if above chart shows 100% filling ratio</p>
													<a href="#" class="btn btn-success w-100 py-3">View QR Codes</a>
												</div>
											</div>
											<!--end::Body-->
											<!--begin::Footer-->
											<!-- <div class="card-footer d-flex flex-center py-5">
												<div class="d-flex align-items-center flex-shrink-0 me-7 me-lg-12">
													<span class="bullet bullet-dot bg-primary me-2 h-10px w-10px"></span>
													<span class="fw-bold text-gray-400 fs-6">Amount X</span>
												</div>
												<div class="d-flex align-items-center flex-shrink-0">
													<span class="bullet bullet-dot bg-success me-2 h-10px w-10px"></span>
													<span class="fw-bold text-gray-400 fs-6">Amount Y</span>
												</div>
											</div> -->
											<!--ed::Info-->
										</div>
										<!--end::Mixed Widget 4-->
									</div>
									<!--end::Col-->
									
								</div>
								<!--end::Row-->
								
								
								
								
							</div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<?php include_once "common/footer.php"; ?>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		
		<script>

		var allocations_chart = '<?php echo get_percentage($allocations_summary['total_allocations'],$allocations_summary['allocated_allocations']); ?>';
		var bookings_chart = '<?php echo get_percentage($bookings_summary['total_bookings'],$bookings_summary['allocated_bookings']); ?>';
		var qr_codes_chart ='<?php echo get_percentage($qr_codes_summary['total_qr_codes'],$qr_codes_summary['allocated_qr_codes']); ?>';

		</script>
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="<?php echo APP_URL; ?>assets/plugins/global/plugins.bundle.js"></script>
		<script src="<?php echo APP_URL; ?>assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="<?php echo APP_URL; ?>assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="<?php echo APP_URL; ?>assets/js/widgets.bundle.js"></script>
		<script src="<?php echo APP_URL; ?>assets/js/others/qc_report.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>