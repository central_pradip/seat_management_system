<?php 
$quotes =array(
	"“Silence is the element in which great things fashion themselves together.”",
	"“Failure is not the opposite of success: it’s part of success.” ",
	"“If your dreams don’t scare you, they are too small.”",
	"“Believe you can and you’re halfway there.”",
	"“Quality means doing it right when no one is looking.”",
	"“Be humble. Be hungry. And always be the hardest worker in the room.”",
	"“Never put off until tomorrow what you can do the day after tomorrow.”",
	"“If you think you are too small to make a difference, try sleeping with a mosquito.”",
	"“Those who say it cannot be done should not interrupt those doing it.”",
	"“Every accomplishment starts with the decision to try.”",
	"“You are not your resume, you are your work.”",
	"“Progress is impossible without change, and those who cannot change their minds cannot change anything.”",
	"“It always seems impossible until it’s done.”",
	"“The difference between ordinary and extraordinary is that little extra.”",
);
$random_key=rand(0,13);
$random_quote=$quotes[$random_key];


$gif_images =array(
	"https://c.tenor.com/2BtNE5PF4-wAAAAd/chill-stop.gif",
	"https://www.icegif.com/wp-content/uploads/funny.gif",
	"https://thumbs.gfycat.com/ConcretePlasticIndianrhinoceros-max-1mb.gif",
	"https://thumbs.gfycat.com/PepperyNegativeHagfish-max-1mb.gif",
	"https://i.gifer.com/LgkL.gif",
	"https://i.gifer.com/Vn7n.gif",
	"https://i.gifer.com/S6OO.gif",
	"https://c.tenor.com/NWZNZJaPh7kAAAAC/chibird-nice-day.gif",
	"https://i.pinimg.com/originals/b4/c3/54/b4c354ac422c1927c5002f8052c22d56.gif",
	"http://www.topbestpics.com/wp-content/uploads/2017/04/funny-gifs-funniest-gif-19.gif",
	"https://static01.nyt.com/images/2022/01/05/technology/personaltech/05TECHTIP-GIF/05TECHTIP-GIF-mobileMasterAt3x.gif",
	"https://qph.cf2.quoracdn.net/main-qimg-d468b30db8a467097d06a27e48c9b14d",
	"https://www.computerhope.com/tips/images/animated-youtube.gif",
	"https://c.tenor.com/1_RmSJ1pYDcAAAAC/monsters-inc-blazed.gif",
	"https://acegif.com/wp-content/uploads/gif-funny-work-48.gif",
	"https://www.readersdigest.ca/wp-content/uploads/2021/03/funny-cat-gifs.gif",
	"https://www.fantasticmedia.co.uk/wp-content/uploads/2016/05/funny-gif-wrestling-4gifs.com_.gif",
	"https://i.gifer.com/P4Vb.gif",
	"https://www.eventstodayz.com/wp-content/uploads/2017/08/Funny-Animated-Gif.gif",
	"https://149363654.v2.pressablecdn.com/wp-content/uploads/2014/12/funny-gifs-shower-prank-1.gif",
	"https://storage.googleapis.com/proudcity/elgl/uploads/2015/03/funny-gif-Panda-Mascot-basketball-ball.gif",
	"https://whws.org.au/wp-content/uploads/2021/12/a27d24_b14d2265343a4b3db8f94295b3bee5b2_mv2.gif",
	"https://www.topbestpics.com/wp-content/uploads/2017/04/funny-gifs-funniest-gif-5.gif",
	"https://i0.wp.com/www.entertainmentmesh.com/wp-content/uploads/2016/10/halloween-kitto-funny-gif.gif?ssl=1",
	"https://www.icegif.com/wp-content/uploads/funny-icegif-1.gif",
	"https://i.gifer.com/35u.gif",
	
);
$random_key_2=rand(0,25);
$random_gif_image=$gif_images[$random_key_2];
?>


<!--begin::Aside-->
<div id="kt_aside" class="aside aside-dark aside-hoverable" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
	<!--begin::Brand-->
	<div class="aside-logo flex-column-auto" id="kt_aside_logo">
		<!--begin::Logo-->
		<a href="#">
			<img alt="Logo" src="https://centraltickets.co.uk/web_root/front_root/images/Ctlogo.png" class="h-25px logo" />
		</a>
		<!--end::Logo-->
		<!--begin::Aside toggler-->
		<div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="aside-minimize">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr079.svg-->
			<span class="svg-icon svg-icon-1 rotate-180">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<path opacity="0.5" d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z" fill="currentColor" />
					<path d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z" fill="currentColor" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Aside toggler-->
	</div>
	<!--end::Brand-->
	<!--begin::Aside menu-->
	<div class="aside-menu flex-column-fluid">
		<!--begin::Aside Menu-->
		<div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer" data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
			<!--begin::Menu-->
			<div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true" data-kt-menu-expand="false">
			<div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
							<span class="svg-icon svg-icon-2">
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-check" viewBox="0 0 16 16">
							<path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3.854 2.146a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 3.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 7.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/>
							</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Matchup Events</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg">
						<div class="menu-item">
							<a class="menu-link" href="/sync_live_events">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Upcoming Matchups</span>
							</a>
						</div>
						
						<div class="menu-item">
							<a class="menu-link" href="/view_live_matchup_events">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">View Matchups (List)</span>
							</a>
						</div>

						<div class="menu-item">
							<a class="menu-link" href="/view_matchup_events_grid">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">View Matchups (Grid)</span>
							</a>
						</div>
					</div>
				</div>
				<div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-fill" viewBox="0 0 16 16">
							 <path d="M4 0h5.293A1 1 0 0 1 10 .293L13.707 4a1 1 0 0 1 .293.707V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm5.5 1.5v2a1 1 0 0 0 1 1h2l-3-3z"/>
							</svg>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Upload Data</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg">
				    	<div class="menu-item">
							<a class="menu-link" href="/add_allocations">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Upload Allocation</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="menu-link" href="/add_bookings">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Upload Bookings</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="menu-link" href="/add_qr_codes">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Upload QR Codes</span>
							</a>
						</div>
						
					</div>
				</div>
				
				<div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-qr-code-scan" viewBox="0 0 16 16">
								<path d="M0 .5A.5.5 0 0 1 .5 0h3a.5.5 0 0 1 0 1H1v2.5a.5.5 0 0 1-1 0v-3Zm12 0a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0V1h-2.5a.5.5 0 0 1-.5-.5ZM.5 12a.5.5 0 0 1 .5.5V15h2.5a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5v-3a.5.5 0 0 1 .5-.5Zm15 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1 0-1H15v-2.5a.5.5 0 0 1 .5-.5ZM4 4h1v1H4V4Z"/>
								<path d="M7 2H2v5h5V2ZM3 3h3v3H3V3Zm2 8H4v1h1v-1Z"/>
								<path d="M7 9H2v5h5V9Zm-4 1h3v3H3v-3Zm8-6h1v1h-1V4Z"/>
								<path d="M9 2h5v5H9V2Zm1 1v3h3V3h-3ZM8 8v2h1v1H8v1h2v-2h1v2h1v-1h2v-1h-3V8H8Zm2 2H9V9h1v1Zm4 2h-1v1h-2v1h3v-2Zm-4 2v-1H8v1h2Z"/>
								<path d="M12 9h2V8h-2v1Z"/>
							</svg>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Manage Venue QR</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg">
				    	
						<div class="menu-item">
							<a class="menu-link" href="/add_venue_qr_codes">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Upload Venue QR </span>
							</a>
						</div>
						<div class="menu-item">
							<a class="menu-link" href="/view_all_qr_matchups">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Manage QR Codes</span>
							</a>
						</div>
						
					</div>
				</div>


				<div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-tools" viewBox="0 0 16 16">
							<path d="M1 0 0 1l2.2 3.081a1 1 0 0 0 .815.419h.07a1 1 0 0 1 .708.293l2.675 2.675-2.617 2.654A3.003 3.003 0 0 0 0 13a3 3 0 1 0 5.878-.851l2.654-2.617.968.968-.305.914a1 1 0 0 0 .242 1.023l3.27 3.27a.997.997 0 0 0 1.414 0l1.586-1.586a.997.997 0 0 0 0-1.414l-3.27-3.27a1 1 0 0 0-1.023-.242L10.5 9.5l-.96-.96 2.68-2.643A3.005 3.005 0 0 0 16 3c0-.269-.035-.53-.102-.777l-2.14 2.141L12 4l-.364-1.757L13.777.102a3 3 0 0 0-3.675 3.68L7.462 6.46 4.793 3.793a1 1 0 0 1-.293-.707v-.071a1 1 0 0 0-.419-.814L1 0Zm9.646 10.646a.5.5 0 0 1 .708 0l2.914 2.915a.5.5 0 0 1-.707.707l-2.915-2.914a.5.5 0 0 1 0-.708ZM3 11l.471.242.529.026.287.445.445.287.026.529L5 13l-.242.471-.026.529-.445.287-.287.445-.529.026L3 15l-.471-.242L2 14.732l-.287-.445L1.268 14l-.026-.529L1 13l.242-.471.026-.529.445-.287.287-.445.529-.026L3 11Z"/>
							</svg>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Tools</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg">
						<div class="menu-item">
							<a class="menu-link" href="#">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Allocation Formatting</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="menu-link" href="#">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Seat Formatting</span>
							</a>
						</div>
					</div>
				</div>
			
				
			
			</div>

			
			<!--end::Menu-->

			
		</div>
		<!--end::Aside Menu-->
	</div>
	<!--end::Aside menu-->
	<!--begin::Footer-->
	<div class="aside-footer flex-column-auto pt-5 pb-7 px-5" id="kt_aside_footer">
	
	<!-- <img src="<?php echo $random_gif_image; ?>" width="235" style="border-radius: 10px; padding: 3px;height: 150px;"/> -->
	
		<a href="#" class="btn btn-custom btn-primary w-100" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss-="click" title="<?php echo $random_quote; ?>">
			<span class="btn-label">Keep calm, you got this :)</span>
			<!--begin::Svg Icon | path: icons/duotune/general/gen005.svg-->
			<span class="svg-icon btn-icon svg-icon-2">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z" fill="currentColor" />
					<rect x="7" y="17" width="6" height="2" rx="1" fill="currentColor" />
					<rect x="7" y="12" width="10" height="2" rx="1" fill="currentColor" />
					<rect x="7" y="7" width="6" height="2" rx="1" fill="currentColor" />
					<path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="currentColor" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</a>
	</div>
	<!--end::Footer-->
</div>
<!--end::Aside-->