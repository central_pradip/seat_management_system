<!--begin::Head-->
<head>
	<?php 
	if(!empty($title)){ 
		echo '<title>'.$title.' - Seat Management System</title>';
	}else{
		echo '<title>Seat Management System</title>';
	}
	?>
	<meta charset="utf-8" />
	<meta name="description" content="Central Tickets - Seat Management System" />
	<meta name="keywords" content="Central Tickets, Seat, Management, System" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Central Tickets - Seat Management System" />
	<meta property="og:url" content="https://centraltickets.co.uk/" />
	<meta property="og:site_name" content="Central Tickets | System Management System" />
	<link rel="canonical" href="https://centraltickets.co.uk/" />
	<link rel="shortcut icon" href="<?php echo APP_URL; ?>assets/media/logos/favicon.ico" />

	<!--begin::Fonts-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	<!--end::Fonts-->

	<?php if($session=="Dark"){ ?>

		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="<?php echo APP_URL; ?>assets/plugins/global/plugins.dark.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo APP_URL; ?>assets/css/style.dark.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->

	<?php }else{ ?>

		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="<?php echo APP_URL; ?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo APP_URL; ?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->

	<?php } ?>
	
</head>
<!--end::Head-->