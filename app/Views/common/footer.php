<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
<!--begin::Container-->
<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
	<!--begin::Copyright-->
	<div class="text-dark order-2 order-md-1">
		<span class="text-muted fw-bold me-1">2022©</span>
		<a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary">Seat Management System (Central Tickets)</a>
	</div>
	<!--end::Copyright-->
	
</div>
<!--end::Container-->
</div>

<script>

    function show_message(response)
	{
							
		if(response.status==="1"){

			toastr.success(
				response.message, 
				"Success Alert!", 
				{
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": true,
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "10000",
					"hideDuration": "10000",
					"timeOut": "10000",
					"extendedTimeOut": "10000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				}
			);
			
			if(response.redirect_url) {

				if(response.redirect_url==="reload") {
					setTimeout(function(){
						location.reload();
					}, 2000);
			    
				}else{

					setTimeout(function(){
						window.location.assign(response.redirect_url);
					}, 3000);

				}

            }
			

		}else if(response.status==="3"){

		toastr.info(
			response.message, 
			"Info Alert!", 
			{
				"closeButton": true,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "2000",
				"hideDuration": "2000",
				"timeOut": "2000",
				"extendedTimeOut": "2000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		);

		}else{
			toastr.error(
				response.message, 
				"Error Alert!", 
				{
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": true,
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "10000",
					"hideDuration": "10000",
					"timeOut": "10000",
					"extendedTimeOut": "10000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				}
			);
		}
		

	}


	function change_mode(Mode)
	{

		// const toster_data = ({"status": '3',"message": 'Changing view to the '+Mode+' mode'});
		// show_message(toster_data);

		var FData = new FormData();
		FData.append("Mode", Mode);

		$.ajax({
			type: "POST",
			url: "<?php echo APP_URL; ?>change_mode",
			data: FData,
			processData: false,
			contentType: false,
			dataType: "json",
			cache: false,
			timeout: 800000,
			success: function (response) {
				show_message(response)
			},
			error: function (e) {

			}
		});

	}
</script>