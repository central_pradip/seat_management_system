<?php
namespace App\Controllers;


class Live_Events extends BaseController {
    
    public function sync_live_events() 
    {
       
        $api_data =call_api('Seat_Managment_API/get_matchup_events','none');
        if($api_data['status']=="200"){

            $blade_data['title']='Sync Live Events';
            $blade_data['session']= $this->session->get('Mode');

            $blade_data['events']=$api_data['events'];
            $blade_data['listing_images']='https://centralticket.nyc3.cdn.digitaloceanspaces.com/listing/email_events/';
            $blade_data['detail_images']='https://centralticket.nyc3.cdn.digitaloceanspaces.com/event_images/';

            return view('sync_live_events', $blade_data);

        }
        
    }


    public function create_matchup_live() 
    {
       
        $schedule_id = $this->uri->getSegment(2);
        if(!empty($schedule_id)){
            
            $api_data['schedule_id']=$schedule_id;
            $api_response =call_api('Seat_Managment_API/get_schedule_info',$api_data);
            if($api_response['status']=="200"){
    
                $blade_data['listing_images']='https://centralticket.nyc3.cdn.digitaloceanspaces.com/listing/email_events/';
                $blade_data['schedule_info']=$api_response['schedule_info'];

                $blade_data['title']='Create Matchup';
                $blade_data['session']= $this->session->get('Mode');

                return view('create_matchup_live',$blade_data);
    
            }

        }
    }

    public function store_matchup_event() {
       
        
        if($this->request->isAJAX()) 
        {

            $post_data=$_POST;
            $seat_colomns=$post_data['seat_colomns'];

            $input = $this->validate(['file' => 'uploaded[file]|max_size[file,1024]|ext_in[file,csv],']);
            if(!$input){ 
            
                echo json_encode(array('status'=>'0','message' => 'File not valid.'));
            
            }else{ 
            
                if($file = $this->request->getFile('file')){

                    if($file->isValid() && !$file->hasMoved()){

                        $Project_id=rand(1111111,9999999);
                        $allocation_file = 'Allocation_File_'.$post_data['schedule_id'].'_'.$Project_id.'.csv';
                        $file->move('../public/matchup/allocation/', $allocation_file);
                        $file = fopen("../public/matchup/allocation/" . $allocation_file, "r");
                        $i = 0;
                    
                        //Creating a live event
                        $live_events = $this->db->table('live_events');

                        $live_event_data['Project_id']=$Project_id;
                        $live_event_data['Event_name']=$post_data['event_name'];
                        $live_event_data['Event_id']=$post_data['event_id'];
                        $live_event_data['Schedule_id']=$post_data['schedule_id'];
                        $live_event_data['Allocation']=$post_data['allocation'];
                        $live_event_data['Purchased']=$post_data['purchased'];
                        $live_event_data['Allocation_file']=$allocation_file;
                        $live_event_data['Seat_colomns']=implode(",",$post_data['seat_colomns']);
                        $live_event_data['Sepration']=$post_data['sepration'];
                        $live_event_data['Image']=$post_data['event_image'];
                        $live_event_data['Created_at']=date("Y-m-d H:i:s");

                        if(!empty($post_data['scanned_tickets'])){
                            $live_event_data['Scanned_tickets']='No';
                        }else{
                            $live_event_data['Scanned_tickets']='Yes';
                        }

                        if(!empty($post_data['seating_type'])){
                            $live_event_data['seating_type']='toghther';
                        }else{
                            $live_event_data['seating_type']='alone';
                        }
                        
                        //Inserting event data
                        $live_events->insert($live_event_data);

                        sleep(3);
                        $allocation_table = $this->db->table('allocation');
                        $tickets=0;
                        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {

                            //Allocation Table
                            $allocation_data['Project_id'] = $Project_id;
                            $allocation_data[$seat_colomns[0]] = $filedata[0];
                            $allocation_data[$seat_colomns[1]] = $filedata[1];
                            $allocation_data[$seat_colomns[2]] = $filedata[2];

                            if(!empty($seat_colomns[3])){
                                $allocation_data[$seat_colomns[3]] = $filedata[3];
                                $allocation_data['Remaining'] = $filedata[3];
                                $tickets=$tickets+$filedata[3];
                            }

                            if(!empty($seat_colomns[4])){
                                $allocation_data[$seat_colomns[4]] = $filedata[4];
                            }else{
                                $allocation_data[$seat_colomns[4]] = 'NA';
                            }

                            if(!empty($seat_colomns[5])){
                                $allocation_data['Extra'] = $filedata[5];
                            }else{
                                $allocation_data['Extra'] = 'NA';
                            } 

                            //inseting allocation data
                            $allocation_table->insert($allocation_data);
                            
                            
                        }

                        
                        fclose($file);

                        $redirect_url=APP_URL.'live_matchup_dashboard/'.$Project_id;
                        echo json_encode(array('status'=>'1','message' => 'Matchup Created Succesfully','redirect_url' => $redirect_url));
                    
                        
                    }else{
                        echo json_encode(array('status'=>'0','message' => 'File Not Imported Currectly!'));
                    }

                }else{
                    echo json_encode(array('status'=>'0','message' => 'File Not Imported!'));
                    
                }
            }

        }

    }


    //Geting Matchup Data
    
    public function get_left_bookings($Project_id)
    {


            $query = $this->db->query("SELECT b.Quantity,SUM(b.Quantity) as total_bookings FROM bookings b where b.Project_id='".$Project_id."' GROUP BY b.Quantity");
            $bookings_records = $query->getResult();
            
            $statistics = [];
            $i=0;
            foreach($bookings_records as $booking){

                    $query2 = $this->db->query("SELECT b.Quantity,SUM(b.Quantity) as left_bookings FROM bookings b where b.Project_id='".$Project_id."' and b.Status='pending' and b.Quantity='".$booking->Quantity."' GROUP BY b.Quantity ");
                    $left_bookings= $query2->getRow();
                    
                    if(!empty($left_bookings)){

                        $statistics[$i]['Quantity']=$booking->Quantity;
                        $statistics[$i]['total_bookings']=$booking->total_bookings;
                        $statistics[$i]['left_bookings']=$left_bookings->left_bookings;
                        
                        
                    }else{

                        $statistics[$i]['Quantity']=$booking->Quantity;
                        $statistics[$i]['total_bookings']=$booking->total_bookings;
                        $statistics[$i]['left_bookings']=0;

                    }
                    $i++;

            }
            krsort($statistics);
            if(empty($statistics)){
                $statistics[0]['Quantity']='None';
            }

            return $statistics;
    }

    
    public function get_left_allocation($Project_id)
    {


            $query = $this->db->query("SELECT a.Quantity,SUM(a.Quantity) as total_seats FROM allocation a where a.Project_id='".$Project_id."' GROUP BY a.Quantity");
            $allocation_records = $query->getResult();
            
            $statistics = [];
            $i=0;
            foreach($allocation_records as $booking){

                
            
                    $query2 = $this->db->query("SELECT a.Quantity,SUM(a.Remaining) as left_seats FROM allocation a where a.Project_id='".$Project_id."' and (a.Status='pending' OR a.Status='progress') and a.Quantity='".$booking->Quantity."' GROUP BY a.Quantity ");
                    $left_allocation= $query2->getRow();
                    

                    // $query3 = $this->db->query("b.Quantity,SUM(b.Quantity) as left_bookings FROM bookings b where b.Project_id='".$Project_id."' and b.Status='pending' and b.Quantity='".$booking->Quantity."'");
                    // $left_bookings = $query3->getRow();
                   
                    

                    // if(!empty($left_bookings)){
                    //     $statistics[$i]['left_bookings']=$left_bookings->left_bookings;
                    // }else{
                    //     $statistics[$i]['left_bookings']=0;
                    // }

                    if(!empty($left_allocation)){

                        $statistics[$i]['Quantity']=$booking->Quantity;
                        $statistics[$i]['total_seats']=$booking->total_seats;
                        $statistics[$i]['left_seats']=$left_allocation->left_seats;
                        
                        
                        
                    }else{

                        $statistics[$i]['Quantity']=$booking->Quantity;
                        $statistics[$i]['total_seats']=$booking->total_seats;
                        $statistics[$i]['left_seats']=0;

                    }
                    $i++;

            }
            krsort($statistics);
            if(empty($statistics)){
                $statistics[0]['Quantity']='None';
            }

            return $statistics;


    }


    public function get_exact_matching($Project_id)
    {

        $query = $this->db->query("SELECT b.Quantity,SUM(b.Quantity) as total_bookings FROM bookings b where b.Project_id='".$Project_id."' and b.Status='pending' GROUP BY b.Quantity");
        $bookings_records = $query->getResult();

        $statistics = [];
        $total_seats=0;
        $i=0;
        if(!empty($bookings_records)){

            foreach($bookings_records as $booking){

                    //Find Exact Matching Buttons
                    $query2 = $this->db->query("SELECT a.Quantity,SUM(a.Quantity) as total_seats FROM allocation a where a.Quantity='".$booking->Quantity."' and a.Project_id='".$Project_id."' and a.Status='pending'");
                    $find_allocation = $query2->getRow();
                    
                    
                    if(!empty($find_allocation->Quantity)){
                        
                        $statistics[$i]['Quantity']=$find_allocation->Quantity;
                        $statistics[$i]['total_seats']=$find_allocation->total_seats;
                        $statistics[$i]['total_bookings']=$booking->total_bookings;

                        if($find_allocation->total_seats>=$booking->total_bookings){
                           $total_seats=$total_seats + $booking->total_bookings;
                        }else{
                           $total_seats=$total_seats + $find_allocation->total_seats;
                        }
                    
                        
                    }
                    $i++;

            }


        }
        
        // krsort($statistics);
        // $statistics['exact_matching']= $statistics;
        // $statistics['total_seats']= $total_seats;

        return $statistics;
    }


    public function live_matchup_dashboard(){

        $Project_id  = $this->uri->getSegment(2);
        if(!empty($Project_id)){
            
            $project_table= $this->db->table('live_events');
            $query = $project_table->Where(['Project_id' => $Project_id]);
            $query = $project_table->get();
            $event_data = $query->getRow();

            if(!empty($event_data->Schedule_id)){

                $api_data['schedule_id']=$event_data->Schedule_id;
                $api_response =call_api('Seat_Managment_API/get_schedule_info',$api_data);
                if($api_response['status']=="200"){
        
                    $allocation_query = $this->db->query("SELECT *  FROM bookings b where b.Project_id='".$Project_id."'");
                    $last_synced_bookings = $allocation_query->getResult();

                    if(count($last_synced_bookings)>=1){
                        $blade_data['bookings']= $last_synced_bookings;

                        $blade_data['allocated_bookings']=$this->db->query("SELECT SUM(b.Quantity) as allocated_bookings FROM bookings b where b.Project_id ='".$Project_id ."' and b.Status='success'")->getRow();
                        $blade_data['unallocated_bookings']=$this->db->query("SELECT SUM(b.Quantity) as unallocated_bookings FROM bookings b where b.Project_id ='".$Project_id ."' and (b.Status='pending' OR b.Status='progress')")->getRow();
                    }
                    

                    $blade_data['allocated_tickets']=$this->db->query("SELECT SUM(b.Quantity) as allocated_tickets FROM allocation b where b.Project_id ='".$Project_id ."' and b.Status='allocated'")->getRow();
                    $blade_data['unallocated_tickets']=$this->db->query("SELECT SUM(b.Remaining) as unallocated_tickets FROM allocation b where b.Project_id ='".$Project_id ."' and (b.Status='pending' OR b.Status='progress')")->getRow();

                    $blade_data['left_allocations']=$this->get_left_allocation($Project_id);
                    $blade_data['get_exact_matching']=$this->get_exact_matching($Project_id);

                    $blade_data['listing_images']='https://centralticket.nyc3.cdn.digitaloceanspaces.com/listing/email_events/';
                    $blade_data['schedule_info']=$api_response['schedule_info'];
                    $blade_data['event_data']=$event_data;

                    $blade_data['title']='Matchup Dashboard';
                    $blade_data['session']= $this->session->get('Mode');

                    return view('live_matchup_dashboard',$blade_data);
        
                }
            }
            

        }

    }


    public function pull_live_bookings(){

        if ($this->request->isAJAX()) {
            
            $post_data=$_POST;
            $schedule_id=$post_data['schedule_id'];
            $Project_id=$post_data['project_id'];

            if(!empty($schedule_id) && !empty($Project_id)){

                $project_table= $this->db->table('live_events');
                $query = $project_table->Where(['Project_id' => $Project_id]);
                $query = $project_table->get();
                $event_data = $query->getRow();


                
                if(!empty($event_data)){

                    $api_data['schedule_id']=$schedule_id;
                    $api_response =call_api('Seat_Managment_API/get_live_bookings',$api_data);

                    if($api_response['status']=="200"){
            
                        $bookings_table = $this->db->table('bookings');
                        
                        //Deleting Old Bookings For This Project
                        $bookings_table->where('Project_id', $Project_id);
                        $bookings_table->delete();
                

                        $bookings=0;
                        foreach($api_response['bookings'] as $booking){

                            $booking_data['Project_id']=$Project_id;
                            $booking_data['Lname']=$booking['lname'];
                            $booking_data['Fname']=$booking['fname'];
                            $booking_data['Email']=$booking['email'];
                            $booking_data['User_id']=$booking['user_id'];
                            $booking_data['Mobile']=$booking['mobilenumber'];
                            $booking_data['Booking_Type']=$booking['orderby'];
                            $booking_data['Booking_id']=$booking['id'];
                            $booking_data['Reference']=$booking['ref_id'];
                            $booking_data['Quantity']=$booking['TotalTickets'];
                            

                            //Inseting Bookings data
                            $bookings_table->insert($booking_data);
                            $bookings=($bookings+$booking['TotalTickets']);
                        

                        }

                        if($bookings){

                            $update_project['Purchased']=$bookings;
                            $project_table->set($update_project);
                            $project_table->where('Project_id', $Project_id);
                            $project_table->update();

                            $redirect_url=APP_URL.'live_matchup_dashboard/'.$Project_id;
                            echo json_encode(array('status'=>'1','message' => $bookings.' bookings succesfully imported from live website.','redirect_url' => $redirect_url));
                        }
                    
            
                    }else{
                        echo json_encode(array('status'=>'0','message' => $api_response['message']));
                    }

                }else{
                    echo json_encode(array('status'=>'0','message' => 'Matchup request does not exist, please double check everything!'));
                }
            
            }else{
                echo json_encode(array('status'=>'0','message' => 'Invalid Request!'));
            }


        }
    }

    
    public function view_live_matchup_events(){
        
        $matchup_events_query = $this->db->query("SELECT *  FROM live_events");
        $matchup_events = $matchup_events_query->getResult();

        $blade_data['matchup_events']=$matchup_events;

        $blade_data['title']='View All Matchups';
        $blade_data['session']= $this->session->get('Mode');

        return view('view_all_live_matchups', $blade_data);

    }

    public function view_matchup_events_grid(){
        
        $matchup_events_query = $this->db->query("SELECT *  FROM live_events order by created_at desc");
        $matchup_events = $matchup_events_query->getResult();

        $blade_data['detail_images']='https://centralticket.nyc3.cdn.digitaloceanspaces.com/event_images/';
        $blade_data['matchup_events']=$matchup_events;

        $blade_data['title']='View All Matchups';
        $blade_data['session']= $this->session->get('Mode');

        return view('view_all_matchups_grid_view', $blade_data);

    }

    public function mark_as_done(){

        
        if ($this->request->isAJAX())
        {

            $post_data=$_POST;
            $matchup_completed = $this->db->table('matchup_completed');
                        
            $insert_data['Event_Name']=$post_data['event_name'];
            $insert_data['Schedule_id']=$post_data['schedule_id'];
            $insert_data['Allocation']=$post_data['allocation'];
            $insert_data['Bookings']=$post_data['bookings'];
            $insert_data['Created_at']=date("Y-m-d H:i:s");

            //Inseting Bookings Data
            $matchup_completed->insert($insert_data);

            echo json_encode(array('status'=>'1','message' => 'Event Succesfully Marked As Done'));

        }else{
            echo json_encode(array('status'=>'0','message' => 'Invalid Request!'));
        }

    }



}


?>