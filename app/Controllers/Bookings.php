<?php
namespace App\Controllers;


class Bookings extends BaseController {


    function add_bookings(){

        if ($this->request->isAJAX()) {

            $post_data=$_POST;
            $Project_id=$post_data['Project_id'];
            $bookings_table = $this->db->table('bookings');

            //Deleting Old Bookings
            $bookings_table->where('Project_id', $Project_id);
            $bookings_table->delete();

            if($file = $this->request->getFile('file')){

                if($file->isValid() && !$file->hasMoved()){

                    $newName = "bookings_".$file->getRandomName();

                    $file->move('../public/matchup/bookings', $newName);
                    $file = fopen("../public/matchup/bookings/" . $newName, "r");
                    $i = 0;

                    
                    $bookings_count=0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        
                        $booking_data['Project_id ']=$Project_id;
                        $booking_data['Lname']=$filedata[0];
                        $booking_data['Fname']=$filedata[1];
                        $booking_data['Mobile']=$filedata[2];
                        $booking_data['Reference']=$filedata[4];
                        $booking_data['Quantity']=$filedata[5];
                        $booking_data['Status']='pending';
                        if($filedata[3]=="CTB"){
                            $booking_data['Booking_Type']='1';
                        }else{
                            $booking_data['Booking_Type']='3';
                        }
                        
                        //Inserting Qr Code Data data
                        $bookings_count=$bookings_count+$booking_data['Quantity'];
                        $bookings_table->insert($booking_data);

                    }

                    echo json_encode(array('status'=>'1','message' => $bookings_count.' Bookings Importedd Successfully')); 
                    die;

                }else{

                    echo json_encode(array('status'=>'0','message' => 'Faild to upload file, please check the file!')); 
                    die;
                    
                }


            }else{

                echo json_encode(array('status'=>'0','message' => 'File not uploaded')); 
                die;

            }
            
        }else{

            $blade_data['events_data'] = $this->db->query("SELECT * FROM live_events le")->getResult();
            
            $blade_data['title']='Upload bookings';
            $blade_data['session']= $this->session->get('Mode');
            return view('add_bookings', $blade_data);

        }
        
    }

    function view_all_bookings(){

        $Project_id  = $this->uri->getSegment(2);

        if(!empty($Project_id)){

            $booking_table= $this->db->table('bookings');
            $query = $booking_table->Where(['Project_id' => $Project_id]);
            $query = $booking_table->get();
            $blade_data['bookings'] = $query->getResult();
 

            $allocation_table= $this->db->table('allocation');
            $query = $allocation_table->Where(['Project_id' => $Project_id,'Status' => 'pending']);
            $query = $allocation_table->orderBy('Quantity','ASC');
            $query = $allocation_table->get();
            $blade_data['allocations'] = $query->getResult();


            $blade_data['event_data'] = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();
            $blade_data['title']='View All Bookings';
            $blade_data['session']= $this->session->get('Mode');

            return view('view_all_bookings', $blade_data);
        }

        

    }

    function format_allocation_seats($Seats){

        $seat_array = explode('--',$Seats);
        if(count($seat_array)>1)
        {
            if($seat_array[0]>$seat_array[1]){ 
                $start=$seat_array[1]; 
                $end=$seat_array[0]; 
            }else{  
                $start=$seat_array[0];
                $end=$seat_array[1]; 
            }

        }else
        {
            
            $start=$seat_array[0];
            $end=$seat_array[1]; 
        }


        $seats_array2=[];
        for ($x = $start; $x <= $end; $x++) {
            $seats_array2[]=$x;
        }
      

        return implode(",",$seats_array2);
    }

    function get_bookings_for_allocation()
    {

        
        $post_data=$_POST;
        $Project_id=$post_data['Project_id'];
        $Section=$post_data['Section'];
        $Row=$post_data['Row'];
        $Seats=$post_data['Seats'];

        if(!empty($Project_id))
        {

            $booking_table = $this->db->table('bookings');
            $query = $booking_table->where(['Project_id' => $Project_id, 'Section' => $Section, 'Row' => $Row]);
            $query = $booking_table->get();
            $bookings = $query->getResult();


            $seat_array_data=$this->format_allocation_seats($Seats);
            
            $ticket_count=0;
            $res='';
            foreach($bookings as $booking)
            {

                    if(strpos($seat_array_data, $booking->Seat) !== false) 
                    {
                           $seat_array= explode(",",$booking->Seat);
                            //Seting Area
                            $seating_area='';
                            if($booking->Entrance!="NA"){
                                $seating_area.=$booking->Entrance.', ';
                            }
                            if($booking->Section!="NA"){
                                $seating_area.=$booking->Section.', ';
                            }
                            if($booking->Row!="NA"){
                                $seating_area.='Row '.$booking->Row;
                            }

                            //Seat Number
                            if(count($seat_array)>1){

                                $seats=[];
                                for ($x = 0; $x < count($seat_array); $x++) {
                                    $seats[]=$seat_array[$x];

                                }
                                $seat_number=implode(", ",$seats);

                            }else{
                                $seat_number=$booking->Seat;
                            }

                            $res.='
                            <div class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-35px symbol-circle">
                                        <span class="symbol-label bg-light-danger text-danger fw-bold">'.substr($booking->Lname, 0, 1).'</span>
                                    </div>
                                    <div class="ms-6">
                                        <a href="#" class="d-flex align-items-center fs-5 fw-bolder text-dark text-hover-primary">'.$booking->Lname.' '.$booking->Fname.' 
                                        <div class="fw-bold text-muted" style="padding-left: 10px;font-size: 12px;">'.$booking->Email.'</div>
                                        </a>
                                        <span class="badge badge-dark fs-8 fw-bold ms-2">'.$seating_area.'</span>
                                        <span class="badge badge-dark fs-8 fw-bold ms-2" >'.$seat_number.'</span>
                                    
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="text-end">
                                        <div class="fs-5 fw-bolder text-dark">'.$booking->Quantity.'</div>
                                        <div class="fs-7  text-dark">'.$booking->Reference.'</div>
                                    </div>
                                </div>
                                
                            </div>';

                            $ticket_count=$ticket_count+$booking->Quantity;
                    }

                            

            }

            if($ticket_count>=1){
                $res.='
                <div class="text-muted fw-bold fs-5" style="    padding-top: 15px !important;text-align: center;">
                    <span class="badge badge-success fs-8 fw-bold ms-2" >Total '.$ticket_count.' Tickets Found!</span>
                </div>';

            }else{
                $res.='
                <div class="text-muted fw-bold fs-5" style="    padding-top: 15px !important;text-align: center;">
                    <span class="badge badge-danger fs-8 fw-bold ms-2" >Sorry! No bookings found :(</span>
                </div>';
            }
           

            echo $res;
          
        }

        

    }


    function delete_booking_row()
    {

       if ($this->request->isAJAX()) {

            $post_data=$_POST;
            $Booking_id=$post_data['Booking_id'];

            $Booking_table = $this->db->table('bookings');
            $Booking_table->where('id', $Booking_id);
            $Booking_table->delete();

            echo json_encode(array('status'=>'1','message' => 'Bookings Deleted Successfully')); 
            die;

        }else{

            echo json_encode(array('status'=>'0','message' => 'Invalid Request!')); 
            die;

        }

    }


    function request_access_need_modal()
    {
        if ($this->request->isAJAX()) {

            $post_data=$_POST;
            $Booking_id=$post_data['Booking_id'];

            $booking_table= $this->db->table('bookings');
            $query = $booking_table->Where('id',$Booking_id);
            $query = $booking_table->get();

            $this_booking = $query->getRow();

            dd($this_booking);


            echo json_encode(array('status'=>'1','message' => 'Bookings Deleted Successfully')); 
            die;

        }else{

            echo json_encode(array('status'=>'0','message' => 'Invalid Request!')); 
            die;

        }

    }


    


}


?>