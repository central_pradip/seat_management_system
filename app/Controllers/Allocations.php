<?php
namespace App\Controllers;


class Allocations extends BaseController {

    function add_allocations(){

        if ($this->request->isAJAX()) {

            $post_data=$_POST;
            $Project_id=$post_data['Project_id'];
            $Allocations_number=$post_data['allocations_number'];

            //Deleting Old Allocations
            $allocation_table = $this->db->table('allocation');
            $allocation_table->where('Project_id', $Project_id);
            $allocation_table->delete();

            if($file = $this->request->getFile('file')){

                if($file->isValid() && !$file->hasMoved()){

                    $newName = "allocations_".$file->getRandomName();

                    $file->move('../public/matchup/allocations', $newName);
                    $file = fopen("../public/matchup/allocations/" . $newName, "r");
                    $i = 0;

                    
                    $allocations_count=0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        
                        $allocation_data['Section']=$filedata[0];
                        $allocation_data['Row']=$filedata[1];
                        $allocation_data['Seat']=$filedata[2];
                        $allocation_data['Quantity']=$filedata[3];
                        $allocation_data['Remaining']=$filedata[3];
                        $allocation_data['Entrance']=$filedata[4];
                        $allocation_data['Project_id']=$Project_id;
                        $allocation_data['Status']='pending';
                        $allocation_data['Last_allocated']=0;
                        if(!empty($filedata[5])){
                         $allocation_data['Extra']=$filedata[5];
                        }
                        
                        //Inserting Qr Code Data data
                        $allocations_count=$allocations_count+ $allocation_data['Quantity'];
                        $allocation_table->insert($allocation_data);

                    }

                    echo json_encode(array('status'=>'1','message' => $allocations_count.' Allocations Seats Imported Successfully')); 
                    die;

                }else{

                    echo json_encode(array('status'=>'0','message' => 'Faild to upload file, please check the file!')); 
                    die;
                    
                }


            }else{

                echo json_encode(array('status'=>'0','message' => 'File not uploaded')); 
                die;

            }
            
        }else{

            $blade_data['events_data'] = $this->db->query("SELECT * FROM live_events le")->getResult();
            $blade_data['title']='Upload allocations';
            $blade_data['session']= $this->session->get('Mode');

            return view('add_allocations', $blade_data);

        }
        
    }


    function delete_allocation_row()
    {

       if ($this->request->isAJAX()) {

            $post_data=$_POST;
            $Allocation_id=$post_data['Allocation_id'];

            $Allocation_table = $this->db->table('allocation');
            $Allocation_table->where('id', $Allocation_id);
            $Allocation_table->delete();

            echo json_encode(array('status'=>'1','message' => 'Allocation Deleted Successfully!')); 
            die;

        }else{

            echo json_encode(array('status'=>'0','message' => 'Invalid Request!')); 
            die;

        }

    }


    function edit_allocation_row(){

        if ($this->request->isAJAX()){

            $post_data=$_POST;
            $Allocation_id=$post_data['Allocation_id'];

            $Allocation_Query = $this->db->query("SELECT * FROM allocation where id='".$Allocation_id."'");
            $Allocation_Row = $Allocation_Query->getRow();

            if(!empty($Allocation_Row)){

                $Select_Options='<option value="">Select Status</option>';
                if($Allocation_Row->Status=="pending"){

                    $Select_Options.='
                    <option value="pending" selected>Pending</option>
                    <option value="allocated">Allocated</option>
                    <option value="progress">Progress</option>';

                }elseif($Allocation_Row->Status=="allocated"){

                    $Select_Options.='
                    <option value="pending" >Pending</option>
                    <option value="allocated" selected>Allocated</option>
                    <option value="progress">Progress</option>';

                }elseif($Allocation_Row->Status=="progress"){

                    $Select_Options.='
                    <option value="pending" >Pending</option>
                    <option value="allocated">Allocated</option>
                    <option value="progress" selected>Progress</option>';
                }
                
                echo '
                    <div class="row g-9 mb-8">
                        <div class="col-md-6 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Entrance</label>
                            <input type="text" class="form-control form-control-solid" placeholder="Enter Entrance" name="Entrance" value="'.$Allocation_Row->Entrance.'" />
                        </div>
                        
                        <div class="col-md-6 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Section</label>
                            <input type="text" class="form-control form-control-solid" placeholder="Enter Section" name="Section" value="'.$Allocation_Row->Section.'" />
                        </div>
                    </div>
                    
                    <div class="row g-9 mb-8">
                        <div class="col-md-6 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Row</label>
                            <input type="text" class="form-control form-control-solid" placeholder="Enter Section" name="Row" value="'.$Allocation_Row->Row.'" />
                        </div>
                        <div class="col-md-6 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Seat</label>
                            <input type="text" class="form-control form-control-solid" placeholder="Enter Seat" name="Seat" value="'.$Allocation_Row->Seat.'" />
                        </div>
                    </div>
                   
                    <div class="row g-9 mb-8">
                        <div class="col-md-6 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Quantity</label>
                            <input type="text" class="form-control form-control-solid" placeholder="Enter Quantity" name="Quantity" value="'.$Allocation_Row->Quantity.'" />
                        </div>

                        <div class="col-md-6 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Remaining</label>
                            <input type="text" class="form-control form-control-solid" placeholder="Enter Remaining" name="Remaining" value="'.$Allocation_Row->Remaining.'" />
                        </div>
                    </div>
                    
                    <div class="row g-9 mb-8">
                        <div class="col-md-6 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Assign</label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Select Status" name="Status" >
                                '.$Select_Options.'
                            </select>
                        </div>
                        
                        <div class="col-md-6 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Last Allocated</label>
                            <div class="position-relative d-flex align-items-center">
                            <input type="text" class="form-control form-control-solid" placeholder="Enter Last Allocated" name="Last_allocated " value="'.$Allocation_Row->Last_allocated .'" />
                            </div>
                        </div>
                    </div>

                    <div class="d-flex flex-column mb-8">
                        <label class="fs-6 fw-bold mb-2">Extra</label>
                        <input type="text" class="form-control form-control-solid" placeholder="Enter Extra" name="Extra" value="'.$Allocation_Row->Extra.'" />
                    </div>';

            }

        }else{

            echo json_encode(array('status'=>'0','message' => 'Invalid Request!')); 
            die;

        }
    }

    function access_need_allocations_seat_data(){

        if ($this->request->isAJAX()) 
        {

            $post_data=$_POST;
            $Allocation_id=$post_data['Allocation_id'];

            $allocation_table = $this->db->table('allocation');
            $query = $allocation_table->Where('id',$Allocation_id);
            $query = $allocation_table->get();
            $this_allocation = $query->getRow();

            $Project_id=$this_allocation->Project_id;

            if(!empty($Project_id)){

                //Geting Event Data
                $query = $this->db->query("SELECT *  FROM live_events le where le.Project_id ='".$Project_id ."'");
                $event_data = $query->getRow();

                $seats_html='';
                $seat_array = explode($event_data->Sepration,$this_allocation->Seat);
                if(count($seat_array)>1)
                {
                    if($seat_array[0]>$seat_array[1]){ 
                        $start=$seat_array[1];
                        $end=$seat_array[0];
                    }else{  
                        $start=$seat_array[0];
                        $end=$seat_array[1];
                    }

                    for ($x = $start; $x <= $end; $x++) {
                    $seats_html.='<div class="d-flex align-items-center mb-8">
                                    <span class="bullet bullet-vertical h-40px bg-success"></span>
                                    <div class="form-check form-check-custom form-check-solid mx-5">
                                        <input class="form-check-input" type="checkbox" value="'.$x.'" name="bookers_seats[]">
                                    </div>
                                    <div class="flex-grow-1">
                                        <a href="#" class="text-gray-800 text-hover-primary fw-bolder fs-6">'.$this_allocation->Section.', Row '.$this_allocation->Row.'</a>
                                        <span class="text-muted fw-bold d-block">Seat Number '.$x.'</span>
                                    </div>
                                    <span class="badge badge-light-success fs-8 fw-bolder">'.$x.'</span>
                                </div>';              
                    }

                }else
                {
                    $seats_html.='<div class="d-flex align-items-center mb-8">
                                    <span class="bullet bullet-vertical h-40px bg-success"></span>
                                    <div class="form-check form-check-custom form-check-solid mx-5">
                                        <input class="form-check-input" type="checkbox" value="'.$this_allocation->Seat.'" name="bookers_seats[]">
                                    </div>
                                    <div class="flex-grow-1">
                                        <a href="#" class="text-gray-800 text-hover-primary fw-bolder fs-6">'.$this_allocation->Section.', Row '.$this_allocation->Row.'</a>
                                        <span class="text-muted fw-bold d-block">Seat Number '.$this_allocation->Seat.'</span>
                                    </div>
                                    <span class="badge badge-light-success fs-8 fw-bolder">'.$this_allocation->Seat.'</span>
                                </div>';
                }

                echo $seats_html;


            }
            

        }else{

            echo json_encode(array('status'=>'0','message' => 'Invalid Request!')); 
            die;

        }
    }


}


?>