<?php
namespace App\Controllers;


class QC_Report extends BaseController {

    function Project_qc_report(){

       $Project_id  = $this->uri->getSegment(2);
       if(!empty($Project_id)){

            $booking_table= $this->db->table('bookings');
            $query = $booking_table->Where(['Project_id' => $Project_id]);
            $query = $booking_table->get();
            $blade_data['bookings'] = $query->getResult();
            $Event_data = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();


            //Allocations Report
            $allocations_table= $this->db->table('allocation');
            $query = $allocations_table->Where(['Project_id' => $Project_id]);
            $query = $allocations_table->get();
            $allocations = $query->getResult();


            $allocations_summary=array();
            $allocations_summary['pending_allocations']=0;
            $allocations_summary['allocated_allocations']=0;
            $allocations_summary['unallocated_allocations']=0;
            foreach($allocations as $key => $allocation){

                if($allocation->Status=="pending"){

                    $allocations_summary['pending_allocations']=$allocations_summary['pending_allocations']+$allocation->Remaining;

                }elseif($allocation->Status=="allocated"){

                    $allocations_summary['allocated_allocations']=$allocations_summary['allocated_allocations']+$allocation->Quantity;
                
                }elseif($allocation->Status=="progress"){

                    $allocations_summary['allocated_allocations']=$allocations_summary['allocated_allocations']+($allocation->Quantity-$allocation->Remaining);

                    $allocations_summary['unallocated_allocations']=$allocations_summary['unallocated_allocations']+$allocation->Remaining;
                
                }
            }
            $allocations_summary['total_allocations']=($allocations_summary['pending_allocations']+$allocations_summary['allocated_allocations']+$allocations_summary['unallocated_allocations']);


            //Bookings Report
            $bookings_table= $this->db->table('bookings');
            $query = $bookings_table->Where(['Project_id' => $Project_id]);
            $query = $bookings_table->get();
            $bookings = $query->getResult();

            $bookings_summary=array();
            $bookings_summary['pending_bookings']=0;
            $bookings_summary['allocated_bookings']=0;
            $bookings_summary['unallocated_bookings']=0;
            foreach($bookings as $key => $booking){

                if($booking->Status=="pending"){

                    $bookings_summary['pending_bookings']=$bookings_summary['pending_bookings']+$booking->Quantity;
                    $bookings_summary['unallocated_bookings']=$bookings_summary['unallocated_bookings']+$booking->Quantity;

                }elseif($booking->Status=="success"){

                    $bookings_summary['allocated_bookings']=$bookings_summary['allocated_bookings']+$booking->Quantity;
                
                }
            }
            $bookings_summary['total_bookings']=($bookings_summary['allocated_bookings']+$bookings_summary['unallocated_bookings']);


            //QR Codes Report
            $qr_codes_table= $this->db->table('qr_codes');
            $query = $qr_codes_table->Where(['Project_id' => $Project_id]);
            $query = $qr_codes_table->get();
            $qr_codes = $query->getResult();

            $qr_codes_summary=array();
            $qr_codes_summary['pending_qr_codes']=0;
            $qr_codes_summary['allocated_qr_codes']=0;
            $qr_codes_summary['unallocated_qr_codes']=0;
            foreach($qr_codes as $key => $qr_code){

                if($qr_code->Status=="pending"){

                    $qr_codes_summary['pending_qr_codes']=$qr_codes_summary['pending_qr_codes']+$qr_code->Quantity;
                    $qr_codes_summary['unallocated_qr_codes']=$qr_codes_summary['unallocated_qr_codes']+$qr_code->Quantity;

                }elseif($qr_code->Status=="success"){

                    $qr_codes_summary['allocated_qr_codes']=$qr_codes_summary['allocated_qr_codes']+$qr_code->Quantity;
                
                }
            }
            $qr_codes_summary['total_qr_codes']=($qr_codes_summary['allocated_qr_codes']+$qr_codes_summary['unallocated_qr_codes']);


            $blade_data['allocations_summary']=$allocations_summary;
            $blade_data['bookings_summary']=$bookings_summary;
            $blade_data['qr_codes_summary']=$qr_codes_summary;
            $blade_data['event_data']=$Event_data;
            
            $blade_data['title']='Project QC reports';
            $blade_data['session']= $this->session->get('Mode');

            return view('project_qc_report', $blade_data);
        }



    }


}


?>