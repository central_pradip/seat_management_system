<?php
namespace App\Controllers;


class Home extends BaseController {
    
    public function change_mode() 
    {

        if ($this->request->isAJAX()) {

            $post_data=$_POST;
            $Mode=$post_data['Mode'];

            $this->session->set('Mode', $Mode);

            $redirect_url= 'reload';
            echo json_encode(array('status'=>'1','redirect_url'=>$redirect_url,'message' => $Mode.' mode applied successfully')); 
            die;

        }else{

            echo json_encode(array('status'=>'0','message' => 'Invalid request!')); 
            die;
        }

    }


}

?>