<?php
namespace App\Controllers;

class Live_Matchup extends BaseController {

    //# Common Functions
    public function filter_specific_tickets($start,$Quantity){

        $seats=[];
        $end=$start+$Quantity;
        for ($x = $start; $x < $end; $x++) {
            $seats[]=$x;
            $last_allocated=$x;
        }
        $seat_number=implode(",",$seats);
        unset($seats);

        $seat_data['seat_number']=$seat_number;
        $seat_data['last_allocated']=$last_allocated;

        return $seat_data;


    }


    public function get_where_in_auto_matchup($Project_id)
    {
        
        $query = $this->db->query("SELECT a.Quantity FROM allocation a where (a.Status='pending' OR a.Status='progress') AND a.Project_id='".$Project_id."' GROUP BY a.Quantity");
        $allocations = $query->getResult();
        
        $Quantities = [];
        foreach($allocations as $allocation){

            if(!empty($allocation)){
                array_push($Quantities,$allocation->Quantity);
            }
        }

        return $Quantities;
    }

    public function reset_everything(){

        $post_data=$_POST;
        $Project_id=$post_data['Project_id'];

        if(!empty($Project_id)){

            $allocation_table = $this->db->table('allocation');
            $query = $allocation_table->where(['Project_id' => $Project_id]);
            $query = $allocation_table->get();
            $allocation_data = $query->getResult();


            foreach($allocation_data as $allocation){

                //Reseting Allocation
                $update_allocation['Remaining']=$allocation->Quantity;
                $update_allocation['Status']='pending';
                $update_allocation['Last_allocated']='0';

                $allocation_table->set($update_allocation);
                $allocation_table->where(['id' => $allocation->id,'Project_id' => $Project_id]);
                $allocation_table->update();

                unset($update_allocation);

            }

            //Reseting ALL Bookings
            $update_booking['Status']='Pending';
            $update_booking['Section']=' ';
            $update_booking['Row']=' ';
            $update_booking['Seat']=' ';
            $update_booking['Entrance']=' ';
            $update_booking['Extra']=' ';

            $bookings_table = $this->db->table('bookings');
            $bookings_table->set($update_booking);
            $bookings_table->where(['Project_id' => $Project_id]);
            $bookings_table->update();

            unset($update_booking);

            $redirect_url=APP_URL.'live_matchup_dashboard/'.$Project_id;
            echo json_encode(array('status'=>'1','message' => 'All bookings & allocations reseted succesfully','redirect_url' =>$redirect_url)); 
            die;
        }

    }


    function auto_matchup_everything()
    {

        $post_data=$_POST;
        $Project_id=$post_data['Project_id'];
        $Assigned_tickets=0;

        //Declaring Tables
        $bookings_table = $this->db->table('bookings');
        

        //Geting Event Data
         $query = $this->db->query("SELECT *  FROM live_events le where le.Project_id ='".$Project_id ."'");
         $event_data = $query->getRow();

         if(!empty($event_data)){

                $Sepration_By=$event_data->Sepration;
                $Quantities=$this->get_where_in_auto_matchup($Project_id);
                
                if(!empty($Quantities)){

                    $allocation_table = $this->db->table('allocation');
                    $query = $allocation_table->where(['Status' => 'pending','Project_id' => $Project_id]);
                    $query = $allocation_table->orderBy('Quantity','DESC');
                    $query = $allocation_table->get();
                    $left_allocation_data = $query->getResult();

                   
                    
                    if(!empty($left_allocation_data)){

                        foreach($left_allocation_data as $allocation){

                            $Quantity=$allocation->Quantity;
                            $Remaining_seats = $allocation->Remaining;
                            
                            $seat_array = explode($Sepration_By,$allocation->Seat);
                            if(count($seat_array)>1)
                            {
                                if($seat_array[0]>$seat_array[1]){ 
                                    $start=$seat_array[1]; 
                                }else{  
                                    $start=$seat_array[0];
                                }

                            }else
                            {
                                $start=$seat_array[0]; 
                            }

                           
                            //Looking for available bookers
                            $looking_for_user = $this->db->query("SELECT * FROM bookings b where b.Project_id='".$Project_id."' and b.Status='pending' ORDER BY b.Quantity DESC");
                            $users_data = $looking_for_user->getResult();

                            
                            
                                foreach($users_data as $udata){
                                    
                                    if($udata->Quantity<=$Remaining_seats)
                                    {
                                        
                                        //Finding seats for this user
                                        $this_seat_data=$this->filter_specific_tickets($start,$udata->Quantity);
                                        
                                        //Assigning seat numbers
                                        $update_booking['Section']=$allocation->Section;
                                        $update_booking['Row']=$allocation->Row;
                                        $update_booking['Entrance']=$allocation->Entrance;
                                        $update_booking['Extra']=$allocation->Extra;
                                        $update_booking['Seat']=$this_seat_data['seat_number'];


                                        if(!empty($update_booking)){

                                            $update_booking['Status']='success';

                                            //Updating Specific Booking
                                            $bookings_table->set($update_booking);
                                            $bookings_table->where(['id' => $udata->id, 'Project_id' => $Project_id]);
                                            $bookings_table->update();

                                            $Remaining_seats=($Remaining_seats)-($udata->Quantity);
                                            $start=$start+$udata->Quantity;

                                            //Only for calculation
                                            $Assigned_tickets=$Assigned_tickets+$udata->Quantity;
                                        
                                    
                                        
                                            if($Remaining_seats<=0){

                                                //Updating Allocation Table
                                                $update_allocation['Status']='allocated';
                                                $update_allocation['Remaining']=$Remaining_seats;
                                                $update_allocation['Last_allocated']=$this_seat_data['last_allocated'];

                                                $allocation_table->set($update_allocation);
                                                $allocation_table->where('id', $allocation->id);
                                                $allocation_table->update();

                                                //echo 'A1 - '.$this->db->getLastQuery().'</br>';
                                                break;

                                            }else{

                                                $looking_for_left_user = $this->db->query("SELECT id FROM bookings b where b.Status='pending' and Project_id='".$Project_id."' ORDER BY b.Quantity DESC");
                                                $user_left = $looking_for_left_user->getRow();

                                                if(empty($user_left)){

                                                    //Updating Allocation Table 
                                                    $update_allocation['Status']='progress';
                                                    $update_allocation['Remaining']=$Remaining_seats;
                                                    $update_allocation['Last_allocated']=$this_seat_data['last_allocated'];

                                                    $allocation_table->set($update_allocation);
                                                    $allocation_table->where('id', $allocation->id);
                                                    $allocation_table->update();

                                                    //echo 'A2 - '.$this->db->getLastQuery().'</br>';
                                                    break;

                                                }
                                            }


                                        }else{

                                            echo json_encode(array('status'=>'0','message' => 'Update Data Not Set!')); 
                                            die;
                                        }


                                    }else{
                                        
                                        //Checking if we can find bookings this remaining seats
                                        $finding_user_for_remaning = $this->db->query("SELECT *  FROM bookings b where b.Project_id='".$Project_id."' and b.Status='pending' and b.Quantity='".$Remaining_seats."' limit 1");
                                        $udata2 = $finding_user_for_remaning->getRow();

                                        if(!empty($udata2)){

                                            //Finding seats for this user
                                            $this_seat_data=$this->filter_specific_tickets($start,$udata2->Quantity);
                                            
                                            //Assigning seat numbers
                                            $update_remaining_booking['Section']=$allocation->Section;
                                            $update_remaining_booking['Row']=$allocation->Row;
                                            $update_remaining_booking['Entrance']=$allocation->Entrance;
                                            $update_remaining_booking['Extra']=$allocation->Extra;
                                            $update_remaining_booking['Seat']=$this_seat_data['seat_number'];

                                            if(!empty($update_remaining_booking)){

                                                $update_remaining_booking['Status']='success';
                                                
                                                //Updating Specific Booking
                                            
                                                $bookings_table->set($update_remaining_booking);
                                                $bookings_table->where(['id' => $udata2->id, 'Project_id' => $Project_id]);
                                                $bookings_table->update();

                                                //echo 'P2 - '.$this->db->getLastQuery().'<br>';
                                                $Remaining_seats=($Remaining_seats)-($udata2->Quantity);
                                                $start=$start+$udata2->Quantity;

                                                //Only for calculation
                                                $Assigned_tickets=$Assigned_tickets+$udata2->Quantity;

                                                $update_remaining_allocation['Status']='allocated';
                                                $update_remaining_allocation['Remaining']=$Remaining_seats;
                                                $update_remaining_allocation['Last_allocated']=$this_seat_data['last_allocated'];

                                                $allocation_table->set($update_remaining_allocation);
                                                $allocation_table->where('id', $allocation->id);
                                                $allocation_table->update();

                                                //echo 'A3 - '.$this->db->getLastQuery().'</br>';
                                                break;
        
                                            }

                                        }else{

                                            //Updating Allocation Table 
                                            $update_allocation['Status']='progress';
                                            $update_allocation['Remaining']=$Remaining_seats;
                                            $update_allocation['Last_allocated']=$this_seat_data['last_allocated'];

                                            $allocation_table->set($update_allocation);
                                            $allocation_table->where('id', $allocation->id);
                                            $allocation_table->update();

                                            unset($update_allocation);
                                            break;

                                        }


                                    }

                                }

                        }

                        $redirect_url= APP_URL.'live_matchup_dashboard/'.$Project_id;
                        echo json_encode(array('status'=>'1','message' => $Assigned_tickets.' Tickets Allocated Succesfully','redirect_url' => $redirect_url)); 
                        die;


                    }else{
                        echo json_encode(array('status'=>'0','message' => 'Allocation Not Found!')); 
                        die;
                    }
                
                }
                

         }


    }


    
    function auto_matchup_left_seats(){

        $post_data=$_POST;
        $Project_id=$post_data['Project_id'];
        $Quantity=$post_data['Quantity'];
        $Assigned_tickets=0;

        //Declaring Tables
        $bookings_table = $this->db->table('bookings');
        

        //Geting Event Data
         $query = $this->db->query("SELECT *  FROM live_events le where le.Project_id ='".$Project_id ."'");
         $event_data = $query->getRow();

         if(!empty($event_data)){

                $Sepration_By=$event_data->Sepration;
                $Quantities=$this->get_where_in_auto_matchup($Project_id);
                
                if(!empty($Quantities)){

                    $allocation_table = $this->db->table('allocation');
                    $query = $allocation_table->where(['Status' => 'pending','Project_id' => $Project_id,'Quantity' => $Quantity]);
                    $query = $allocation_table->orderBy('Quantity','DESC');
                    $query = $allocation_table->get();
                    $left_allocation_data = $query->getResult();

                   
                    if(!empty($left_allocation_data)){

                        foreach($left_allocation_data as $allocation){

                            $Quantity=$allocation->Quantity;
                            $Remaining_seats = $allocation->Remaining;
                            
                            $seat_array = explode($Sepration_By,$allocation->Seat);
                            if(count($seat_array)>1)
                            {
                                if($seat_array[0]>$seat_array[1]){ 
                                    $start=$seat_array[1]; 
                                }else{  
                                    $start=$seat_array[0];
                                }

                            }else
                            {
                                $start=$seat_array[0]; 
                            }

                           
                            //Looking for available bookers
                            $looking_for_user = $this->db->query("SELECT * FROM bookings b where b.Project_id='".$Project_id."' and b.Status='pending' ORDER BY b.Quantity DESC");
                            $users_data = $looking_for_user->getResult();

                            if(count($users_data)>=1){
                            
                                foreach($users_data as $udata){
                                    
                                    if($udata->Quantity<=$Remaining_seats){
                                        
                                        //Finding seats for this user
                                        $this_seat_data=$this->filter_specific_tickets($start,$udata->Quantity);
                                        
                                        //Assigning seat numbers
                                        $update_booking['Section']=$allocation->Section;
                                        $update_booking['Row']=$allocation->Row;
                                        $update_booking['Entrance']=$allocation->Entrance;
                                        $update_booking['Extra']=$allocation->Extra;
                                        $update_booking['Seat']=$this_seat_data['seat_number'];


                                        if(!empty($update_booking)){
                                            $update_booking['Status']='success';

                                            //Updating Specific Booking
                                            $bookings_table->set($update_booking);
                                            $bookings_table->where(['id' => $udata->id, 'Project_id' => $Project_id]);
                                            $bookings_table->update();

                                            $Remaining_seats=($Remaining_seats)-($udata->Quantity);
                                            $start=$start+$udata->Quantity;

                                            //Only for calculation
                                            $Assigned_tickets=$Assigned_tickets+$udata->Quantity;
                                        
                                    
                                        
                                            if($Remaining_seats<=0){

                                                //Updating Allocation Table
                                                $update_allocation['Status']='allocated';
                                                $update_allocation['Remaining']=$Remaining_seats;
                                                $update_allocation['Last_allocated']=$this_seat_data['last_allocated'];

                                                $allocation_table->set($update_allocation);
                                                $allocation_table->where('id', $allocation->id);
                                                $allocation_table->update();

                                                //echo 'A1 - '.$this->db->getLastQuery().'</br>';
                                                break;

                                            }else{

                                                $looking_for_left_user = $this->db->query("SELECT id FROM bookings b where b.Status='pending' and Project_id='".$Project_id."' ORDER BY b.Quantity DESC");
                                                $user_left = $looking_for_left_user->getRow();

                                                if(empty($user_left)){

                                                    //Updating Allocation Table 
                                                    $update_allocation['Status']='progress';
                                                    $update_allocation['Remaining']=$Remaining_seats;
                                                    $update_allocation['Last_allocated']=$this_seat_data['last_allocated'];

                                                    $allocation_table->set($update_allocation);
                                                    $allocation_table->where('id', $allocation->id);
                                                    $allocation_table->update();

                                                    //echo 'A2 - '.$this->db->getLastQuery().'</br>';
                                                    break;

                                                }
                                            }


                                        }else{

                                            echo json_encode(array('status'=>'0','message' => 'Update Data Not Set!')); 
                                            die;
                                        }


                                    }else{
                                        
                                        //Checking if we can find bookings this remaining seats
                                        $finding_user_for_remaning = $this->db->query("SELECT *  FROM bookings b where b.Project_id='".$Project_id."' and b.Status='pending' and b.Quantity='".$Remaining_seats."' limit 1");
                                        $udata2 = $finding_user_for_remaning->getRow();

                                        if(!empty($udata2)){

                                            //Finding seats for this user
                                            $this_seat_data=$this->filter_specific_tickets($start,$udata2->Quantity);
                                            
                                            //Assigning seat numbers
                                            $update_remaining_booking['Section']=$allocation->Section;
                                            $update_remaining_booking['Row']=$allocation->Row;
                                            $update_remaining_booking['Entrance']=$allocation->Entrance;
                                            $update_remaining_booking['Extra']=$allocation->Extra;
                                            $update_remaining_booking['Seat']=$this_seat_data['seat_number'];

                                            if(!empty($update_remaining_booking)){

                                                $update_remaining_booking['Status']='success';
                                                
                                                //Updating Specific Booking
                                            
                                                $bookings_table->set($update_remaining_booking);
                                                $bookings_table->where(['id' => $udata2->id, 'Project_id' => $Project_id]);
                                                $bookings_table->update();

                                                //echo 'P2 - '.$this->db->getLastQuery().'<br>';
                                                $Remaining_seats=($Remaining_seats)-($udata2->Quantity);
                                                $start=$start+$udata2->Quantity;

                                                //Only for calculation
                                                $Assigned_tickets=$Assigned_tickets+$udata2->Quantity;

                                                $update_remaining_allocation['Status']='allocated';
                                                $update_remaining_allocation['Remaining']=$Remaining_seats;
                                                $update_remaining_allocation['Last_allocated']=$this_seat_data['last_allocated'];

                                                $allocation_table->set($update_remaining_allocation);
                                                $allocation_table->where('id', $allocation->id);
                                                $allocation_table->update();

                                                //echo 'A3 - '.$this->db->getLastQuery().'</br>';
                                                break;
        
                                            }

                                        }else{

                                            //Updating Allocation Table 
                                            $update_allocation['Status']='progress';
                                            $update_allocation['Remaining']=$Remaining_seats;
                                            $update_allocation['Last_allocated']=$this_seat_data['last_allocated'];

                                            $allocation_table->set($update_allocation);
                                            $allocation_table->where('id', $allocation->id);
                                            $allocation_table->update();

                                            unset($update_allocation);
                                            break;

                                        }


                                    }

                                }

                            }else{

                                echo json_encode(array('status'=>'3','message' => 'No bookings found while performing matchup!')); 
                                die;

                            }
                            

                        }

                        $redirect_url= APP_URL.'live_matchup_dashboard/'.$Project_id;
                        echo json_encode(array('status'=>'1','message' => $Assigned_tickets.' Tickets Allocated Succesfully','redirect_url' =>$redirect_url)); 
                        die;


                    }else{
                        echo json_encode(array('status'=>'0','message' => 'No more seats left from the allocation of '.$Quantity.'s')); 
                        die;
                    }
                
                }
                

         }


    }


    
    function auto_matchup_exact_seats(){

        $post_data=$_POST;
        $Project_id=$post_data['Project_id'];
        $Quantity=$post_data['Quantity'];
        $Assigned_tickets=0;

        //Declaring Tables
        $bookings_table = $this->db->table('bookings');
        

        //Geting Event Data
         $query = $this->db->query("SELECT *  FROM live_events le where le.Project_id ='".$Project_id ."'");
         $event_data = $query->getRow();

         if(!empty($event_data)){

                $Sepration_By=$event_data->Sepration;
                $Quantities=$this->get_where_in_auto_matchup($Project_id);
                
                if(!empty($Quantities)){

                    $allocation_table = $this->db->table('allocation');
                    $query = $allocation_table->where(['Status' => 'pending','Project_id' => $Project_id,'Quantity' => $Quantity]);
                    $query = $allocation_table->orderBy('Quantity','DESC');
                    $query = $allocation_table->get();
                    $left_allocation_data = $query->getResult();

                   
                    if(!empty($left_allocation_data)){

                        foreach($left_allocation_data as $allocation){

                            $Quantity=$allocation->Quantity;
                            $Remaining_seats = $allocation->Remaining;
                            
                            $seat_array = explode($Sepration_By,$allocation->Seat);
                            if(count($seat_array)>1)
                            {
                                if($seat_array[0]>$seat_array[1]){ 
                                    $start=$seat_array[1]; 
                                }else{  
                                    $start=$seat_array[0];
                                }

                            }else
                            {
                                $start=$seat_array[0]; 
                            }

                           
                            //Looking for available bookers
                            $looking_for_user = $this->db->query("SELECT * FROM bookings b where b.Project_id='".$Project_id."' and b.Status='pending' and b.Quantity='".$Quantity."' ORDER BY b.Quantity DESC");
                            $users_data = $looking_for_user->getResult();

                            if(count($users_data)>=1)
                            {
                            
                                foreach($users_data as $udata){
                                    
                                    if($udata->Quantity<=$Remaining_seats)
                                    {
                                        
                                        //Finding seats for this user
                                        $this_seat_data=$this->filter_specific_tickets($start,$udata->Quantity);
                                        
                                        //Assigning seat numbers
                                        $update_booking['Section']=$allocation->Section;
                                        $update_booking['Row']=$allocation->Row;
                                        $update_booking['Entrance']=$allocation->Entrance;
                                        $update_booking['Extra']=$allocation->Extra;
                                        $update_booking['Seat']=$this_seat_data['seat_number'];


                                        if(!empty($update_booking)){
                                            $update_booking['Status']='success';

                                            //Updating Specific Booking
                                            $bookings_table->set($update_booking);
                                            $bookings_table->where(['id' => $udata->id, 'Project_id' => $Project_id]);
                                            $bookings_table->update();

                                            $Remaining_seats=($Remaining_seats)-($udata->Quantity);
                                            $start=$start+$udata->Quantity;

                                            //Only for calculation
                                            $Assigned_tickets=$Assigned_tickets+$udata->Quantity;
                                        
                                    
                                        
                                            if($Remaining_seats<=0){

                                                //Updating Allocation Table
                                                $update_allocation['Status']='allocated';
                                                $update_allocation['Remaining']=$Remaining_seats;
                                                $update_allocation['Last_allocated']=$this_seat_data['last_allocated'];

                                                $allocation_table->set($update_allocation);
                                                $allocation_table->where('id', $allocation->id);
                                                $allocation_table->update();

                                                //echo 'A1 - '.$this->db->getLastQuery().'</br>';
                                                break;

                                            }else{

                                                $looking_for_left_user = $this->db->query("SELECT id FROM bookings b where b.Status='pending' and Project_id='".$Project_id."' and b.Quantity='".$Quantity."' ORDER BY b.Quantity DESC");
                                                $user_left = $looking_for_left_user->getRow();

                                                if(empty($user_left)){

                                                    //Updating Allocation Table 
                                                    $update_allocation['Status']='progress';
                                                    $update_allocation['Remaining']=$Remaining_seats;
                                                    $update_allocation['Last_allocated']=$this_seat_data['last_allocated'];

                                                    $allocation_table->set($update_allocation);
                                                    $allocation_table->where('id', $allocation->id);
                                                    $allocation_table->update();

                                                    //echo 'A2 - '.$this->db->getLastQuery().'</br>';
                                                    break;

                                                }
                                            }


                                        }else{

                                            echo json_encode(array('status'=>'0','message' => 'Update Data Not Set!')); 
                                            die;
                                        }


                                    }

                                }

                            }else{


                                if($Assigned_tickets>=1){

                                    $redirect_url= APP_URL.'live_matchup_dashboard/'.$Project_id;
                                    echo json_encode(array('status'=>'1','message' => $Assigned_tickets.' Tickets Allocated Succesfully','redirect_url' =>$redirect_url)); 
                                    die;

                                }else{
                                    
                                    echo json_encode(array('status'=>'3','message' => 'No bookings found while performing matchup!')); 
                                    die;
                                }
                                

                            }
                            

                        }

                        $redirect_url= APP_URL.'live_matchup_dashboard/'.$Project_id;
                        echo json_encode(array('status'=>'1','message' => $Assigned_tickets.' Tickets Allocated Succesfully','redirect_url' =>$redirect_url)); 
                        die;


                    }else{
                        echo json_encode(array('status'=>'0','message' => 'No more seats left from the allocation of '.$Quantity.'s')); 
                        die;
                    }
                
                }
                

         }


    }

}


?>