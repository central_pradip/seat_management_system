<?php
namespace App\Controllers;


class QR_Codes extends BaseController {

    

    public function view_all_qr_matchups(){
        
        $matchup_events_query = $this->db->query("SELECT *  FROM live_events le where le.Scanned_tickets='No' order by le.created_at desc");
        $matchup_events = $matchup_events_query->getResult();

        $blade_data['detail_images']='https://centralticket.nyc3.cdn.digitaloceanspaces.com/event_images/';
        $blade_data['matchup_events']=$matchup_events;

        $blade_data['title']='View all QR matchups';
        $blade_data['session']= $this->session->get('Mode');

        return view('view_all_qr_matchups', $blade_data);

    }


    function add_qr_codes(){

        if ($this->request->isAJAX()) {

            $post_data=$_POST;
            $Project_id=$post_data['Project_id'];
            $qr_codes_table = $this->db->table('qr_codes');

            //Deleting Old QR
            $qr_codes_table->where('Project_id', $Project_id);
            $qr_codes_table->delete();

            $qr_count = 0;
            if($post_data['live_bookings']=="1"){

                //Pull QR Codes From Live Website
                $Event_data = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();

                $api_data['schedule_id']=$Event_data->Schedule_id;
                $api_response =call_api('Seat_Managment_API/get_event_qr_codes',$api_data);

                if($api_response['status']=="200"){

                    foreach($api_response['bookings'] as $booking){
                        
                        $qr_booking_data['Fname']=$booking['fname'];
                        $qr_booking_data['Lname']=$booking['lname'];
                        $qr_booking_data['Email']=$booking['email'];
                        $qr_booking_data['Reference']=$booking['ref_id'];
                        $qr_booking_data['Quantity']='1';
                        $qr_booking_data['Barcode']=$booking['qr_code'];
                        $qr_booking_data['Project_id']=$Project_id;
                        $qr_booking_data['Status']='pending';

                        
                        $qr_codes_table->insert($qr_booking_data);
                        $qr_count++;

                    }
                   
                    echo json_encode(array('status'=>'1','message' => $qr_count.' QR Codes Imported Successfully')); 
                    die;

                }else{

                    echo json_encode(array('status'=>'0','message' => $api_response['message'])); 
                    die;
                }

            }else{

                if($file = $this->request->getFile('file')){

                    if($file->isValid() && !$file->hasMoved()){
    
                        $newName = "qr_codes_".$file->getRandomName();
    
                        $file->move('../public/matchup/qr_codes', $newName);
                        $file = fopen("../public/matchup/qr_codes/" . $newName, "r");
                        $i = 0;
    
                        
                        $qr_count=0;
                        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                            
                            $qr_booking_data['Fname']=$filedata[0];
                            $qr_booking_data['Lname']=$filedata[1];
                            $qr_booking_data['Email']=$filedata[2];
                            $qr_booking_data['Reference']=$filedata[3];
                            $qr_booking_data['Quantity']=$filedata[4];
                            $qr_booking_data['Barcode']=$filedata[5];
                            $qr_booking_data['Project_id']=$Project_id;
                            $qr_booking_data['Status']='pending';
                            
                            //Inserting Qr Code Data data
                            $qr_count++;
                            $qr_codes_table->insert($qr_booking_data);
    
                        }
    
                        echo json_encode(array('status'=>'1','message' => $qr_count.' QR Code Uploaded Successfully')); 
                        die;
    
                    }else{
                        echo json_encode(array('status'=>'0','message' => 'File not uploaded')); 
                        die;
                        
                    }
    
    
                }else{

                    echo json_encode(array('status'=>'0','message' => 'File not uploaded')); 
                    die;
    
                }
            }
            


        }else{

            $blade_data['events_data'] = $this->db->query("SELECT * FROM live_events le order by le.id DESC")->getResult();
            $blade_data['title']='Add CT QR';
            $blade_data['session']= $this->session->get('Mode');
            return view('add_qr_codes', $blade_data);

        }
        
    }


    function add_venue_qr_codes(){

        if ($this->request->isAJAX()) {

            $post_data=$_POST;
            $Project_id=$post_data['Project_id'];
            $venue_qr_codes_table = $this->db->table('venue_qr_codes');

            //Deleting Old QR
            $venue_qr_codes_table->where('Project_id', $Project_id);
            $venue_qr_codes_table->delete();

                if($file = $this->request->getFile('file')){

                    if($file->isValid() && !$file->hasMoved()){
    
                        $newName = "venue_qr_codes_".$file->getRandomName();
    
                        $file->move('../public/matchup/venue_qr_codes', $newName);
                        $file = fopen("../public/matchup/venue_qr_codes/" . $newName, "r");
                        $i = 0;
    
                        
                        $qr_count=0;
                        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                            
                           if(!empty($filedata[0]) && !empty($filedata[1])){

                                $venue_qr_booking_data['Section']=$filedata[0];
                                $venue_qr_booking_data['Row']=$filedata[1];
                                $venue_qr_booking_data['Seat']=$filedata[2];
                                $venue_qr_booking_data['Quantity']=$filedata[3];
                                $venue_qr_booking_data['Barcode']=$filedata[4];

                                if(!empty($filedata[5])){
                                    $venue_qr_booking_data['Entrance']=$filedata[5];
                                }else{
                                    $venue_qr_booking_data['Entrance']='NA';
                                }
                                $venue_qr_booking_data['Project_id']=$Project_id;
                                $venue_qr_booking_data['Status']='pending';
                                
                                //Inserting Qr Code Data data
                                $qr_count++;
                                $venue_qr_codes_table->insert($venue_qr_booking_data);

                            }
    
                        }
    
                        echo json_encode(array('status'=>'1','message' => $qr_count.' Venue`s QR Codes Imported Successfully')); 
                        die;
    
                    }else{
                        echo json_encode(array('status'=>'0','message' => 'File not uploaded')); 
                        die;
                        
                    }
    
    
                }else{

                    echo json_encode(array('status'=>'0','message' => 'File not uploaded')); 
                    die;
    
                }
            
        }else{

            $blade_data['events_data'] = $this->db->query("SELECT * FROM live_events le order by le.id DESC")->getResult();

            $blade_data['title']='Add Venue QR';
            $blade_data['session']= $this->session->get('Mode');

            return view('add_venue_qr_codes', $blade_data);

        }
        
    }


    

    //Merge Actions
    public function merge_ct_qr_codes(){

        if ($this->request->isAJAX()) {

            $post_data=$_POST;
            $Project_id=$post_data['Project_id'];


            //Getting Bookings
            $booking_table  = $this->db->table('bookings');
            $query = $booking_table->Where(['Project_id' => $Project_id]);
            $query = $booking_table ->get();
            $bookings_rows = $query->getResult();

           

            $qr_codes_table= $this->db->table('qr_codes');
            $ct_qr_count=0;
            foreach($bookings_rows as $booking){

                $seat_array=explode(",",$booking->Seat);
                if(count($seat_array)>1){

                    for ($x = 0; $x < count($seat_array); $x++) 
                    {
                        $update_booking['Project_id']=$Project_id;
                        $update_booking['Section']=$booking->Section;
                        $update_booking['Row']=$booking->Row;
                        $update_booking['Seat']=$seat_array[$x];
                        $update_booking['Status']='success';

                        if(!empty($booking->Booking_id)){
                            $update_booking['Booking_id']=$booking->Booking_id;
                        }
                        if(!empty($booking->Entrance)){
                            $update_booking['Entrance']=$booking->Entrance;
                        }
                        if(!empty($booking->Extra)){
                            $update_booking['Extra']=$booking->Extra;
                        }
                        
                        $qr_codes_table->set($update_booking);
                        $qr_codes_table->where('Reference', $booking->Reference);
                        $qr_codes_table->where('Status', 'pending');
                        $qr_codes_table->limit(1);
                        $qr_codes_table->update(); 
                        
                        unset($update_booking);

                        $ct_qr_count++;
    
                    }

                }else{

                    $update_booking['Project_id']=$Project_id;
                    $update_booking['Section']=$booking->Section;
                    $update_booking['Row']=$booking->Row;
                    $update_booking['Seat']=$booking->Seat;
                    $update_booking['Status']='success';

                    if(!empty($booking->Booking_id)){
                        $update_booking['Booking_id']=$booking->Booking_id;
                    }
                    if(!empty($booking->Entrance)){
                        $update_booking['Entrance']=$booking->Entrance;
                    }
                    if(!empty($booking->Extra)){
                        $update_booking['Extra']=$booking->Extra;
                    }

                    $qr_codes_table->set($update_booking);
                    $qr_codes_table->where('Reference', $booking->Reference);
                    $qr_codes_table->where('Status', 'pending');
                    $qr_codes_table->limit(1);
                    $qr_codes_table->update(); 
                    
                    unset($update_booking);
                    $ct_qr_count++;

                }
                unset($seat_array);

            }

            echo json_encode(array('status'=>'1','message' => $ct_qr_count.' QR Codes Merged Successfully')); 
            die;


        }else{

            echo json_encode(array('status'=>'0','message' => 'Invalid Request!')); 
            die;

        }


    }

    public function merge_venue_qr_codes(){

        if ($this->request->isAJAX()) {

            $post_data=$_POST;
            $Project_id=$post_data['Project_id'];


            //Getting Bookings
            $qr_codes_table  = $this->db->table('qr_codes');
            $query = $qr_codes_table->Where(['Project_id' => $Project_id]);
            $query = $qr_codes_table ->get();
            $qr_codes_rows = $query->getResult();

            
            $venue_qr_codes_table= $this->db->table('venue_qr_codes');
            $ct_qr_count=0;

            foreach($qr_codes_rows as $qr_code){

                $update_booking['CT_Barcode']=$qr_code->Barcode;
                $update_booking['CT_Barcode_id']=$qr_code->id;
                $update_booking['Status']='success';

                
                $venue_qr_codes_table->set($update_booking);
                $venue_qr_codes_table->where('Section', $qr_code->Section);
                $venue_qr_codes_table->where('Row', $qr_code->Row);
                $venue_qr_codes_table->where('Seat', $qr_code->Seat);
                $venue_qr_codes_table->limit(1);
                $venue_qr_codes_table->update(); 
                
                unset($update_booking);
                $ct_qr_count++;
            }

            echo json_encode(array('status'=>'1','message' => $ct_qr_count.' QR Codes Merged Successfully')); 
            die;


        }else{

            echo json_encode(array('status'=>'0','message' => 'Invalid Request!')); 
            die;

        }


    }
    //Merge Actions

    //Download Pages
    public function download_all_qr_codes(){

       $Project_id  = $this->uri->getSegment(3);

        if(!empty($Project_id)){

            $qr_codes_table= $this->db->table('qr_codes');
            $query = $qr_codes_table->Where(['Project_id' => $Project_id]);
            $query = $qr_codes_table->get();

            $blade_data['event_data'] = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();
            $blade_data['qr_codes'] = $query->getResult();

            $blade_data['title']='Download all QR';
            $blade_data['session']= $this->session->get('Mode');

            return view('__download_all_qr_codes', $blade_data);


        }
    }

    public function download_ct_qr_codes(){

        $Project_id  = $this->uri->getSegment(3);

        if(!empty($Project_id)){

            $qr_codes_table= $this->db->table('qr_codes');
            $query = $qr_codes_table->Where(['Project_id' => $Project_id]);
            $query = $qr_codes_table->get();

            $blade_data['event_data'] = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();
            $blade_data['qr_codes'] = $query->getResult();

            $blade_data['title']='Download CT QR';
            $blade_data['session']= $this->session->get('Mode');

            return view('__download_ct_qr_codes', $blade_data);


        }
    }

    public function download_venue_qr_codes(){

        $Project_id  = $this->uri->getSegment(3);

        if(!empty($Project_id)){

            $venue_qr_codes_table= $this->db->table('venue_qr_codes');
            $query = $venue_qr_codes_table->Where(['Project_id' => $Project_id]);
            $query = $venue_qr_codes_table->get();

            $blade_data['event_data'] = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();
            $blade_data['qr_codes'] = $query->getResult();

            $blade_data['title']='Download venue QR';
            $blade_data['session']= $this->session->get('Mode');

            return view('__download_venue_qr_codes', $blade_data);


        }
    }

    public function download_qr_codes_for_developers(){

        $Project_id  = $this->uri->getSegment(3);

        if(!empty($Project_id)){

            $qr_codes_table= $this->db->table('qr_codes');
            $query = $qr_codes_table->Where(['Project_id' => $Project_id]);
            $query = $qr_codes_table->get();

            $blade_data['event_data'] = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();
            $blade_data['qr_codes'] = $query->getResult();

            $blade_data['title']='Download ALL QR';
            $blade_data['session']= $this->session->get('Mode');

            return view('__download_qr_codes_for_developers', $blade_data);


        }
    }

    //Download Pages

    //Reset All QR Codes
    public function reset_all_qr_codes(){

            $post_data=$_POST;
            $Project_id=$post_data['Project_id'];

            if(!empty($Project_id)){

                //Reseting CT QR Codes
                $update_ct_qr['Status']='Pending';
                $update_ct_qr['Section']=' ';
                $update_ct_qr['Row']='';
                $update_ct_qr['Seat']='';
                $update_ct_qr['Entrance']='';
                $update_ct_qr['Extra']='';
                $update_ct_qr['Booking_id']='';


                $ct_qr_code_table = $this->db->table('qr_codes');
                $ct_qr_code_table->set($update_ct_qr);
                $ct_qr_code_table->where(['Project_id' => $Project_id]);
                $ct_qr_code_table->update();


                //Reseting Venue QR Codes
                $update_venue_qr['Status']='Pending';
                $update_venue_qr['CT_Barcode_id']='';
                $update_venue_qr['CT_Barcode']='';


                $venue_qr_code_table = $this->db->table('venue_qr_codes');
                $venue_qr_code_table->set($update_venue_qr);
                $venue_qr_code_table->where(['Project_id' => $Project_id]);
                $venue_qr_code_table->update();

                echo json_encode(array('status'=>'1','message' => 'All QR codes are reseted successfully!')); 
                die;


            }


    }
    //Reset All QR Codes

    public function replace_ct_qr_with_venue()
    {

        $post_data=$_POST;
        $Project_id=$post_data['Project_id'];

        if(!empty($Project_id)){

            $event_data = $this->db->query("SELECT le.Event_id FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();

            if(!empty($event_data->Event_id)){

                $venue_qr_codes_query = $this->db->query("SELECT vqr.Barcode,vqr.CT_Barcode FROM venue_qr_codes vqr where vqr.Project_id ='".$Project_id ."' and vqr.Status = 'success'");
                $Venue_QR_Codes = $venue_qr_codes_query->getResult();

                $api_data['Venue_QR_Codes']=$Venue_QR_Codes;
                $api_data['event_id']=$event_data->Event_id;

                $api_response =call_api('Seat_Managment_API/replace_qr_with_venue',$api_data);

                if($api_response['status']=="200"){

                    echo json_encode(array('status'=>'1','message' =>$api_response['message'])); 
                    die;

                }else{

                    echo json_encode(array('status'=>'1','message' =>$api_response['message'])); 
                    die;

                }

            }

            

        }

    }

}


?>