<?php
namespace App\Controllers;


class Download extends BaseController {

    public function download_bookings_type_1(){
        
        $Project_id  = $this->uri->getSegment(3);
        if(!empty($Project_id)){

            $booking_table= $this->db->table('bookings');
            $query = $booking_table->Where(['Project_id' => $Project_id]);
            $query = $booking_table->get();
            $blade_data['bookings'] = $query->getResult();
 
            $blade_data['event_data'] = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();

            $blade_data['title']='Download Bookings';
            $blade_data['session']= $this->session->get('Mode');
            return view('__download_bookings_type_1', $blade_data);
        }

    }


    public function download_bookings_type_2(){
        
        $Project_id  = $this->uri->getSegment(3);
        if(!empty($Project_id)){

            $booking_table= $this->db->table('bookings');
            $query = $booking_table->Where(['Project_id' => $Project_id]);
            $query = $booking_table->get();
            $blade_data['bookings'] = $query->getResult();
 
            $blade_data['event_data'] = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();
            
            $blade_data['title']='Download Bookings';
            $blade_data['session']= $this->session->get('Mode');
            return view('__download_bookings_type_2', $blade_data);
        }

    }


    public function download_bookings_custom_type(){
        
        $Project_id  = $this->uri->getSegment(3);
        if(!empty($Project_id)){

            $booking_table= $this->db->table('bookings');
            $query = $booking_table->Where(['Project_id' => $Project_id]);
            $query = $booking_table->get();
            $blade_data['bookings'] = $query->getResult();
 
            $blade_data['event_data'] = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();
            
            $blade_data['title']='Download Bookings';
            $blade_data['session']= $this->session->get('Mode');
            return view('__download_bookings_custom_type', $blade_data);
        }

    }



    //Allocations
    public function download_left_allocation(){
        
        $Project_id  = $this->uri->getSegment(3);
        if(!empty($Project_id)){

            $allocation_table= $this->db->table('allocation');
            $Status = ['pending', 'progress'];
            $query = $allocation_table->whereIn('Status', $Status);
            $query = $allocation_table->Where(['Project_id' => $Project_id]);
            $query = $allocation_table->orderBy('Status','ASC');
            $query = $allocation_table->get();
            $blade_data['allocations'] = $query->getResult();
 
            $blade_data['event_data'] = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();
           
            $blade_data['title']='Download Left Allocations';
            $blade_data['session']= $this->session->get('Mode');
            return view('__download_left_allocation', $blade_data);
        }

    }


    function download_all_allocation(){

       
        $Project_id  = $this->uri->getSegment(3);
        if(!empty($Project_id)){

            $allocation_table= $this->db->table('allocation');
            $query = $allocation_table->Where(['Project_id' => $Project_id]);
            $query = $allocation_table->orderBy('Quantity','DESC');
            $query = $allocation_table->get();

            $blade_data['allocations'] = $query->getResult();
            $blade_data['event_data'] = $this->db->query("SELECT * FROM live_events le where le.Project_id ='".$Project_id ."'")->getRow();
            
            $blade_data['title']='Download All Allocations';
            $blade_data['session']= $this->session->get('Mode');

            return view('__download_all_allocation.php', $blade_data);
        }

        

    }



}


?>